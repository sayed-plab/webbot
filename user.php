<?php

require_once 'app/inc/common.php';

session();

if (isset($_SESSION['user'])) {
    header('Location: index.php');
}

require_once 'app/users.php';

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Airtel Buzz Live Chat">
    <meta name="author" content="Airtel Buzz">
    <meta name="generator" content="Airtel Buzz">
    <title>Live Chat</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.3/examples/sign-in/signin.css" rel="stylesheet">
    <link rel="stylesheet" href="css/user.css">
</head>
<body class="text-center">

<form class="form-signin" action="user.php" method="post">

    <div class="mb-3">
        <img src="https://forbangladesh.com/airtel_messengerx/dashboardx/assets/img/logo.png" alt="Airtel Logo">
    </div>

    <h1 class="h3 mb-3 font-weight-normal">Live Chat</h1>

    <div class="form-group">
        <label for="name" class="sr-only">Your Nick Name</label>
        <input type="text" name="name" id="name" class="form-control" placeholder="Enter your nick name" required autofocus>
    </div>

    <div class="form-group">
        <select class="form-control" name="location" id="location" required>
            <option>Select Your Division</option>
            <option value="Dhaka">Dhaka</option>
            <option value="Chittagong">Chittagong</option>
            <option value="Barishal">Barishal</option>
            <option value="Dhaka">Khulna</option>
            <option value="Chittagong">Mymensingh</option>
            <option value="Barishal">Rajshahi</option>
            <option value="Chittagong">Rangpur</option>
            <option value="Barishal">Sylhet</option>
        </select>
    </div>

    <div class="form-group">
        <label for="phone" class="sr-only">Phone</label>
        <input type="text" name="phone" id="phone" class="form-control" placeholder="Enter your Airtel number" required>
    </div>

    <div class="form-group">
        <button class="btn btn-lg btn-block" name="login" type="submit">Join Now!</button>
        <p class="mt-5 mb-3 text-muted">Powered by &copy Preneur Lab Ltd.</p>
    </div>

</form>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-73942782-5"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-73942782-5');
</script>


</body>
</html>
