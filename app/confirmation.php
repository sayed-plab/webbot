<?php

require_once 'core/db/db.php'; 
require_once 'core/WebBot.php';

$wBot = new WebBot();

$webBotUrl = 'https://airtelbot.forbangladesh.com/webbot/engine.php';

date_default_timezone_set('Asia/Dhaka');


$dat=date('d-m-Y');
$time= date('h:i a');
$datec = strtotime(date('d-m-Y'));

$status=$_POST['status'];
$amount=$_POST['amount'];
$name=$_POST['name'];
$email=$_POST['email'];
$mobile=$_POST['mobile'];
$category=$_POST['category'];
$invoice=$_POST['invoice'];


if(isset($status) && !empty($status)) {

    $values = array(
    	null,
        $status,
        $amount,
        $name,
        $email,
        $mobile,
        $category,
        $invoice,
        $dat,
        $time,
        $datec
    );

	$wBot->savePayment($values);

}


$senderId = $wBot->getApiIssueUserIdByMSISDN($mobile);

if( $status == "success" ){

    $text="Your recharge has been successful. Enjoy yourself with #1 Network of Friends";

    $data = array (
        'recipient' =>
            array (
                'id' => $senderId,
            ),
        'message' =>
            array (
                'text' => $text,
            ),
    );

    $response = json_encode(
        array("chat" =>
            array(
                "query" => 'Recharge Status',
                "response" => $text
            ),
            "data" => $data),
        JSON_UNESCAPED_UNICODE);

} else{

    $text="Your recharge was not successful!";

   $data = array (
        'recipient' =>
            array (
                'id' => $senderId,
            ),
        'message' =>
            array (
                'text' => $text,
            ),
    );

    $response = json_encode(
        array("chat" =>
            array(
                "query" => 'Recharge Status',
                "response" => $text
            ),
            "data" => $data),
        JSON_UNESCAPED_UNICODE);
}


if (!empty($response)) {

    $ch = curl_init($webBotUrl);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $response);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type=> application/json',
            'Content-Length=> ' . strlen($response))
    );
    curl_exec($ch);
    curl_close($ch);
}

echo "<script>window.close();</script>";


