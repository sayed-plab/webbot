<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 1/31/19
 * Time: 4:46 PM
 */

require_once 'core/otp/OTP.php';
require_once 'core/db/db.php';

$user_id = htmlspecialchars($_GET['user_id']);
$msisdn = htmlspecialchars($_GET['msisdn']);
$query = htmlspecialchars($_GET['query']);

$api = new OTP();
$api->db_config($db);
$api->SendOTPToken($user_id,$msisdn,$query);
