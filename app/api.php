<?php

require_once 'core/api/CBSADCS.php';
require_once 'core/api/FnF.php';
require_once 'core/api/XRBT.php';
require_once 'core/db/db.php';

$query = $_GET['query'] ?? null;
$msisdn = $_GET['msisdn'] ?? null;
$plan_name = $_GET['plan_name']  ?? null;
$valid_mode = $_GET['valid_mode'] ?? null;
$id = $_GET['id'] ?? null;
$msisdn_fnf = $_GET['msisdn_fnf'] ?? null;
$msisdn_partner = $_GET['msisdn_partner'] ?? null;
$promo_id = $_GET['promo_id'] ?? null;
$trx_id = $_GET['trx_id'] ?? null;

$cbsadcs = new \cbsadcs\CBSADCS();
$fnf = new \fnf\FnF();
$xrbt = new \xrbt\XRBT();

$cbsadcs->db_config($db);
$fnf->db_config($db);
$xrbt->db_config($db);

//var_dump($cbsadcs->get_token());

switch ($query){

    /*************************************   CBS and ADCS  ********************************/
    case 'msisdn_type':
        $cbsadcs->msisdn_type($msisdn);
        break;
    case 'query_balance':
        $cbsadcs->query_balance($msisdn);
        break;
    case 'query_balance_postpaid':
        $cbsadcs->query_balance_postpaid($msisdn);
        break;
    case 'tarrif_plan':
        $cbsadcs->tarrif_plans($msisdn);
        break;
    case 'tarrif_plan_info':
        $cbsadcs->tarrif_plan_info($msisdn);
        break;
    case 'subscriber_allowed_plans':
        $cbsadcs->subscriber_allowed_plans($msisdn);
        break;
    case 'internet_pack_provisioning':
        $cbsadcs->internet_pack_provisioning($msisdn, $plan_name);
        break;
    case 'query_plan_purchase':
        $cbsadcs->query_plan_purchase($trx_id);
        break;
    case 'subscribe_appendant_product':
        $cbsadcs->subscribe_appendant_product($msisdn, $id);
        break;
    case 'adjust_account':
        $cbsadcs->adjust_account($msisdn);
        break;

    /***************************************   FnF  ******************************************/

    case 'get_fnf':
        $fnf->get_fnf($msisdn);
        break;
    case 'add_fnf':
        $fnf->add_fnf($msisdn, $msisdn_fnf);
        break;
    case 'add_partner':
        $fnf->add_partner($msisdn, $msisdn_partner);
        break;
    case 'delete_fnf':
        $fnf->delete_fnf($msisdn, $msisdn_fnf);
        break;

    /***************************************     (MyTune) RRBT   ******************************************/
    case 'rrbt_activation':
        $xrbt->rrbt_activation($msisdn);
        break;
    case 'rrbt_deactivation':
        $xrbt->rrbt_deactivation($msisdn);
        break;
    case 'rrbt_content_subscriptions':
        $xrbt->rrbt_content_subscriptions($msisdn, $promo_id);
        break;


    /***************************************    (Goon Goon) RBT  ******************************************/

    case 'rbt_activation':
        $xrbt->rbt_activation($msisdn);
        break;
    case 'rbt_deactivation':
        $xrbt->rbt_deactivation($msisdn);
        break;
    case 'rbt_content_subscriptions':
        $xrbt->rbt_content_subscriptions($msisdn, $promo_id);
        break;
    case 'rbt_status':
        $xrbt->rbt_status($msisdn);
        break;


    /***************************************    Airtel RRBT  ******************************************/
    case 'airtel_rrbt_activation':
        $xrbt->airtel_rrbt_activation($msisdn);
        break;
    case 'airtel_rrbt_deactivation':
        $xrbt->airtel_rrbt_deactivation($msisdn);
        break;
    case 'airtel_rrbt_content_subscriptions':
        $xrbt->airtel_rrbt_content_subscriptions($msisdn, $promo_id);
        break;


    /***************************************  (Caller Tune) Airtel RBT ******************************************/

    case 'airtel_rbt_activation':
        $xrbt->airtel_rbt_activation($msisdn);
        break;

    case 'airtel_rbt_deactivation':
        $xrbt->airtel_rbt_deactivation($msisdn);
        break;
    case 'airtel_rbt_content_subscriptions':
        $xrbt->airtel_rbt_content_subscriptions($msisdn, $promo_id);
        break;
    default:
        echo 'Query does not match.';
}
