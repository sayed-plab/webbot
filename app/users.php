<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 2/23/19
 * Time: 12:34 PM
 */

require_once 'core/Users.php';


if (isset($_POST['login'])) {


    $userInfo = array(
        "name" => $_POST['name'],
        "location" => $_POST['location'],
        "phone" => $_POST['phone']
    );

    $login = new Users();
    $loginResponse = json_decode($login->userLogin($userInfo), true);

    if($loginResponse['status'] == 'success') {


        $_SESSION['user'] = $loginResponse['user'];

        if (isset($_SESSION['user'])) {
            header('Location: index.php');
        }

    } else {
        header('Location: users.php');
    }
}
