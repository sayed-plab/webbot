<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 1/31/19
 * Time: 11:43 AM
 */

require_once 'db/db.php';

class Facebook {

    private $db;

    public function __construct(){
        global $db;
        $this->db = $db;
    }

    public function matchAll($search) {

        $match_both = array();

        foreach ($search as $row) {
            $match_both[] = "+".$row."*";
        }

        $match_both_keywords = implode(" ",$match_both);

        $sql = "SELECT * FROM query_response WHERE MATCH(keywords_en) AGAINST ('$match_both_keywords' IN NATURAL LANGUAGE MODE);";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(2);

    }

    public function exactMatch($search) {

        $exact_match = array();

        foreach ($search as $row) {
            $exact_match[] = $row;
        }

        $exact_match_keywords = implode(" ",$exact_match);

        $sql = "SELECT * FROM query_response WHERE MATCH(keywords_en) AGAINST ('\"".$exact_match_keywords."\"' IN NATURAL LANGUAGE MODE)";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(2);
    }

    public function nlpEngine($search) {

        if (sizeof($response = $this->exactMatch($search))) {
            return $response;
        } elseif (sizeof($response = $this->matchAll($search))) {
            return $response;
        } elseif (sizeof($response = $this->exactMatch(array('default')))) {
            return $response;
        }
    }


    public function getCarousel($mapping) {

        $sql = "SELECT * FROM `carousels` WHERE `mapping` = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($mapping));
        return $stmt->fetchAll(2);
    }

    public function getButtons($mapping) {

        $sql = "SELECT * FROM `buttons` WHERE mapping = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($mapping));
        return $stmt->fetchAll(2);
    }


    public function message($recipientId, $responseText) {

        $data = array(
            "recipient" => array(
                "id" => $recipientId,
            ),
            "message" => array(
                "text" => $responseText
            )
        );

        return json_encode($data,JSON_UNESCAPED_UNICODE);
    }

}