<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 2/23/19
 * Time: 12:13 PM
 */

require_once 'db/db.php';

class Users {

    private $db;

    public function __construct(){
        global $db;
        $this->db = $db;
    }

    public function getUserName($user) {

        $sql = "SELECT name FROM `customer` WHERE id = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($user));
        $resp = $stmt->fetch(2);
        return $resp['name'];
    }

    public function userLogin($userInfo){

        $sql = "SELECT id FROM `customer` WHERE phone = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($userInfo['phone']));
        $user = $stmt->fetch(2);

        if ($stmt->rowCount()) {

            $sql = "UPDATE customer SET name = ?, location = ?, last_date = ?, last_time =?, status=? WHERE phone =?";
            $stmt = $this->db->prepare($sql);
            $stmt->execute(array(
                $userInfo['name'],
                $userInfo['location'],
                date('Y-m-d'),
                date('g:i a'),
                1,
                $userInfo['phone'],
            ));

            return json_encode(array('status' => 'success', 'user' => $user['id']));

        } else {

            $sql = "INSERT INTO customer VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $stmt = $this->db->prepare($sql);
           // $this->db->beginTransaction();
            $stmt->execute(array(
                $userInfo['name'],
                $userInfo['phone'],
                $userInfo['location'],
                date('Y-m-d'),
                date('g:i a'),
                date('Y-m-d'),
                date('g:i a'),
                '',
                1
            ));

            if($this->db->lastInsertId()) {
                return json_encode(array('status' => 'success', 'user' => $this->db->lastInsertId()));
            } else {
                return json_encode(array('status' => 'failed', 'user' => null));
            }

        }
    }
}