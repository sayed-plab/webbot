<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 2/17/19
 * Time: 4:19 PM
 */

class API {

    private $otp_base_url = "https://airtelbot.forbangladesh.com/webbot/app/";

    function sendOTP($user_id, $msisdn, $query) {

        $otp_url = $this->otp_base_url.'/send-otp.php?user_id='.$user_id.'&msisdn='.$msisdn.'&query='.$query;
        $otp_status = json_decode(file_get_contents($otp_url), true);

        if ($otp_status['status'] == 'sent') {

            return 'sent';

        } elseif ($otp_status['status'] == 'already_valid'){

            return 'already_valid';

        } else {

            return 'failed';
        }
    }

    function verifyOTP($user_id, $msisdn, $issue, $otp_code) {

        $otp_url = $this->otp_base_url.'/verify-token.php?user_id='.$user_id.'&msisdn='.$msisdn.'&issue='.$issue.'&otp_code='.$otp_code;
        $otp_status = json_decode(file_get_contents($otp_url), true);

        if ($otp_status['status'] == 'valid') {

            return 'valid';

        } elseif ($otp_status['status'] == 'wrong'){

            return 'wrong';

        } elseif ($otp_status['status'] == 'expired'){

            return 'expired';

        } else {

            return 'failed';
        }
    }

}