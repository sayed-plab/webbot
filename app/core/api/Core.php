<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/11/18
 * Time: 10:19 AM
 */

namespace core;


class Core {

    protected $db;
    protected $username='MIFE_BMC_Chatbot';
    protected $password='Chatbot@123';
    protected $auth_token = 'SGFjRWluMDdTWXI1bjJ5dHFQM1BJbnJJdzk0YTpSZm15NlRmdDljaDRiNDRqMVpvenVuSnFUVnNh';
    protected $token;

    public function db_config($config)
    {
        $this->db = $config;
    }

    /**
     * API constructor.
     */
    public function __construct()
    {
        header('Content-Type: application/json');

        $url = "https://api.robi.com.bd/token";

        $headers = array(
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: Basic ".$this->auth_token
        );

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS,
            "grant_type=password&username=".$this->username."&password=".$this->password."&scope=PRODUCTION");
        $resp = curl_exec($curl);
        $array_format_resp = json_decode($resp, true);
        $this->token = $array_format_resp;
        curl_close($curl);
    }

    public function get_token()
    {
        return $this->token;
    }

    public function format_msisdn($msisdn, $length)
    {

        switch ($length)
        {
            case 10:
                $start = 0;
                if(strlen($msisdn) == 14){
                    $start = 4;
                }
                elseif(strlen($msisdn) == 13){
                    $start = 3;
                }
                elseif(strlen($msisdn) == 11){
                    $start = 1;
                }
                return substr($msisdn, $start);
                break;

            case 13:
                if(strlen($msisdn) == 14){
                    $format_msisdn = substr($msisdn, 1);
                }
                elseif(strlen($msisdn) == 11){
                    $format_msisdn = '88'.$msisdn;
                }
                elseif(strlen($msisdn) == 10){
                    $format_msisdn = '880'.$msisdn;
                }
                else
                {
                    $format_msisdn = $msisdn;
                }
                return $format_msisdn;
                break;
        }

    }

    public function record_history($values)
    {
        $sql = "INSERT INTO api_history (msisdn,query,response,date) VALUES(?,?,?,?)";
        $stmt = $this->db->prepare($sql);
        $stmt->execute($values);
    }

}