<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/7/18
 * Time: 10:02 AM
 */

namespace cbsadcs;

require_once 'Core.php';

use core\Core;

/**
 * Class CBSADCS
 * @package cbs
 */
class CBSADCS extends Core {

    /**
     * @param $msisdn
     */
    public function msisdn_type($msisdn){

        $subscriber_no = $this->format_msisdn($msisdn, 10);

        $url = "https://api.robi.com.bd/ocs/v1/integrationEnquiry";

        $headers = array(
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: Bearer ".$this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POST, 1);
        /*curl_setopt($curl, CURLOPT_POSTFIELDS,
            "SubscriberNo=".$SubscriberNo."&CommandId=QueryBalance&RequestType=Event");*/
        curl_setopt($curl, CURLOPT_POSTFIELDS,
            "SubscriberNo=".$subscriber_no."&CommandId=IntegrationEnquiry&RequestType=Event");
        $resp = curl_exec($curl);
        $api_data = json_decode($resp, true);

        $balance_desc = $api_data['IntegrationEnquiryResult']['BalanceRecordList']['BalanceRecord'][0]['BalanceDesc'];

        switch ($balance_desc) {
            case 'PospaidBalanceSubaccount':
                echo '{"msisdn_type" : "Post-Paid"}';
                break;
            case 'PrepaidBalanceSubaccount':
                echo '{"msisdn_type" : "Pre-Paid"}';
                break;
            default:
                echo '{"msisdn_type" : "Undefined"}';
                break;
        }

    }

    /**
     * @param $msisdn
     */
    public function query_balance($msisdn)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        $url = "https://api.robi.com.bd/ocs/v1/integrationEnquiry";

        $headers = array(
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: Bearer ".$this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);
        curl_setopt($curl, CURLOPT_POST, 1);
        /*curl_setopt($curl, CURLOPT_POSTFIELDS,
            "SubscriberNo=".$SubscriberNo."&CommandId=QueryBalance&RequestType=Event");*/
        curl_setopt($curl, CURLOPT_POSTFIELDS,
            "SubscriberNo=".$subscriber_no."&CommandId=IntegrationEnquiry&RequestType=Event");
        $resp = curl_exec($curl);

        $api_data = json_decode($resp, true);

        $balance = $api_data['IntegrationEnquiryResult']['BalanceRecordList']['BalanceRecord'][0]['Balance'];
        $expire_time = $api_data['IntegrationEnquiryResult']['SubscriberState']['ActiveStop'];

        date_default_timezone_set('Asia/Dhaka');

        $result_status = ($api_data['ResultHeader']['ResultCode'] == "405000000") ? 'success' : 'failed';

        echo $data = json_encode(array(
            'status' => $result_status,
            'body' => array(
                'balance' => round(($balance/10000), 2),
                'validity' => date("d M Y",strtotime($expire_time))
            )
        ), JSON_PRETTY_PRINT);


        $this->record_history(array(
            $subscriber_no,
            'Query Balance',
            $data,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);
    }

    /**
     * @param $msisdn
     */

    public function query_balance_postpaid($msisdn)
    {
        $message_seq = str_shuffle(strrev(strtotime(date('YmdHis'))));
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        $url = "https://api.robi.com.bd/cbs/v1/cbsQuerySumBalanceAndCredit";

        $headers = array(
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: Bearer ".$this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POST, 1);
        /*curl_setopt($curl, CURLOPT_POSTFIELDS,
            "SubscriberNo=".$SubscriberNo."&CommandId=QueryBalance&RequestType=Event");*/
        curl_setopt($curl, CURLOPT_POSTFIELDS,
            "Version=1&MessageSeq=".$message_seq."&PrimaryIdentity=".$subscriber_no."&OperatorID=102&BEID=101&BusinessCode=QuerySumBalanceAndCredit&ChannelID=1&AccessMode=3&MsgLanguageCo
de=2002&TimeZoneID=101&TimeType=2");
        $resp = curl_exec($curl);

        $api_data = json_decode($resp, 1);


        $unbilled_amount = $api_data['QuerySumBalanceAndCreditResult']['SumBalanceAndCreditResult']['UnbilledResult']['ChargeAmount'];
        $outstanding_amount = $api_data['QuerySumBalanceAndCreditResult']['SumBalanceAndCreditResult']['OutStandingResult']['OutStandingAmount'];
        $total_usage_amount = $api_data['QuerySumBalanceAndCreditResult']['SumBalanceAndCreditResult']['AccountCredit']['TotalUsageAmount'];
        $total_remain_amount = $api_data['QuerySumBalanceAndCreditResult']['SumBalanceAndCreditResult']['AccountCredit']['TotalRemainAmount'];

        echo $data = json_encode(array(
            'unbilled_amount' => 'TK. '.round(($unbilled_amount/10000), 2),
            'outstanding_amount' => 'TK. '.round(($outstanding_amount/10000), 2),
            'total_usage_amount' => 'TK. '.round(($total_usage_amount/10000), 2),
            'total_remain_amount' => 'TK. '.round(($total_remain_amount/10000), 2)
        ), JSON_PRETTY_PRINT);

        $this->record_history(array(
            $subscriber_no,
            'Query Balance Post-Paid',
            $data,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);
    }

    /**
     * @param $msisdn
     */
    public function tarrif_plans($msisdn)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 13);

        $url = "https://api.robi.com.bd/adcs/v1/queryCurrentPlans?msisdn=".$subscriber_no."&planName=MIFE";

        $headers = array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $resp = curl_exec($curl);


        $api_data = json_decode($resp, true);

        $format_api_data = array();

       for ($i=0; $i< $api_data['length']; $i++)
        {
            $tmp_array = array(
                'name' => $api_data['plan'][$i]['planDefinition']['name'],
                'summary' => ($api_data['plan'][$i]['planDefinition']['summary']) ? $api_data['plan'][$i]['planDefinition']['summary'] : $api_data['plan'][$i]['planDefinition']['name'],
                'validity_period' => $api_data['plan'][$i]['planDefinition']['validityPeriod'],
                'state' => $api_data['plan'][$i]['state']
            );

            array_push($format_api_data, array($tmp_array));

        }

        echo $data = json_encode(array('plans' => $format_api_data), JSON_PRETTY_PRINT);


        $this->record_history(array(
            $this->format_msisdn($msisdn, 10),
            'Tarrif Plan',
            $data,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);

    }


    /**
     * @param $msisdn
     */
    public function tarrif_plan_info($msisdn)
    {

        $subscriber_no = $this->format_msisdn($msisdn, 13);

        $url = "https://api.robi.com.bd/adcs/v1/queryCurrentPlans?msisdn=".$subscriber_no."&planName=MIFE";

        $headers = array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $resp = curl_exec($curl);

        $api_data = json_decode($resp, true);
        $plans = array();

        for ($i=0; $i< $api_data['length']; $i++)
        {
            if (strpos($api_data['plan'][$i]['planDefinition']['name'], 'PPU') === false) {

                if (!$api_data['plan'][$i]['cancelled']) {

                    if ($api_data['plan'][$i]['state'] != 'activation_pending') {

                        $unit_amount = $api_data['plan'][$i]['planDefinition']['unitAmount'];
                        $threshold = $unit_amount * 0.9;
                        $usage = $api_data['plan'][$i]['usage'];
                        $usage_pct = $usage ? round(($usage / $unit_amount) * 100, 2) : 0;


                        if ($usage >= $threshold) {

                            $tmp_plans = array(
                                "name" => $api_data['plan'][$i]['planDefinition']['name'],
                                "state" => $api_data['plan'][$i]['state'],
                                "validity_period" => $api_data['plan'][$i]['planDefinition']['validityPeriod'],
                                "recurring" => $api_data['plan'][$i]['planDefinition']['recurring'],
                                "unit_amount" => $api_data['plan'][$i]['planDefinition']['unitAmount'],
                                "purchase_timestamp" => $api_data['plan'][$i]['purchaseTimestamp'],
                                "expire_timestamp" => $api_data['plan'][$i]['expiryTimestamp'],
                                "usage" => $api_data['plan'][$i]['usage'],
                                "usage_pct" => $usage_pct
                            );

                            array_push($plans, $tmp_plans);

                        }
                    }

                }
            }

        }


        $max_usage = array(
            "name" => 0,
            "state" => 0,
            "validity_period" => 0,
            "recurring" => 0,
            "unit_amount" => 0,
            "purchase_timestamp" => 0,
            "expire_timestamp" => 0,
            "usage" => 0,
            "usage_pct" => 0
        );

        foreach ($plans as $plan) {

            if ($max_usage['unit_amount'] < $plan['unit_amount']) {

                $max_usage = array(
                    "name" => $plan['name'],
                    "state" => $plan['state'],
                    "validity_period" => $plan['validity_period'],
                    "recurring" => $plan['recurring'],
                    "unit_amount" => $plan['unit_amount'],
                    "purchase_timestamp" => $plan['purchase_timestamp'],
                    "expire_timestamp" => $plan['expire_timestamp'],
                    "usage" => $plan['usage'],
                    "usage_pct" => $plan['usage_pct']
                );
            }
        }

        if (!empty($max_usage['name'])) {
            echo json_encode($max_usage);
        } else {
            echo json_encode(array('status' => 'empty'));
        }


        curl_close($curl);

    }

    /**
     * @param $msisdn
     */

    public function subscriber_allowed_plans($msisdn)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 13);

        $url ="https://api.robi.com.bd/adcs/v1/getSubscribersAllowedPlans?msisdn=".$subscriber_no;

        $headers = array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $resp = curl_exec($curl);

        $api_data = json_decode($resp, true);

        $format_api_data = array();

        for ($i=0; $i< $api_data['length']; $i++)
        {
            array_push($format_api_data, $api_data['planDefinition'][$i]['name']);
        }

        echo $data = json_encode(array('allowed_plans' => $format_api_data), JSON_PRETTY_PRINT);


       $this->record_history(array(
            $this->format_msisdn($msisdn, 10),
            'Subscriber Allowed Plans',
            $data,
           date('Y-m-d H:i:s')

        ));

        curl_close($curl);
    }

    /**
     * @param $trx_id
     * @return mixed
     */
    public function query_plan_purchase($trx_id)
    {
        $url ="https://api.robi.com.bd/adcs/v1/queryPlanPurchase?transactionId=".$trx_id;

        $headers = array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $resp = curl_exec($curl);

        $api_data = json_decode($resp, true);
        curl_close($curl);

        return $api_data['planState'];
    }

    /**
     * @param $msisdn
     * @param $plan_name
     */
    public function internet_pack_provisioning($msisdn, $plan_name)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 13);
        $format_api_data = null;

        $url = "https://api.robi.com.bd/adcs/v1/packProvisioningNormal";

        $headers = array(
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: Bearer ".$this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS,
            "MSISDN=".$subscriber_no."&name=".$plan_name);
        $resp = curl_exec($curl);
        $api_data = json_decode($resp, 1);

        $data = json_decode(substr($resp,0,(strpos($resp, '<'))), true);

        $plan_state = $this->query_plan_purchase($data['TransactionId']);

        switch ($api_data['ResponseCode'])
        {
            case 0:
                echo $format_api_data = json_encode(array('status'=> 'Plan purchase request accepted.', 'plan_state' => $plan_state), JSON_PRETTY_PRINT);
                break;
            case 2:
                echo $format_api_data = json_encode(array('status'=> 'You are not eligible for this plan.', 'plan_state' => false), JSON_PRETTY_PRINT);
                break;
            case 3:
                echo $format_api_data = json_encode(array('status'=> 'Plan definition not found.', 'plan_state' => false), JSON_PRETTY_PRINT);
                break;
        }

        $this->record_history(array(
            $this->format_msisdn($msisdn, 10),
            'Internet Pack Provisioning',
            $format_api_data,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);
    }

    /**
     * @param $msisdn
     * @param $id
     */
    public function subscribe_appendant_product($msisdn, $id)
    {
        $format_api_data = null;
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        //[{"ValidMode":"4050001","Id":"****"}]
        $product = json_encode(
            array(
                array(
                'ValidMode' => '4050000',
                'Id' => $id
                )
            )
        );


        $url = "https://api.robi.com.bd/ocs/v1/subscribeAppendantProduct";

        $headers = array(
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: Bearer ".$this->token['access_token']
        );

        $data = array(
            "CommandId" => "SubscribeAppendantProduct",
            "RequestType" => "Event",
            "SubscriberNo"=> $subscriber_no,
            "Product" =>$product
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($data));
        $resp = curl_exec($curl);

        $api_data = json_decode($resp, 1);

        switch ($api_data['ResultHeader']['ResultCode'])
        {
            case 405000000:
                echo $format_api_data = json_encode(array('status'=> 'Operation successful.', 'plan_state' => 'purchased'), JSON_PRETTY_PRINT);
                break;
            case 118013302:
                echo $format_api_data = json_encode(array('status'=> 'The account balance of the subscriber is insufficient.', 'plan_state' => 'charging_fail'), JSON_PRETTY_PRINT);
                break;
            case 118030218:
                echo $format_api_data = json_encode(array('status'=> "Offering does not exist.", 'plan_state' => 'not_eligible'), JSON_PRETTY_PRINT);
                break;
        }

        $this->record_history(array(
            $this->format_msisdn($msisdn, 10),
            'Subscribe Appendant Product',
            $format_api_data,
            date('Y-m-d H:i:s')

        ));


        curl_close($curl);
    }


    /**
     * @param $msisdn
     */
    public function adjust_account($msisdn)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        $url = "https://api.robi.com.bd/ocs/v1/adjustaccount";

        $headers = array(
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: Bearer ".$this->token['access_token']
        );

        $data = array(
            "SubscriberNo" => $subscriber_no,
            "CommandId" => 'AdjustAccount',
            "RequestType"=> 'Event',
            "AccountType" => 2000,
            "CurrAcctChgAmt" => 2,
            "OperateType" => 2,
            'OperatorId' => 'Mife_chatbot'
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS,$data);
        echo $resp = curl_exec($curl);

        $this->record_history(array(
            $subscriber_no,
            'Adjust Account',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);
    }


}
