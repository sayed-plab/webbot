<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/11/18
 * Time: 3:04 PM
 */

namespace xrbt;

require_once 'Core.php';
require_once 'CBSADCS.php';

use \core\Core;
use \cbsadcs\CBSADCS;

class XRBT extends Core
{
    /***************************************************************************************************
     *                                              Robi
     ***************************************************************************************************/



    /***************************************    RRBT  (MyTune)  ******************************************/

    /**
     * @param $msisdn
     */
    public function rrbt_activation($msisdn)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        $url = "https://api.robi.com.bd/xrbt/v1/rrbtActivation?MSISDN=" . $subscriber_no . "&ACTIVATED_BY=MIFE&REQUEST=ACTIVATE";

        $headers = array(
            "Content-Type: application/text",
            "Authorization: Bearer " . $this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);

        $this->record_history(array(
            $this->format_msisdn($subscriber_no, 10),
            'RRBT Activation',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);

    }


    /**
     * @param $msisdn
     */
    public function rrbt_deactivation($msisdn)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        $url = "https://api.robi.com.bd/xrbt/v1/rrbtDeactivation?MSISDN=" . $subscriber_no . "&REQUEST=DEACTIVATE";

        $headers = array(
            "Content-Type: application/text",
            "Authorization: Bearer " . $this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);


        $this->record_history(array(
            $this->format_msisdn($subscriber_no, 10),
            'RRBT Deactivation',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);

    }

    /**
     * @param $msisdn
     * @param $promo_id
     */
    public function rrbt_content_subscriptions($msisdn, $promo_id)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        $url = "https://api.robi.com.bd/xrbt/v1/rrbtContentSubscriptions?MSISDN=" . $subscriber_no . "&PROMO_ID=" . $promo_id . "&REQUEST=SELECTION&SUB_TYPE=Prepaid&CATEGORY_ID=3&ISACTIVATE=TRUE&SUBSCRIPTION_CLASS=DEFAULT&CHARGE_CLASS=DEFAULT&IN_LOOP=TRUE";

        $headers = array(
            "Content-Type: application/text",
            "Authorization: Bearer " . $this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);

        $this->record_history(array(
            $this->format_msisdn($subscriber_no, 10),
            'RRBT Content Subscriptions',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);
    }


    /***************************************    RBT (Goon Goon)   ******************************************/


    /**
     * @param $msisdn
     */
    public function rbt_activation($msisdn)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        $url = "https://api.robi.com.bd/xrbt/v1/rbtActivation?MSISDN=" . $subscriber_no . "&ACTIVATED_BY=MIFE&REQUEST=ACTIVATE";

        $headers = array(
            "Content-Type: application/text",
            "Authorization: Bearer " . $this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);


        $this->record_history(array(
            $this->format_msisdn($subscriber_no, 10),
            'RBT Activation',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);

        /*
         * If Already Activated {"Resultcode":"FAILURE"}
         * {"Resultcode":"SUCCESS"}
         * {"Resultcode":"DEACTIVATION_PENDING"}
         */
    }



    /**
     * @param $msisdn
     */
    public function rbt_status($msisdn)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        $url = "https://api.robi.com.bd/xrbt/v1/rbtSubscriberStatus?MSISDN=".$subscriber_no."&request=STATUS";

        $headers = array(
            "Content-Type: application/json",
            "Authorization: Bearer " . $this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);


       $this->record_history(array(
            $this->format_msisdn($subscriber_no, 10),
            'RBT Status',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);

        /*
         * {"Resultcode":"ACTIVE"}
         * After Active but Not Success {"Resultcode":"ACT_PENDING"}
         * After Active {"Resultcode":"GRACE"}
         * After Successfully Deactivation {"Resultcode":"DEACTIVE"}
         * {"Resultcode":"DEACT_PENDING"}
         */
    }


    /**
     * @param $msisdn
     */
    public function rbt_deactivation($msisdn)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        $url = "https://api.robi.com.bd/xrbt/v1/rbtDeactivation?MSISDN=" . $subscriber_no . "&REQUEST=DEACTIVATE";

        $headers = array(
            "Content-Type: application/text",
            "Authorization: Bearer " . $this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);

        $this->record_history(array(
            $this->format_msisdn($subscriber_no, 10),
            'RBT Deactivation',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);

        /*
         * After Active Request for Deactivation {"Resultcode":"SUCCESS"}
         * If Not Request for Activation {"Resultcode":"ALREADY_INACTIVE"}
         * {"Resultcode":"DEACTIVATION_PENDING"}
         */

    }

    /**
     * @param $msisdn
     * @param $promo_id
     */
    public function rbt_content_subscriptions($msisdn, $promo_id)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        $url = "https://api.robi.com.bd/xrbt/v1/rbtContentSubscriptions?MSISDN=" . $subscriber_no . "&PROMO_ID=" . $promo_id . "&REQUEST=SELECTION&SUB_TYPE=Prepaid&CATEGORY_ID=3&ISACTIVATE=TRUE&SUBSCRIPTION_CLASS=DEFAULT&CHARGE_CLASS=DEFAULT&IN_LOOP=TRUE";

        $headers = array(
            "Content-Type: application/text",
            "Authorization: Bearer " . $this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);

        $this->record_history(array(
            $this->format_msisdn($subscriber_no, 10),
            'RBT Content Subscriptions',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);


    }












    /***********************************************************************************************************
     *                                                 AIRTEL
     **********************************************************************************************************/



    /***************************************    Airtel RRBT   ******************************************/

    /**
     * @param $msisdn
     */
    public function airtel_rrbt_activation($msisdn)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        //$url = "https://api.robi.com.bd/xrbt/v1/rrbtActivation?MSISDN=".$subscriber_no."&ACTIVATED_BY=MIFE&REQUEST=ACTIVATE";
        $url = "https://api.robi.com.bd/xrbt/v1/airtelRrbtSubscriberStatus?MSISDN=" . $subscriber_no . "&REQUEST=ACTIVATE";

        $headers = array(
            "Content-Type: application/text",
            "Authorization: Bearer " . $this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);

        $this->record_history(array(
            $this->format_msisdn($subscriber_no, 10),
            'Airtel RRBT Activation',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);

    }


    /**
     * @param $msisdn
     */
    public function airtel_rrbt_deactivation($msisdn)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        //$url = "https://api.robi.com.bd/xrbt/v1/rrbtDeactivation?MSISDN=".$subscriber_no."&REQUEST=DEACTIVATE";
        $url = "https://api.robi.com.bd/xrbt/v1/airtelRrbtDeactivation?MSISDN=" . $subscriber_no . "&REQUEST=DEACTIVATE";

        $headers = array(
            "Content-Type: application/text",
            "Authorization: Bearer " . $this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);


        $this->record_history(array(
            $this->format_msisdn($subscriber_no, 10),
            'Airtel RRBT Deactivation',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);

    }

    /**
     * @param $msisdn
     * @param $promo_id
     */
    public function airtel_rrbt_content_subscriptions($msisdn, $promo_id)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        //$url = "https://api.robi.com.bd/xrbt/v1/rrbtContentSubscriptions?MSISDN=".$subscriber_no."&PROMO_ID=".$promo_id."&REQUEST=SELECTION&SUB_TYPE=Prepaid&CATEGORY_ID=3&ISACTIVATE=TRUE&SUBSCRIPTION_CLASS=DEFAULT&CHARGE_CLASS=DEFAULT&IN_LOOP=TRUE";
        $url = "https://api.robi.com.bd/xrbt/v1/airtelRrbtContentSubscriptions?MSISDN=" . $subscriber_no . "&PROMO_ID=" . $promo_id . "&REQUEST=SELECTION&SUB_TYPE=Prepaid&CATEGORY_ID=3&ISACTIVATE=TRUE&SUBSCRIPTION_CLASS=DEFAULT&CHARGE_CLASS=DEFAULT&IN_LOOP=TRUE";

        $headers = array(
            "Content-Type: application/text",
            "Authorization: Bearer " . $this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);

        $this->record_history(array(
            $this->format_msisdn($subscriber_no, 10),
            'Airtel RRBT Content Subscriptions',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);
    }


    /***************************************   Airtel RBT  ******************************************/

    /**
     * @param $msisdn
     */
    public function airtel_rbt_activation($msisdn)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        //$url = "https://api.robi.com.bd/xrbt/v1/rbtActivation?MSISDN=".$subscriber_no."&ACTIVATED_BY=MIFE&REQUEST=ACTIVATE";
        $url = "https://api.robi.com.bd/xrbt/v1/airtelRbtActivation?MSISDN=" . $subscriber_no . "&REQUEST=ACTIVATE";

        $headers = array(
            "Content-Type: application/text",
            "Authorization: Bearer " . $this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);


        $this->record_history(array(
            $this->format_msisdn($subscriber_no, 10),
            'RBT Activation',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);

    }


    /**
     * @param $msisdn
     */
    public function airtel_rbt_deactivation($msisdn)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        //$url = "https://api.robi.com.bd/xrbt/v1/rbtDeactivation?MSISDN=".$subscriber_no."&REQUEST=DEACTIVATE";
        $url = "https://api.robi.com.bd/xrbt/v1/airtelRbtDeactivation?MSISDN=" . $subscriber_no . "&REQUEST=DEACTIVATE";

        $headers = array(
            "Content-Type: application/text",
            "Authorization: Bearer " . $this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);

        $this->record_history(array(
            $this->format_msisdn($subscriber_no, 10),
            'RBT Deactivation',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);

    }

    /**
     * @param $msisdn
     * @param $promo_id
     */
    public function airtel_rbt_content_subscriptions($msisdn, $promo_id)
    {
        $subscriber_no = $this->format_msisdn($msisdn, 10);

        //$url = "https://api.robi.com.bd/xrbt/v1/rbtContentSubscriptions?MSISDN=".$subscriber_no."&PROMO_ID=".$promo_id."&REQUEST=SELECTION&SUB_TYPE=Prepaid&CATEGORY_ID=3&ISACTIVATE=TRUE&SUBSCRIPTION_CLASS=DEFAULT&CHARGE_CLASS=DEFAULT&IN_LOOP=TRUE";
        $url = "https://api.robi.com.bd/xrbt/v1/airtelRbtContentSubscriptions?MSISDN=" . $subscriber_no . "&PROMO_ID=" . $promo_id . "&REQUEST=SELECTION&SUB_TYPE=Prepaid&CATEGORY_ID=3&ISACTIVATE=TRUE&SUBSCRIPTION_CLASS=DEFAULT&CHARGE_CLASS=DEFAULT&IN_LOOP=TRUE";

        $headers = array(
            "Content-Type: application/text",
            "Authorization: Bearer " . $this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);

        $this->record_history(array(
            $this->format_msisdn($subscriber_no, 10),
            'Airtel RBT Content Subscriptions',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);


    }
}

