<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/11/18
 * Time: 10:19 AM
 */

namespace fnf;

require_once 'Core.php';

use \core\Core;

/**
 * Class FnF
 * @package fnf
 */

class FnF extends Core{

    /**
     * @param $msisdn
     */
    public function get_fnf($msisdn)
    {
        $SubscriberNo = $this->format_msisdn($msisdn, 13);

        $url = "https://api.robi.com.bd/fnf/v1/getFNFList?msisdn=".$SubscriberNo;

        $headers = array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $api_data = curl_exec($curl);

        $this->record_history(array(
            $this->format_msisdn($msisdn, 10),
            'Get FnF',
            $api_data,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);
    }

    /**
     * @param $msisdn
     * @param $msisdn_fnf
     */
    public function add_fnf($msisdn,$msisdn_fnf)
    {
        $SubscriberNo = $this->format_msisdn($msisdn, 13);
        $msisdn_fnf = explode(' ',$this->format_msisdn($msisdn_fnf, 13));

        $url = "https://api.robi.com.bd/fnf/v1/addFNF?msisdn=".$SubscriberNo."&msisdnFNFList=".$msisdn_fnf[0];

        $headers = array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);

        $this->record_history(array(
            $this->format_msisdn($SubscriberNo, 10),
            'Add FnF',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);
    }

    /**
     * @param $msisdn
     * @param $msisdn_fnf
     */
    public function delete_fnf($msisdn,$msisdn_fnf)
    {
        $SubscriberNo = $this->format_msisdn($msisdn, 13);
        $msisdn_fnf = $this->format_msisdn($msisdn_fnf, 13);

        $url = "https://api.robi.com.bd/fnf/v1/deleteFNF?msisdn=".$SubscriberNo."&msisdnFnF=".$msisdn_fnf;

        $headers = array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);

         $this->record_history(array(
             $this->format_msisdn($SubscriberNo, 10),
             'Delete FnF',
             $resp,
             date('Y-m-d H:i:s')

         ));

        curl_close($curl);

    }

    /**
     * @param $msisdn
     * @param $msisdn_partner
     */

    public function add_partner($msisdn,$msisdn_partner)
    {
        $SubscriberNo = $this->format_msisdn($msisdn, 13);
        $msisdn_partner = $this->format_msisdn($msisdn_partner, 13);

        $url = "https://api.robi.com.bd/fnf/v1/addPartner?msisdn=".$SubscriberNo."&msisdnPartner=".$msisdn_partner;

        $headers = array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$this->token['access_token']
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        echo $resp = curl_exec($curl);

        $this->record_history(array(
            $this->format_msisdn($SubscriberNo, 10),
            'Add Partner',
            $resp,
            date('Y-m-d H:i:s')

        ));

        curl_close($curl);

    }


}