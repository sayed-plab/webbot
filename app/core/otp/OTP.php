<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/11/18
 * Time: 10:19 AM
 */

date_default_timezone_set('Asia/Dhaka');

class OTP {

    protected $username='preneur';
    protected $password='Abcd@1234';
    protected $from_number='airtel BOT';
    protected $SubscriberNo = null;
    protected $db;

    public function db_config($config)
    {
        $this->db = $config;
    }

    public function FormatMSISDN($msisdn, $length)
    {

        switch ($length)
        {
            case 10:
                $start = 0;
                if(strlen($msisdn) == 14){
                    $start = 4;
                }
                elseif(strlen($msisdn) == 13){
                    $start = 3;
                }
                elseif(strlen($msisdn) == 11){
                    $start = 1;
                }
                return substr($msisdn, $start);
                break;

            case 13:
                if(strlen($msisdn) == 14){
                    $format_msisdn = substr($msisdn, 1);
                }
                elseif(strlen($msisdn) == 11){
                    $format_msisdn = '88'.$msisdn;
                }
                elseif(strlen($msisdn) == 10){
                    $format_msisdn = '880'.$msisdn;
                }
                else
                {
                    $format_msisdn = $msisdn;
                }
                return $format_msisdn;
                break;
        }

    }


    public function StoreOPTResponse($values)
    {
        $sql = "INSERT INTO otp_responses (user_id,msisdn,query,message_id,otp_code,validity,issue_date) VALUES(?,?,?,?,?,?,?)";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute($values);
    }

    public function CheckOTPValidity($user_id, $msisdn, $query)
    {
        $sql = "SELECT * FROM otp_responses WHERE user_id=? AND msisdn=? AND query=? ORDER BY id DESC";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($user_id,$msisdn,$query));
        $res = $stmt->fetch(PDO::FETCH_ASSOC);

        if(date("Y-m-d H:i:s") <= $res['validity'])
        {
            return true;
        }
        else
        {
            return false;
        }

    }


    public function TokenVerification($user_id,$msisdn,$query,$otp_code)
    {
        if(!empty($user_id) && !empty($msisdn) && !empty($query) && !empty($otp_code))
        {
            $sql = "SELECT * FROM otp_responses WHERE user_id=? AND msisdn=? AND query=? AND otp_code =? ORDER BY id DESC";
            $stmt = $this->db->prepare($sql);
            $stmt->execute(array($user_id, $this->FormatMSISDN($msisdn, 13), $query, $otp_code));
            $res = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($res['otp_code'] == $otp_code) {

                if(date("Y-m-d H:i:s") <= $res['validity'])
                {
                    echo json_encode(array('status' => 'valid'));
                }
                else
                {
                    echo json_encode(array('status' => 'expired'));
                }
            } else {

                echo json_encode(array('status' => 'wrong'));
            }

        }
        else
        {
            echo json_encode(array('status' => 'empty params'));
        }

    }

    public function SendOTPToken($user_id,$msisdn,$query)
    {
        if(!empty($user_id) && !empty($msisdn) && !empty($query))
        {
            if($this->CheckOTPValidity($user_id,$this->FormatMSISDN($msisdn, 13),$query) == true)
            {
                echo json_encode(array('status' => 'already_valid'));
            }
            else
            {
                $OTP = rand(50000,90000);
                $this->SubscriberNo = $this->FormatMSISDN($msisdn, 13);
                $url = "https://api.mobireach.com.bd/SendTextMessage";


                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS,
                    "Username=".$this->username."&Password=".$this->password."&From=".$this->from_number."&To=".$this->SubscriberNo."&Message=Your verification code is: ".$OTP.". Use this code to verify. To avoid misuse, please do not forward or share this code with others.");

                $xml = curl_exec($curl);
                $xml_string = simplexml_load_string($xml);
                $json = json_encode($xml_string);

                curl_close($curl);

                $api_data = json_decode($json,TRUE);

                if($api_data['ServiceClass']['StatusText'] == "success")
                {
                    $date = date("Y-m-d H:i:s");
                    $currentDate = strtotime($date);
                    $futureDate = $currentDate+(60*5);
                    $validity = date("Y-m-d H:i:s", $futureDate);

                    $values = array(
                        $user_id,
                        $this->SubscriberNo,
                        $query,
                        $api_data['ServiceClass']['MessageId'],
                        $OTP,
                        $validity,
                        date('Y-m-d')
                    );

                    $this->StoreOPTResponse($values);

                    echo json_encode(array('status' => 'sent'));
                }
                else
                {
                    echo json_encode(array('status' => 'failed'));
                }
            }
        }
        else
        {
            echo json_encode(array('status' => 'empty params'));
        }

    }

}