<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 1/31/19
 * Time: 11:43 AM
 */

require_once 'db/db.php';

class WebBot {

    private $db;

    public function __construct(){
        global $db;
        $this->db = $db;
    }

    public function isAirtel($msisdn) {

        $code = null;

        if (strlen($msisdn) == 11) {
            $code = substr($msisdn, 0, 3);
        } elseif (strlen($msisdn) == 13) {
            $code = substr($msisdn, 2, 3);
        } elseif (strlen($msisdn) == 14) {
            $code = substr($msisdn, 3, 3);
        }

        if(strlen($msisdn) >= 11 && strlen($msisdn) <=14  && $code != '018') {
            return true;
        } else {
            return false;
        }
    }

    public function levenshteinDiff($search) {

        $search_keywords = explode(" ", $search);
        $text = '';

        $sql = "SELECT * FROM query_response";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $query_responses = $stmt->fetchAll(2);

        foreach ($search_keywords as $search_keyword) {

            $tmp_keyword = '';
            $found = false;

            foreach ($query_responses as $query_response) {

                $keywords = explode(" ", $query_response['keywords']);

                foreach ($keywords as $keyword) {
                    if (levenshtein(strtolower(trim($keyword)), strtolower(trim($search_keyword))) == 1) {
                        $found = true;
                        $tmp_keyword = trim($keyword);
                    } elseif (levenshtein(strtolower(trim($keyword)), strtolower(trim($search_keyword))) == 0) {
                        $found = true;
                        $tmp_keyword = trim($search_keyword);
                    }
                }
            }

            if (!$found) {
                $text = $text.$search_keyword." ";
            } else {
                $text = $text.$tmp_keyword." ";
            }


        }

        return $text;

    }

    public function matchAll($search) {

        //$levenshteinSearch = $this->levenshteinDiff($search);
        $sql = "SELECT * FROM query_response WHERE MATCH(keywords) AGAINST ('".$search."' IN NATURAL LANGUAGE MODE);";

//        $sql = "SELECT * FROM query_response WHERE MATCH(keywords) AGAINST ('\"".$search."\"' IN BOOLEAN MODE);";

        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(2);

    }

    public function matchFromSmallTalks($search) {

        $sql = "SELECT * FROM small_talks WHERE MATCH(keywords) AGAINST ('$search' IN NATURAL LANGUAGE MODE);";
     //   $sql = "SELECT * FROM small_talks WHERE MATCH(keywords) AGAINST ('\"".$search."\"' IN BOOLEAN MODE);";

        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(2);

    }

    public function matchFromRetrain($search) {

        $search = urlencode($search);
        $token = "3c27f99287b686e9c90b672eda7b2674";
        $url = "https://airtelbot.forbangladesh.com/airtel/retrain/api/?search=$search&token=$token";
        $ch = curl_init($url);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($data, true);
        return $response['param'] ?? null;
    }
    

    public function isBengali($str) {
        if (mb_detect_encoding($str) == 'UTF-8') {
            return true;
        } else {
            return false;
        }
    }


    public function differEngBang($search) {

        if ($this->isBengali($search)) {

            return 'bn';
        } else {

            $isBengali = false;

            foreach (explode(' ', $search) as $word) {

                $sql = "SELECT * FROM diff_bang_eng WHERE keywords LIKE :key";
                $stmt = $this->db->prepare($sql);
                $stmt->execute(array(':key' => "% ".$word." %"));

                if ($stmt->rowCount()) {
                    $isBengali = true;
                }
            }

            if ($isBengali) {
                return 'bn';
            } else {
                return 'en';
            }

        }

    }

    public function matchBelowTwo($search) {

        $sql = "SELECT * FROM query_response WHERE keywords LIKE :key";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array(':key' => "%$search%"));
        return $stmt->fetch(2);
    }

    public function nlpEngine($search) {

        date_default_timezone_set("Asia/Dhaka");
        $dateTime = date('Y-m-d H:i:s');

        if (!empty($response = $this->matchAll($search))) {

            $lang = $this->differEngBang($search);
            return json_encode(array("lang" => $lang, "response" =>$response), JSON_UNESCAPED_UNICODE);
        } elseif (!empty($response = $this->matchAll($this->matchFromRetrain($search)))) {

            $lang = $this->differEngBang($search);
            return json_encode(array("lang" => $lang, "response" =>$response), JSON_UNESCAPED_UNICODE);
        } elseif (!empty($response = $this->matchFromSmallTalks($search))) {

            $lang = $this->differEngBang($search);
            return json_encode(array("lang" => $lang, "response" =>$response), JSON_UNESCAPED_UNICODE);
        } else {

            $lang = $this->differEngBang($search);
            $response = $this->matchAll('default');
            $this->recordBotCanNotReplyUserMessage(array($search,$dateTime));
            return json_encode(array("lang" => $lang, "response" =>$response), JSON_UNESCAPED_UNICODE);

        }
    }

    public function initDailyAnalyticsData() {

        date_default_timezone_set("Asia/Dhaka");
        $date = date('Y-m-d');

        $sql = "SELECT * FROM `daily_analytics` WHERE date = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($date));

        if (!$stmt->rowCount()) {

            $sql = "INSERT INTO `daily_analytics` (date) VALUES(?)";
            $stmt = $this->db->prepare($sql);
            return $stmt->execute(array($date));

        }

    }

    function updateDailyAnalytics($column) {
        date_default_timezone_set("Asia/Dhaka");
        $date = date('Y-m-d');

        $this->initDailyAnalyticsData();

        switch ($column) {

            case 'recharge':
                $sql = "UPDATE daily_analytics SET recharge = recharge + 1 WHERE date = ?";
                $stmt = $this->db->prepare($sql);
                $stmt->execute(array($date));
                break;
            case 'data_purchase':
                $sql = "UPDATE daily_analytics SET data_purchase = data_purchase + 1 WHERE date = ?";
                $stmt = $this->db->prepare($sql);
                $stmt->execute(array($date));
                break;
        }
    }


    public function userChatHistoryRaw($userChat) {

        $sql = "INSERT INTO user_chatting_raw (user, query, response, dateTime) VALUES(?, ?, ?, ?)";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute($userChat);

    }

    public function recordUserMessage($message) {

        $sql = "INSERT INTO messages (user,query,response,date) VALUES(?,?,?,?)";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute($message);
    }

    public function recordBotCanNotReplyUserMessage($message) {

        $sql = "INSERT INTO `bot_can_not_reply` (query,date) VALUES(?,?)";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute($message);
    }
    
    public function savePayment($values) {
		$sql = "INSERT INTO save_payment VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		$stmt = $this->db->prepare($sql);
		$stmt->execute($values);
    }

    public function dataPurchase($info) {
        $sql = "INSERT INTO data_purchase (user, msisdn, plan, price, status, date) VALUES(?,?,?,?,?,?)";
        $stmt = $this->db->prepare($sql);
        $stmt->execute($info);
    }

    public function lastMessages($user) {

        $sql = "(SELECT * FROM `user_chatting_raw` WHERE user=? ORDER BY id DESC LIMIT 0,50) ORDER BY id ASC";
        $stmt1 = $this->db->prepare($sql);
        $stmt1->execute(array($user));
        return $stmt1->fetchAll(2);

    }

    public function totalMessagesCount($user) {

        $sql = "SELECT id FROM `user_chatting_raw` WHERE user=?";
        $stmt1 = $this->db->prepare($sql);
        $stmt1->execute(array($user));
        return $stmt1->rowCount();

    }

    public function getCarousel($mapping) {

        $sql = "SELECT * FROM `carousels` WHERE `mapping` = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($mapping));
        return $stmt->fetchAll(2);
    }

    public function getButtons($mapping) {

        $sql = "SELECT * FROM `buttons` WHERE mapping = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($mapping));
        return $stmt->fetchAll(2);
    }

    public function getQuickReplies($mapping) {

        $sql = "SELECT * FROM `quick_replies` WHERE mapping = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($mapping));
        return $stmt->fetchAll(2);
    }

    public function getApiIssue($user_id) {

        $sql = "SELECT * FROM `api_issue` WHERE user_id = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($user_id));

        return $stmt->fetch(2);
    }

    public function getApiIssueUserIdByMSISDN($msisdn) {

        $sql = "SELECT user_id FROM `api_issue` WHERE msisdn = ? ORDER BY id DESC";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($msisdn));
        $res = $stmt->fetch(2);

        return $res['user_id'];
    }

    public function updateApiIssueMSISDN($msisdn, $user_id) {

        $sql = "UPDATE `api_issue` SET msisdn=? WHERE user_id = ?";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute(array($msisdn, $user_id));
    }

    public function updateApiIssueISSUE($issue, $user_id) {

        $sql = "UPDATE `api_issue` SET issue=? WHERE user_id = ?";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute(array($issue, $user_id));
    }

    public function updateApiIssueAddFnF($add_fnf_msisdn, $user_id) {

        $sql = "UPDATE `api_issue` SET add_fnf=? WHERE user_id = ?";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute(array($add_fnf_msisdn, $user_id));
    }

    public function updateApiIssuePlan($plan, $price, $user_id) {

        $sql = "UPDATE `api_issue` SET plan=?, price=? WHERE user_id = ?";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute(array($plan, $price, $user_id));
    }

    public function updateApiIssueLang($lang, $user_id) {

        $sql = "UPDATE `api_issue` SET lang=? WHERE user_id = ?";
        $stmt = $this->db->prepare($sql);
        return $stmt->execute(array($lang, $user_id));
    }

    public function getAllowedPlan($plan) {

        $sql = "SELECT * FROM `allowed_plans` WHERE plan =?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($plan));
        return $stmt->fetch(2);
    }

    public function recordApiIssue($values) {

        $sql = "SELECT * FROM `api_issue` WHERE user_id = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($values['user_id']));

        if (!$stmt->rowCount()) {

           // $sql = "INSERT INTO api_issue (".implode(array_keys($values)).") VALUES(?, ?, ?, ?, ?, ?)";


            $params = array_fill(0, count($values), '?');
            $sql = "INSERT INTO `api_issue` (".implode(', ', array_keys($values)).") VALUES(" . implode(',', $params) . ")";

            $stmt = $this->db->prepare($sql);
            return $stmt->execute(array_values($values));

        } else {

            $sql = "UPDATE api_issue SET issue=? WHERE user_id =?" ;
            $stmt = $this->db->prepare($sql);
            return $stmt->execute(array($values['issue'], $values['user_id']));
        }

    }

    public function message($recipientId, $query, $responseText) {

        $data = array(
            "recipient" => array(
                "id" => $recipientId,
                "query" => $query
            ),
            "message" => array(
                "text" => $responseText
            )
        );

        return json_encode(
            array("chat" =>
                array(
                    "query" => $query,
                    "response" => $responseText
                ),
                "data" => $data),
            JSON_UNESCAPED_UNICODE);
    }


    public function payInput($recipientId, $query, $responseText, $buttons) {

        $data = array(
            "recipient" => array(
                "id" => $recipientId,
                "query" => $query
            ),
            "message" => array(
                "attachment" => array(
                    "type" => "payInput",
                    "elements" => array(
                        "text" => $responseText,
                        "buttons" => $buttons
                    )

                )
            )
        );

        return json_encode(
            array("chat" =>
                array(
                    "query" => $query,
                    "response" => $responseText
                ),
                "data" => $data),
            JSON_UNESCAPED_UNICODE);
    }


    public function quickReplies($recipientId, $query, $responseText, $buttons) {

        $data = array(
            "recipient" => array(
                "id" => $recipientId,
                "query" => $query
            ),
            "message" => array(
                "attachment" => array(
                    "type" => "quickReplies",
                    "elements" => array(
                        "text" => $responseText,
                        "buttons" => $buttons
                    )

                )
            )
        );

        return json_encode(
            array("chat" =>
                array(
                    "query" => $query,
                    "response" => $responseText
                ),
                "data" => $data),
            JSON_UNESCAPED_UNICODE);
    }

    public function carousel($recipientId, $query, $responseText, $sliders, $buttons = array()) {

        $data = array(
            "recipient" => array(
                "id" => $recipientId,
                "query" => $query
            ),
            "message" => array(
                "attachment" => array(
                    "type" => "carousel",
                    "elements" => array(
                        "text" => $responseText,
                        "sliders" => $sliders,
                        "buttons" => $buttons
                    )

                )
            )
        );

        return json_encode(
            array("chat" =>
                array(
                    "query" => $query,
                    "response" => $responseText
                ),
                "data" => $data),
            JSON_UNESCAPED_UNICODE);

    }

}
