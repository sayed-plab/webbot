<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 10/15/18
 * Time: 2:21 PM
 */

namespace visa;

require_once 'curl.php';

use simple_curl\curl;

class Visa extends curl {

    public static function init($mobile, $amount, $category, $name = '', $email = '') {

        $url = 'https://robi.portwallet.com/api/v1';

        $headers = array(

        );

        $app_key = '2835a4dfda56173391dfd214d6794fa4';
        $secret_key = '5cd71577676104c0fddc8da258f2aea7';
        $timestamp = time();
        $token = md5($secret_key.$timestamp);
        $name = !empty($name) ? $name : $mobile;
        $email = !empty($email) ? $email : "$mobile@airtel.com.bd";
        $redirect_url = 'https://airtelbot.forbangladesh.com/webbot/app/confirmation.php';
        $query = array(
            'app_key' => (string)$app_key,
            'timestamp' => (int)$timestamp,
            'token' => (string)$token,
            'amount' => (double)$amount,
            'name' => (string)$name,
            'email' => (string)$email,
            'mobile' => (string)$mobile,
            'category' => (string)$category,
            'redirect_url' => (string)$redirect_url,
        );

        curl::prepare($url,$headers,$query);
        curl::exec_post();
        return curl::get_response_assoc();

    }

}
