-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 22, 2019 at 06:48 AM
-- Server version: 5.6.30-1
-- PHP Version: 7.2.4-1+b1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web_bot`
--

-- --------------------------------------------------------

--
-- Table structure for table `allowed_plans`
--

CREATE TABLE `allowed_plans` (
  `id` int(11) NOT NULL,
  `who` varchar(200) NOT NULL,
  `plan` varchar(111) NOT NULL,
  `price` varchar(255) NOT NULL,
  `pack_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `api_history`
--

CREATE TABLE `api_history` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(255) NOT NULL,
  `query` text NOT NULL,
  `response` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `api_issue`
--

CREATE TABLE `api_issue` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `msisdn` varchar(255) NOT NULL,
  `add_fnf` varchar(255) DEFAULT NULL,
  `issue` varchar(255) NOT NULL,
  `plan` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `lang` varchar(255) DEFAULT NULL,
  `date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bot_can_not_reply`
--

CREATE TABLE `bot_can_not_reply` (
  `id` int(11) NOT NULL,
  `query` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `buttons`
--

CREATE TABLE `buttons` (
  `id` int(11) NOT NULL,
  `mapping` varchar(255) NOT NULL,
  `btn_type` varchar(255) NOT NULL,
  `url_or_title` longtext NOT NULL,
  `title_or_payload` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `carousels`
--

CREATE TABLE `carousels` (
  `id` int(11) NOT NULL,
  `mapping` varchar(255) NOT NULL,
  `active` varchar(255) NOT NULL,
  `title` longtext NOT NULL,
  `subtitle` longtext NOT NULL,
  `image_url` longtext NOT NULL,
  `buttons` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8 NOT NULL,
  `location` varchar(255) CHARACTER SET utf8 NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `last_date` varchar(255) NOT NULL,
  `last_time` varchar(255) NOT NULL,
  `survey` longtext CHARACTER SET utf8,
  `status` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `daily_analytics`
--

CREATE TABLE `daily_analytics` (
  `id` int(11) NOT NULL,
  `data_purchase` int(11) NOT NULL,
  `recharge` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_purchase`
--

CREATE TABLE `data_purchase` (
  `id` int(11) NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `msisdn` varchar(255) NOT NULL,
  `plan` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `diff_bang_eng`
--

CREATE TABLE `diff_bang_eng` (
  `id` int(11) NOT NULL,
  `keywords` longtext CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `query` text NOT NULL,
  `response` longtext NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `otp_responses`
--

CREATE TABLE `otp_responses` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `msisdn` varchar(255) NOT NULL,
  `query` varchar(255) NOT NULL,
  `message_id` varchar(255) NOT NULL,
  `otp_code` varchar(255) NOT NULL,
  `validity` varchar(255) NOT NULL,
  `issue_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `query_response`
--

CREATE TABLE `query_response` (
  `id` int(11) NOT NULL,
  `greet_en` varchar(255) NOT NULL,
  `greet_bn` varchar(255) NOT NULL,
  `intent` varchar(255) NOT NULL,
  `param_en` varchar(255) NOT NULL,
  `keywords` varchar(1000) DEFAULT NULL,
  `reply_text_en` longtext NOT NULL,
  `reply_text_bn` longtext NOT NULL,
  `carousels` varchar(255) NOT NULL,
  `buttons` longtext NOT NULL,
  `quick_replies` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quick_replies`
--

CREATE TABLE `quick_replies` (
  `id` int(11) NOT NULL,
  `mapping` varchar(255) NOT NULL,
  `btn_type` varchar(255) NOT NULL,
  `url_or_title` longtext NOT NULL,
  `title_or_payload` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `retrain`
--

CREATE TABLE `retrain` (
  `id` int(11) NOT NULL,
  `greet_en` varchar(255) NOT NULL,
  `greet_bn` varchar(255) NOT NULL,
  `intent` varchar(255) NOT NULL,
  `param_en` varchar(255) NOT NULL,
  `keywords` varchar(1000) DEFAULT NULL,
  `reply_text_en` longtext NOT NULL,
  `reply_text_bn` longtext NOT NULL,
  `carousels` varchar(255) NOT NULL,
  `buttons` longtext NOT NULL,
  `quick_replies` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `save_payment`
--

CREATE TABLE `save_payment` (
  `id` int(11) NOT NULL,
  `status` longtext NOT NULL,
  `amount` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `invoice` longtext NOT NULL,
  `date` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `timestamp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `small_talks`
--

CREATE TABLE `small_talks` (
  `id` int(11) NOT NULL,
  `greet_en` varchar(255) NOT NULL,
  `greet_bn` varchar(255) NOT NULL,
  `intent` varchar(255) NOT NULL,
  `param_en` varchar(255) NOT NULL,
  `keywords` varchar(1000) NOT NULL,
  `reply_text_en` longtext NOT NULL,
  `reply_text_bn` longtext NOT NULL,
  `carousels` varchar(255) NOT NULL,
  `buttons` longtext NOT NULL,
  `quick_replies` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_chatting_raw`
--

CREATE TABLE `user_chatting_raw` (
  `id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  `response` longtext NOT NULL,
  `dateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `words`
--

CREATE TABLE `words` (
  `id` int(11) NOT NULL,
  `main_word` varchar(255) NOT NULL,
  `synonyms` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allowed_plans`
--
ALTER TABLE `allowed_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_history`
--
ALTER TABLE `api_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_issue`
--
ALTER TABLE `api_issue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bot_can_not_reply`
--
ALTER TABLE `bot_can_not_reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buttons`
--
ALTER TABLE `buttons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carousels`
--
ALTER TABLE `carousels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- Indexes for table `daily_analytics`
--
ALTER TABLE `daily_analytics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_purchase`
--
ALTER TABLE `data_purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diff_bang_eng`
--
ALTER TABLE `diff_bang_eng`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otp_responses`
--
ALTER TABLE `otp_responses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `query_response`
--
ALTER TABLE `query_response`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `query_response` ADD FULLTEXT KEY `keywords` (`keywords`);
ALTER TABLE `query_response` ADD FULLTEXT KEY `keywords_2` (`keywords`);
ALTER TABLE `query_response` ADD FULLTEXT KEY `keywords_3` (`keywords`);

--
-- Indexes for table `quick_replies`
--
ALTER TABLE `quick_replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `retrain`
--
ALTER TABLE `retrain`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `retrain` ADD FULLTEXT KEY `keywords` (`keywords`);
ALTER TABLE `retrain` ADD FULLTEXT KEY `keywords_2` (`keywords`);
ALTER TABLE `retrain` ADD FULLTEXT KEY `keywords_3` (`keywords`);

--
-- Indexes for table `save_payment`
--
ALTER TABLE `save_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `small_talks`
--
ALTER TABLE `small_talks`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `small_talks` ADD FULLTEXT KEY `keywords` (`keywords`);
ALTER TABLE `small_talks` ADD FULLTEXT KEY `keywords_2` (`keywords`);
ALTER TABLE `small_talks` ADD FULLTEXT KEY `keywords_3` (`keywords`);
ALTER TABLE `small_talks` ADD FULLTEXT KEY `keywords_4` (`keywords`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_chatting_raw`
--
ALTER TABLE `user_chatting_raw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `words`
--
ALTER TABLE `words`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allowed_plans`
--
ALTER TABLE `allowed_plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `api_history`
--
ALTER TABLE `api_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `api_issue`
--
ALTER TABLE `api_issue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bot_can_not_reply`
--
ALTER TABLE `bot_can_not_reply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `buttons`
--
ALTER TABLE `buttons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `carousels`
--
ALTER TABLE `carousels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `daily_analytics`
--
ALTER TABLE `daily_analytics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `data_purchase`
--
ALTER TABLE `data_purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `diff_bang_eng`
--
ALTER TABLE `diff_bang_eng`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `otp_responses`
--
ALTER TABLE `otp_responses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `query_response`
--
ALTER TABLE `query_response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quick_replies`
--
ALTER TABLE `quick_replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `retrain`
--
ALTER TABLE `retrain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `save_payment`
--
ALTER TABLE `save_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `small_talks`
--
ALTER TABLE `small_talks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_chatting_raw`
--
ALTER TABLE `user_chatting_raw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `words`
--
ALTER TABLE `words`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
