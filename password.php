<script language="javascript">

function passwordChanged() {

    let strength = document.getElementById('strength');
    let strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
    let mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
    let enoughRegex = new RegExp("(?=.{8,}).*", "g");
    let pwd = document.getElementById("password");

    if (pwd.value.length==0) {
        strength.innerHTML = 'Type Password';
    } else if (pwd.value.length < 8) {
        strength.innerHTML = 'Password must be greater than 8 length';
    } else if (false == enoughRegex.test(pwd.value)) {
        strength.innerHTML = 'More Characters';
    } else if (strongRegex.test(pwd.value)) {
            strength.innerHTML = '<span style="color:green">Strong!</span>';
    } else if (mediumRegex.test(pwd.value)) {
            strength.innerHTML = '<span style="color:orange">Medium!</span>';
    } else {
            strength.innerHTML = '<span style="color:red">Weak!</span>';
    }
}
</script>

<form action="" method="post">

    <input name="password" id="password" type="text" size="15" maxlength="20" onkeyup="return passwordChanged();" />
    <span id="strength">Type Password</span>
    <button type="submit" value="Submit">Submit</button>
</form>




<?php

function checkPassword($pwd) {

    $errors = 'passed';

    if (strlen($pwd) < 8) {
        $errors = "Password too short!";
    }

    if (!preg_match("#[0-9]+#", $pwd)) {
        $errors = "Password must include at least one number!";
    }

    if (!preg_match("#[a-z]+#", $pwd)) {
        $errors = "Password must include at least one small letter!";
    }

    if (!preg_match("#[A-Z]+#", $pwd)) {
        $errors = "Password must include at least one capital letter!";
    }

    if (!preg_match("#[\W]+#", $pwd)) {
        $errors = "Password must include at least one Special Character!";
    }
    return $errors;
}

if (isset($_POST['password'])) {

    $pwd = $_POST['password'];
    if(checkPassword($pwd) == 'passed') {
        echo 'OK';
    }
}


?>