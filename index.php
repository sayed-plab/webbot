<?php

require_once 'app/inc/common.php';
require_once 'app/core/WebBot.php';
require_once 'app/core/Users.php';

session();

$bot = new WebBot();
$user = new Users();
$user_name = $user->getUserName($_SESSION['user']);

if (!isset($_SESSION['user'])) {
    header('Location: user.php');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Airtel Buzz Live Chat">
    <meta name="author" content="Airtel Buzz">
    <meta name="generator" content="Airtel Buzz">
    <link rel="icon" href="https://www.bd.airtel.com/static/favicon.ico" />
    <title>Live Chat</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css'>
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.2/css/font-awesome.min.css'>
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="css/style_olf.css">
</head>
<body>

<audio id="myAudio">
  <source src="sound.mp3" type="audio/mpeg">
</audio>

<div id="frame">

    <div class="content">
        <div class="contact-profile">
            <img src="https://forbangladesh.com/airtel_messengerx/dashboardx/assets/img/logo.png" alt="" />
            <p>Live Chat - (Welcome <?= $user_name ?>)</p>
            <div class="social-media">
                <a href="logout.php" class="logout">Logout<i class="fa fa-sign-out" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="messages">
            <ul>
                <!--	<li class="sent">
                        <img src="'+repliesImg+'" alt="" />
                        <p>How the hell am I supposed to get a jury to believe you when I am not even sure that I do?!</p>
                    </li>
                    <li class="replies">
                        <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                        <p>When you're backed against the wall, break the god damn thing down.</p>
                    </li>
                    <li class="replies">
                        <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                        <p>Excuses don't win championships.</p>
                    </li>
                    <li class="sent">
                        <img src="'+repliesImg+'" alt="" />
                        <p>Oh yeah, did Michael Jordan tell you that?</p>
                    </li>
                    <li class="replies">
                        <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                        <p>No, I told him that.</p>
                    </li>
                    <li class="replies">
                        <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                        <p>What are your choices when someone puts a gun to your head?</p>
                    </li>
                    -->
                <!--<li class="sent">
                    <img src="'+repliesImg+'" alt="" />
                    <p>Get Started</p>
                </li>


-->

                <!--<li class="replies">
                    <img class="img" src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                    <p>
                        Hi, Mohammad. This is Hridi from Airtel. Welcome to the #1 Network of Friends. <br>Hi, Mohammad. আমি হৃদি এয়ারটেল থেকে বলছি। বন্ধুদের #১ নেটওয়ার্কে আপনাকে স্বাগতম| Please select a language.
                    </p>

                    <div id="carouselExample" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="https://api.bd.airtel.com/uploads/2019/01/a61ab23c-3c07-40b0-a692-b733c9c546d4.jpg" class="d-block c_slider" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="https://api.bd.airtel.com/uploads/2019/01/a61ab23c-3c07-40b0-a692-b733c9c546d4.jpg" class="d-block c_slider" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="https://api.bd.airtel.com/uploads/2018/09/fb4c4be8-9574-4f68-a4a3-b2984e795269.jpg" class="d-block c_slider" alt="...">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                </li>-->
            </ul>
        </div>
        <div class="message-input">
            <div class="wrap">
                <input id="submit-btn" type="text" placeholder="Write your message..." autofocus="autofocus"/>
                <button class="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-73942782-5"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-73942782-5');
</script>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

<!-- New embed code for asynchronous font loading -->
<script src="https://use.typekit.net/hoy3lrg.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

<script >

    $(document).ready(function(){

        let text = "";
        let payload = "";
        let sender_id = <?php print $_SESSION['user'] ?>;
        let reloadCount = 0;
        let senderImg = "https://forbangladesh.com/airtel_messengerx/dashboardx/assets/img/logo.png";
        let repliesImg = "http://emilcarlsson.se/assets/mikeross.png";
        
        function audioPlay() {
          document.getElementById("myAudio").play();
        }
        
        function reload() {

            $.ajax({
                url: "reload.php",
                data: {
                    sender_id: sender_id,
                },
                type: "POST",
            })

            .done(function (data) {

                let responseCount;

                responseCount = JSON.parse(data.count);

                if (responseCount !== reloadCount) {
                    
                     //audioPlay();
                    $('.messages ul').empty();

                    data.content.forEach(function(entry) {
                       // console.log(JSON.parse(entry));
                        sendReply(JSON.parse(entry));

                    });

                    reloadCount = responseCount;
                    console.log(reloadCount);

                    setTimeout(function(){$(".messages").animate({scrollTop: $('.messages').get(0).scrollHeight}, "fast")}, 500);
                }

            })

        }


        (function reloadFun() {
            reload();
            setTimeout(reloadFun, 500);
        })();
        
        function sendReply(data) {
            
             
            if (data.message.hasOwnProperty("text")) {

                if (data.recipient.hasOwnProperty("query")) {
                    $('<li class="replies"><p>' + data.recipient.query + '</p></li>').appendTo($('.messages ul'));
                }

                $('<li class="sent"><img class="img" src="'+senderImg+'" alt="" /><p>' + data.message.text + '</p></li>').appendTo($('.messages ul'));

            } else if (data.message.hasOwnProperty("attachment")) {

                if (data.message.attachment.type === "payInput") {

                    let buttons = '';

                    for(let key in data.message.attachment.elements.buttons){

                        if (data.message.attachment.elements.buttons[key]['type'] === 'web_url') {

                            buttons += '<a class="pay_input pay_url" target="_blank"  href="'+data.message.attachment.elements.buttons[key]['url']+'">'+data.message.attachment.elements.buttons[key]['title']+'</a>';
                        } else {
                            buttons += '<input class="pay_input" type="button" data-payload="'+data.message.attachment.elements.buttons[key]['payload']+'" value="'+data.message.attachment.elements.buttons[key]['title']+'" />';
                        }

                    }

                    $('<li class="replies"><p>' + data.recipient.query + '</p></li>').appendTo($('.messages ul'));
                    $('<li class="sent"><img class="img" src="'+senderImg+'" alt="" /><p>' + data.message.attachment.elements.text + '<br />' + buttons + '</p></li>').appendTo($('.messages ul'));

                } else if (data.message.attachment.type === "quickReplies") {

                    let buttons = '';

                    for(let key in data.message.attachment.elements.buttons){

                        if (data.message.attachment.elements.buttons[key]['type'] === 'web_url') {

                            buttons += '<a class="quick_replies pay_url" target="_blank" href="'+data.message.attachment.elements.buttons[key]['url']+'">'+data.message.attachment.elements.buttons[key]['title']+'</a>';
                        } else {
                            buttons += '<input class="quick_replies" type="button" data-payload="'+data.message.attachment.elements.buttons[key]['payload']+'" value="'+data.message.attachment.elements.buttons[key]['title']+'" />';
                        }

                    }

                    //<img class="img" src="'+repliesImg+'" alt="" />
                    $('<li class="replies"><p>' + data.recipient.query + '</p></li>').appendTo($('.messages ul'));
                    $('<li class="sent"><img class="img" src="'+senderImg+'" alt="" /><p>' + data.message.attachment.elements.text + '<br />' + buttons + '</p></li>').appendTo($('.messages ul'));

                } else if (data.message.attachment.type  === 'carousel') {

                    let rand_id = Math.random().toString(36).substring(7);
                    let sliders = '';
                    let next_pre_btn = '';

                    let slider_length = data.message.attachment.elements.sliders.length;

                    for(let key in data.message.attachment.elements.sliders ){

                        let buttons = '';

                        for (let btn_key in data.message.attachment.elements.sliders[key].buttons) {

                            if (data.message.attachment.elements.sliders[key]['buttons'][btn_key]['type'] === 'web_url') {
                                buttons += '<a class="carousel-btn-url" target="_blank" href="'+data.message.attachment.elements.sliders[key]['buttons'][btn_key]['url']+'">'+data.message.attachment.elements.sliders[key]['buttons'][btn_key]['title']+'</a>';
                            } else {
                                buttons += '<input class="carousel-btn-button" type="button" data-payload="'+data.message.attachment.elements.sliders[key]['buttons'][btn_key].payload+'" value="'+data.message.attachment.elements.sliders[key]['buttons'][btn_key].title+'" />';
                            }


                        }

                        sliders += '<div class="carousel-item text-center '+data.message.attachment.elements.sliders[key]["active"]+'">'+
                            '<img src="'+data.message.attachment.elements.sliders[key]["image_url"]+'" class="d-block c_slider" alt="...">'+
                            '<div class="carousel-caption d-none d-md-block">'+
                            '<h5 class="carousel-heading">'+data.message.attachment.elements.sliders[key]["title"]+'</h5>'+
                            '<h4 class="carousel-sub-heading">'+data.message.attachment.elements.sliders[key]["subtitle"]+'</h4>'+
                            '</div>'+buttons+'</div>';
                    }

                    if (slider_length > 1) {
                        next_pre_btn = '<a class="carousel-control-prev" href="#carouselControls'+rand_id+'" role="button" data-slide="prev">'+
                            '<span class="carousel-control-prev-icon" aria-hidden="true"></span>'+
                            '<span class="sr-only">Previous</span>'+
                            '</a>'+
                            '<a class="carousel-control-next" href="#carouselControls'+rand_id+'" role="button" data-slide="next">'+
                            '<span class="carousel-control-next-icon" aria-hidden="true"></span>'+
                            '<span class="sr-only">Next</span>'+
                            '</a>';
                    }

                    let carousel =

                        '<li class="sent">'+
                        '<img class="img" src="'+senderImg+'" alt="" />'+
                        '<p>'+data.message.attachment.elements.text+'</p>'+
                        '<div id="carouselControls'+rand_id+'" class="carousel slide" data-ride="carousel" data-interval="false">'+
                        '<div class="carousel-inner">'+sliders+'</div>'
                        + next_pre_btn +
                        '</div>'+
                        '</li>';

                    $('<li class="replies"><p>' + data.recipient.query + '</p></li>').appendTo($('.messages ul'));
                    $('.messages ul').append(carousel);

                   if (data.message.attachment.elements.hasOwnProperty("buttons")) {

                       let pay_input_buttons = '';

                       if (data.message.attachment.elements.buttons !== null) {

                           for (let key in data.message.attachment.elements.buttons) {

                               if (data.message.attachment.elements.buttons[key]['type'] === 'web_url') {

                                   pay_input_buttons += '<a class="quick_replies" target="_blank" type="button" href="' + data.message.attachment.elements.buttons[key]['url'] + '">' + data.message.attachment.elements.buttons[key]['title'] + '</a>';
                               } else {
                                   pay_input_buttons += '<input class="quick_replies" type="button" data-payload="' + data.message.attachment.elements.buttons[key]['payload'] + '" value="' + data.message.attachment.elements.buttons[key]['title'] + '" />';
                               }

                           }

                           $('<li class="sent"><p>' + pay_input_buttons + '</p></li>').appendTo($('.messages ul'));
                       }

                   }

                }

            }

        }


        function sendToHook(text, payload, sender_id) {

            if($.trim(text) === '') {
                return false;
            }

            let data = {
                message : {
                    text :text,
                    payload : payload
                },
                sender : {
                    id: sender_id
                },
                meta : {
                    source: 'web_bot'
                }
            };

            $.ajax({
                url: "hook.php",
                data: JSON.stringify({
                    entry: data,
                }),
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',

            })

            .done(function (data) {
               // console.log(data);
            })
            .always(function( xhr, status ) {
                $('.message-input input').val(null);
                //reload();
            });

        }

        /*User Input Message Start*/

        if($.trim($('.messages ul').text()) === '') {

            $('.get_started_block').css('display', 'block');
            $('<li class="sent get_started_block"><img class="img" src= "'+senderImg+'" alt="" /><input class="get_started" type="button" data-payload="get_started" value="Get Started" /></li>').appendTo($('.messages ul'));

        } else{
            $('.get_started_block').css('display', 'none');
        }

        // on pressing submit button get user input message
        $('.submit').click(function(){

            $('.get_started_block').css('display', 'none');
            text = $('.message-input input').val();
            sendToHook(text, null, sender_id);

        });

        // on keydown get user input message
        $(window).on('keydown', function(e) {
            if (e.which === 13) {
                $('.get_started_block').css('display', 'none');
                text = $('.message-input input').val();
                sendToHook(text, null, sender_id);

            }
        });

        // on button click get user input message
        $(document).on('click','input:button', function() {

            $('.get_started_block').css('display', 'none');
            text = $(this).val();
            payload = $(this).data("payload");
            sendToHook(text, payload, sender_id);
        
        });

        /*User Input Message End*/
        

    });

</script>
</body></html>


