<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/inbox-controller.php';
require_once 'app/customer-controller.php';

//unauthorizedUserRedirect('login.php');

ini_set('memory_limit', -1);


if (isset($_GET['from_date']) && !empty($_GET['from_date']) && isset($_GET['to_date']) && !empty($_GET['to_date'])) {

    $start_date = date('Y-m-d', strtotime($_GET['from_date']));
    $end_date = date('Y-m-d', strtotime($_GET['to_date']));
    $inbox = inboxFiltered($start_date, $end_date);
} else {

    $start_date = date('Y-m-d');
    $end_date = date('Y-m-d');
    $inbox = inboxFiltered($start_date, $end_date);
}

?>


<?php setPageTitle('Dashboard - Inbox'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>


<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <div class="float-left">
            <i class="fas fa-table"></i>
            Inbox 
        </div>
       <!-- <div class="float-right">
            <strong>From </strong> 12-10-2018 <strong>To </strong> 12-10-2018
        </div>-->

    </div>

    <div class="card-body">

        <form action="inbox.php" method="get">
            <div class="row">
                <div class="col mb-3">
                    <label for="from_date">FROM DATE</label>
                    <input type="date" class="form-control" name="from_date" id="from_date" value="<?php (isset($_GET['from_date'])) ? print $_GET['from_date'] : '' ?>">
                </div>

                <div class="col mb-3">
                    <label for="to_date">TO DATE</label>
                    <input type="date" class="form-control" name="to_date" id="to_date" value="<?php (isset($_GET['to_date'])) ? print $_GET['to_date'] : '' ?>">
                </div>

                <div class="col mb-3">
                    <button class="btn btn-primary form-control" type="submit" style="margin-top: 30px" name="submit" id="submit">SUBMIT</button>
                </div>
            </div>
        </form>

        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>SENDER</th>
                    <th>MESSAGE</th>
                    <th>RESPONSE</th>
                    <th style="width: 200px">DATE</th>
                </tr>
                </thead>
                <tbody>
                <?php $sl = 0 ?>
                <?php foreach ($inbox as $row): ?>
                <?php $sl ++ ; ?>
                <tr>
                    <td><?= $sl ?></td>
                    <td><?= getUserName($row['user']) ?></td>
                    <td><?= $row['query']?> </td>
                    <td><?= $row['response'] ?></td>
                    <td><?= date('M-d-Y h:i a', strtotime($row['date'])) ?></td>
                 </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </div>
        
        
        </div>
    </div>
    <div class="card-footer small text-muted"></div>
</div>

<!--<p class="small text-center text-muted my-5">
    <em>More table examples coming soon...</em>
</p>
-->


<?php require_once 'footer.php' ?>
