<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/auth-controller.php';

unauthorizedUserRedirect('login.php');

if (isset($_POST['add_user'])) {
    $values = array(
        'name' => $_POST['name'],
        'email' => $_POST['email'],
        'password' => $_POST['password']
    );

    $status = userReg($values);
}

?>


<?php setPageTitle('Add User'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>


<div class="row justify-content-center">
    <div class="col-lg-8">
        <div class="card mb-3">
            <div class="card-header">
                <div class="float-left">
                    <i class="fas fa-user"></i>
                    Add User
                </div>
               <!-- <div class="float-right">
                    <strong>From </strong> 12-10-2018 <strong>To </strong> 12-10-2018
                </div>-->

            </div>

            <div class="card-body">

                <?php

                if(isset($_POST['add_user']))
                {
                    switch ($status)
                    {
                        case 'email_invalid':
                            echo alert('Invalid email address!', 'warning');
                            break;
                        case 'fields_empty':
                            echo alert('Fields can not be empty!', 'warning');
                            break;
                        case 'reg_success':
                            echo alert('User created successfully!', 'success');
                            break;
                        case 'exists_true':
                            echo alert('User exists already!', 'warning');
                            break;

                    }
                }

                ?>

                <form method="post" enctype="multipart/form-data" action="add-user.php">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" name="email" class="form-control" id="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    </div>
                    <button type="submit" name="add_user" class="btn btn-primary float-right">Add User</button>
                </form>
            </div>
            <!--<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
        </div>
    </div>
</div>
<!--<p class="small text-center text-muted my-5">
    <em>More table examples coming soon...</em>
</p>
-->


<?php require_once 'footer.php' ?>
