<?php include 'header.php';?>
<!--    [ Strat Section Area]-->
<section id="query" class="body-part">
    <div class="container"> <h2 style="text-align:center;">Daily-Campaign</h2>
        <div class="row justify-content-center">
           
            <div class="col-lg-6">
                <a href="daily_push.php" class="single-query">
                    <i class="fa fa-handshake-o" aria-hidden="true"></i>
                    <span>POST CAMPAIGN</span>
                </a>
            </div>
            <div class="col-lg-6">
                <a href="daily_campaign_dash.php" class="single-query">
                    <i class="fa fa-linode" aria-hidden="true"></i>
                        <span>CAMPAIGN DASHBOARD</span>

                </a>
            </div>
            <div class="col-lg-6">
                <a href="daily_campaign_analytics.php" class="single-query">
                    <i class="fa fa-pie-chart" aria-hidden="true"></i>
                        <span>CAMPAIGN ANALYTICS</span>

                </a>
            </div>

        </div>
    </div>
</section>
<!--    [Finish Section Area]-->
<?php include 'footer.php';?>
