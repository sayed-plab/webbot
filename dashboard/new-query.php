<?php include 'header.php';?>
<!--    [ Strat Section Area]-->
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: 'textarea'
    });

</script>
<section id="newq" class="body-part">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="page-heading text-center">
                    <h3>NEW QUERY ADD DASHBOARD</h3>
                </div>
            </div>
        </div>
        <form   action="new-query-data.php" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-6">
                    <div class="ques-ans-section">

                        <input type="text" name="english_question" placeholder="English Question">
                        <textarea name="english_answer" id="" cols="10" rows="5"></textarea>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ques-ans-section">

                        <input type="text" name="bangla_question" placeholder="Bangla  Question">
                        <textarea name="bangla_answer" id="" cols="10" rows="5"></textarea>

                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="ques-ans-section">

                        <input type="text" name="banglish_question" placeholder="Banglish  Question">
                        <textarea name="banglish_answer" id="" cols="10" rows="5"></textarea>

                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="title-img-link">
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="text" name="english_url" placeholder="English URL">
                                <!--<div class="carousel-option">-->
                                <!--    <span>English carousel</span><input type="radio" name="e-caro" value="Yes">Yes <input type="radio" name="e-caro" value="no">No-->
                                <!--</div>-->
                            </div>
                            <div class="col-lg-6">
                                <input type="text" name="bangla_url" placeholder="Bangla URL">
                                <!--<div class="carousel-option">-->
                                <!--    <span>Bangla carousel</span><input type="radio" name="e-caro" value="Yes">Yes <input type="radio" name="e-caro" value="no">No-->
                                <!--</div>-->
                            </div>
                        </div>


                        <!--<input type="text" placeholder="Title">-->
                        <!--<input type="text" placeholder="images">-->
                        <!--<input type="link" placeholder="link">-->
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="sec-title">
                        <h4>English Payloads</h4>
                    </div>
                    <div class="new-query-form">


                        <input type="text" name="payeng1" placeholder="Pay Load 1">
                        <input type="text" name="payengno1" placeholder="Pay Load No 1">

                        <input type="text" name="payeng2" placeholder="Pay Load 2">
                        <input type="text" name="payengno2" placeholder="Pay Load No 2">

                        <input type="text" name="payeng3" placeholder="Pay Load 3">
                        <input type="text" name="payengno3" placeholder="Pay Load No 3">

                        <input type="text" name="payeng4" placeholder="Pay Load 4">
                        <input type="text" name="payengno4" placeholder="Pay Load No 4">

                        <input type="text" name="payeng5" placeholder="Pay Load 5">
                        <input type="text" name="payengno5" placeholder="Pay Load No 5">

                        <input type="text" name="payeng6" placeholder="Pay Load 6">
                        <input type="text" name="payengno6" placeholder="Pay Load No 6">

                        <input type="text" name="payeng7" placeholder="Pay Load 7">
                        <input type="text" name="payengno7" placeholder="Pay Load No 7">

                        <input type="text" name="payeng8" placeholder="Pay Load 8">
                        <input type="text" name="payengno8" placeholder="Pay Load No 8">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="sec-title">
                        <h4>Bangla Payloads</h4>
                    </div>
                    <div class="new-query-form">
                        <input type="text" name="paybn1" placeholder="Pay Load 1">
                        <input type="text" name="paybnno1" placeholder="Pay Load No 1">

                        <input type="text" name="paybn2" placeholder="Pay Load 2">
                        <input type="text" name="paybnno2" placeholder="Pay Load No 2">

                        <input type="text" name="paybn3" placeholder="Pay Load 3">
                        <input type="text" name="paybnno3" placeholder="Pay Load No 3">

                        <input type="text" name="paybn4" placeholder="Pay Load 4">
                        <input type="text" name="paybnno4" placeholder="Pay Load No 4">

                        <input type="text" name="paybn5" placeholder="Pay Load 5">
                        <input type="text" name="paybnno5" placeholder="Pay Load No 5">

                        <input type="text" name="paybn6" placeholder="Pay Load 6">
                        <input type="text" name="paybnno6" placeholder="Pay Load No 6">

                        <input type="text" name="paybn7" placeholder="Pay Load 7">
                        <input type="text" name="paybnno7" placeholder="Pay Load No 7">

                        <input type="text" name="paybn8" placeholder="Pay Load 8">
                        <input type="text" name="paybnno8" placeholder="Pay Load No 8">
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <div class="submit-btn text-center">
                        <button>SUBMIT</button>
                    </div>

                </div>
            </div>
        </form>
    </div>
</section>
<script>
    tinymce.init({
        selector: 'textarea',
        height: 100,
        theme: 'modern',
        plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
        image_advtab: true,
        templates: [{
                title: 'Test template 1',
                content: 'Test 1'
            },
            {
                title: 'Test template 2',
                content: 'Test 2'
            }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//assets/css/codepen.min.css'
        ]
    });

</script>
<!--    [Finish Section Area]-->
<?php include 'footer.php';?>
