
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">

        <li class="nav-item" >
            <a class="nav-link waves-effect" href="index.php">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>OVERVIEW</span>
            </a>
        </li>
        
        
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle waves-effect" href="#" id="pagesDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-table pr-2"></i>
                <span>MESSAGES</span>
            </a>
             <ul class="dropdown-menu" aria-labelledby="pagesDropdown1">
                <li class="dropdown-item"><a href="inbox.php"><i class="fas fa-table pr-2"></i>MESSENGER INBOX</a></li>
                <li class="dropdown-item"><a href="bot-can-not-reply.php"><i class="fas fa-table pr-2"></i>CAN NOT REPLY</a></li>
            </ul>
           
        </li>
        
        
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle waves-effect" href="#" id="pagesDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-table pr-2"></i>
                <span>REVENUE</span>
            </a>
             <ul class="dropdown-menu" aria-labelledby="pagesDropdown1">
                <li class="dropdown-item"><a href="data-purchase.php"><i class="fas fa-table pr-2"></i>DATA PURCHASE</a></li>
                <li class="dropdown-item"><a href="recharge.php"><i class="fas fa-table pr-2"></i>RECHARGE</a></li>
            </ul>
           
        </li>

        
      <!--  <li class="nav-item">
            <a class="nav-link waves-effect" href="bot-can-not-reply.php">
                <i class="fa fa-inbox"></i>
                <span>Bot Can Not Reply</span>
            </a>
        </li>
-->

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle waves-effect" href="#" id="pagesDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-table pr-2"></i>
                <span>RESPONSE QUERY</span>
            </a>
            <ul class="dropdown-menu" aria-labelledby="pagesDropdown1">
                <li class="dropdown-item"><a href="add-query.php"><i class="fas fa-table pr-2"></i>ADD</a></li>
                <li class="dropdown-item"><a href="query-dash.php"><i class="fas fa-table pr-2"></i>VIEW</a></li>
            </ul>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle waves-effect" href="#" id="pagesDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-table pr-2"></i>
                <span>BUTTONS</span>
            </a>
            <ul class="dropdown-menu" aria-labelledby="pagesDropdown1">
                <li class="dropdown-item"><a href="add-button.php"><i class="fas fa-table pr-2"></i>ADD</a></li>
                <li class="dropdown-item"><a href="button-dash.php"><i class="fas fa-table pr-2"></i>VIEW</a></li>
            </ul>
        </li>


        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle waves-effect" href="#" id="pagesDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-table pr-2"></i>
                <span>QUICK REPLY</span>
            </a>
            <ul class="dropdown-menu" aria-labelledby="pagesDropdown1">
                <li class="dropdown-item"><a href="add-quick-reply.php"><i class="fas fa-table pr-2"></i>ADD</a></li>
                <li class="dropdown-item"><a href="quick-reply-dash.php"><i class="fas fa-table pr-2"></i>VIEW</a></li>
            </ul>
        </li>


    </ul>

    <div id="content-wrapper">

        <div class="container-fluid">
            <!-- Page Content -->
            <!-- Box Content -->


