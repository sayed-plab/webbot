<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/nps-controller.php';

unauthorizedUserRedirect('login.php');

ini_set('memory_limit', -1);

$date = (isset($_GET['date']) && !empty($_GET['date'])) ? date('d-m-Y', strtotime($_GET['date'])) : date('d-m-Y');

$ice = npsEveryDate($date);

$date = date('d-m-Y', strtotime($_GET['date']));
$file_name = "ICE Report - $date";

$yes = iceAnalytics($date, 'yes');
$no = iceAnalytics($date, 'no');
$empty = iceAnalytics($date, 'empty');
$comment = iceAnalytics($date, 'comment');
$total = $yes + $no + $empty + $comment;

?>


<?php setPageTitle('Dashboard - ICE Message'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>

<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <div class="float-left">
            <i class="fas fa-table"></i>
            ICE Report <strong>Total: <?= $total ?>
                Date: <?= date('d-m-Y', strtotime($_GET['date'])) ?>
                Day: <?= date('l', strtotime($_GET['date'])) ?>
            </strong>
        </div>
       <!-- <div class="float-right">
            <strong>From </strong> 12-10-2018 <strong>To </strong> 12-10-2018
        </div>-->

    </div>

    <div class="card-body">

        <div class="card-body">
           <?= $ice[0]['title']?>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>DATE</th>
                    <th>TOTAL SENT</th>
                    <th>YES</th>
                    <th>NO</th>
                    <th>EMPTY</th>
                    <th>COMMENT</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?= $date ?></td>
                    <td><?= $total ?></td>
                    <td><?= $yes ?></td>
                    <td><?= $no ?></td>
                    <td><?= $empty ?></td>
                    <td><?= $comment ?></td>
                </tr>
                </tbody>
            </table>

        </div>

        <div class="table-responsive" id="iceNPS" >
            <table class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>SENT DATE</th>
                    <th>SENT TIME</th>
                    <th>ANSWER</th>
                    <th>REPLY DATE</th>
                    <th>REPLY TIME</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($ice as $row): ?>
                <tr>
                    <td>
                        <?= $row['id']?>
                    </td>

                    <td>
                        <?= $row['date'] ?>
                    </td>

                    <td>
                        <?= $row['time'] ?>
                    </td>

                    <td>
                        <?= $row['answer'] ?>
                    </td>

                    <td>
                        <?= $row['s_date'] ?>
                    </td>
                    
                    <td>
                        <?= $row['s_time'] ?>
                    </td>
                 </tr>

                <?php endforeach; ?>
                </tbody>
            </table>

        </div>

    </div>
    <div class="card-footer small text-muted"></div>
</div>

<!--<p class="small text-center text-muted my-5">
    <em>More table examples coming soon...</em>
</p>
-->

<?php require_once 'footer.php' ?>

<script>

    $("#iceNPS").tableExport({
        formats: ["xlsx"],
        bootstrap: true,
        exportButtons: true,
        filename: "<?= $file_name ?>",
    });

</script>
