<?php

require_once 'app/general/functions.php';
require_once 'app/query-controller.php';

//unauthorizedUserRedirect('login.php');

if(isset($_POST['add_button'])) {
    
    $query_info = array(
        'mapping' => $_POST['mapping'],
        'button_type' => $_POST['button_type'],
        'url_title' => $_POST['url_title'],
        'title_payload' => $_POST['title_payload'],
        );
        
    $status = addButton($query_info);
}


?>

<?php setPageTitle('Add Button'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>

<style>
    
    input textarea{
        border-radius: 0;
    }
    
</style>

<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <div class="float-left">
            <i class="fas fa-table"></i>
            Add Button
        </div>
       <!-- <div class="float-right">
            <strong>From </strong> 12-10-2018 <strong>To </strong> 12-10-2018
        </div>-->

    </div>

    <div class="card-body">

        <?php if(isset($status) & $status == true): ?>
        <div class="alert alert-success text-center">
            Button Added Successfully
        </div>
        <?php endif; ?>


       <form action="add-button.php" method="post">

          <div class="row mb-2">
            <div class="col">
                <label for="intent">MAPPING</label>
              <input type="text" name="mapping" id="mapping" class="form-control" placeholder="MAPPING">
            </div>
            <div class="col">
              <label for="button_type">BUTTON TYPE</label>
                <select name="button_type" id="button_type" class="form-control">
                    <option value="web_url">Select Option</option>
                    <option value="web_url">Web URL</option>
                    <option value="postpack">Postback</option>
                </select>
            </div>
          </div>

          <div class="row mb-2">
              <div class="col">
                  <label for="url_title">URL/TITLE</label>
                  <input type="text" name="url_title" id="url_title" class="form-control" placeholder="URL or TITLE">
              </div>
              <div class="col">
                  <label for="intent">TITLE/PAYLOAD</label>
                  <input type="text" name="title_payload" id="title_payload" class="form-control" placeholder="TITLE or PAYLOAD">
              </div>
          </div>

          <div class="row mb-2">
              <div class="col">
                <button type="submit" name="add_button" class="btn btn-primary float-right">Add Button</button>
              </div>
          </div>

        </form>

    </div>
    
    <!--<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
</div>

<!--<p class="small text-center text-muted my-5">
    <em>More table examples coming soon...</em>
</p>
-->





<?php require_once 'footer.php' ?>