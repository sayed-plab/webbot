<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/inbox-controller.php';
require_once 'app/customer-controller.php';

//unauthorizedUserRedirect('login.php');

ini_set('memory_limit', -1);

if (isset($_GET['user'])) {
    $user = $_GET['user'];
    $inbox = showSingleInbox($user);
} else {
    header('Location: inbox.php');
}

?>


<?php setPageTitle('Dashboard - Single User'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>


<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <div class="float-left">
            <i class="fas fa-table"></i>
            Inbox Single
        </div>
       <!-- <div class="float-right">
            <strong>From </strong> 12-10-2018 <strong>To </strong> 12-10-2018
        </div>-->

    </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>SENDER</th>
                        <th>MESSAGE</th>
                        <th>RESPONSE</th>
                        <th>DATE</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($inbox as $row): ?>
                        <tr>

                            <td><?= getUserName($row['user']) ?></td>
                            <td><?= $row['query']?> </td>
                            <td><?= $row['response'] ?></td>
                            <td><?= date('d-m-Y', strtotime($row['date'])) ?></td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

            </div>


        </div>
    </div>

    <div class="card-footer small text-muted"></div>
</div>

<!--<p class="small text-center text-muted my-5">
    <em>More table examples coming soon...</em>
</p>
-->


<?php require_once 'footer.php' ?>
