<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/retrain-controller.php';

unauthorizedUserRedirect('login.php');

ini_set('memory_limit', -1);

$date = date('d-m-Y', strtotime($_GET['date']));
$untrainedReplies = untrainedReplies($date);
$bangla_intents = parameters('bangla');
$english_intents = parameters('english');
$banglish_intents = parameters('banglish');

if(isset($_POST['retrain'])) {
    
    if (!empty($_POST['bangla_param'])) {
        $param = $_POST['bangla_param'];
    } elseif (!empty($_POST['english_param'])) {
        $param = $_POST['english_param'];
    } elseif (!empty($_POST['banglish_param'])) {
        $param = $_POST['banglish_param'];
    } 
    
    $retrain_data = array(
         'id' => $_POST['id'],
         'message' => $_POST['message'],
         'tag' => implode(" ",$_POST['tag']),
         'param' => $param,
         'lang' => $_POST['language']
         );
         
    addRetrain($retrain_data);
    
    header('Location: retrain-every-date.php?date='.$_POST['date']);
    
} elseif (isset($_GET['action']) && $_GET['action'] == 'discard' && !empty($_GET['id'])) {
    
    $id = $_GET['id'];
    discardRetrain($id);
}

?>


<?php setPageTitle('Dashboard - Bot Can Not Reply'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>


<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <div class="float-left">
            <i class="fas fa-table"></i>
            Retrain - Bot Can Not Reply
        </div>
       <!-- <div class="float-right">
            <strong>From </strong> 12-10-2018 <strong>To </strong> 12-10-2018
        </div>-->

    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>MAIN QUERY</th>
                    <th>TAG1</th>
                    <th>TAG2</th>
                    <th>TAG3</th>
                    <th>TAG4</th>
                    <th>TAG5</th>
                    <th>LANGUAGE</th>
                    <th>BANGLA INTENT</th>
                    <th>ENGLISH INTENT</th>
                    <th>BANGLISH INTENT</th>
                    <th>ACTION</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($untrainedReplies as $row): ?>
                
                <form method="post" action="retrain-every-date.php">
                    
                    <input type="hidden" name="date" value="<?= date('d-m-Y', strtotime($_GET['date'])) ?>">

                    <tr>
                        <td><input type="hidden" name="id" value="<?= $row['id'] ?>"><?= $row['id'] ?></td>
                        <td><input type="hidden" name="message" value="<?= $row['message_content'] ?>"><?= $row['message_content'] ?></td>
                        <td>
                            <select name="tag[]">
                                <option value="">Select Tag1</option>
                                <?php foreach(explode(" ",$row['message_content']) as $data):?>
                                <option value="<?= $data ?>"><?= substr($data, 0, 20) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <select name="tag[]">
                                <option value="">Select Tag2</option>
                                <?php foreach(explode(" ",$row['message_content']) as $data):?>
                                <option value="<?= $data ?>"><?= substr($data, 0, 20) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <select name="tag[]">
                                <option value="">Select Tag3</option>
                                <?php foreach(explode(" ",$row['message_content']) as $data):?>
                                <option value="<?= $data ?>"><?= substr($data, 0, 20) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <select name="tag[]">
                                <option value="">Select Tag4</option>
                                <?php foreach(explode(" ",$row['message_content']) as $data):?>
                                <option value="<?= $data ?>"><?= substr($data, 0, 20) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <select name="tag[]">
                                <option value="">Select Tag5</option>
                                <?php foreach(explode(" ",$row['message_content']) as $data):?>
                                <option value="<?= $data ?>"><?= substr($data, 0, 20) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <select name="language">
                                <option value="">Language</option>
                                <option value="Bangla">Bangla</option>
                                <option value="English">English</option>
                                <option value="Banglish">Banglish</option>
                            </select>
                        </td>
                        <td>
                            <select name="bangla_param">
                                <option value="">Bangla Param</option>
                                <?php foreach($bangla_intents as $row): ?>
                                <option value="<?= $row['bangla_param'] ?>"><?= $row['bangla_param'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <select name="english_param">
                                <option value="">English Param</option>
                                <?php foreach($english_intents as $row): ?>
                                <option value="<?= $row['english_param'] ?>"><?= $row['english_param'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <select name="banglish_param">
                                <option value="">Banglish Param</option>
                                <?php foreach($banglish_intents as $row): ?>
                                <option value="<?= $row['banglish_param'] ?>"><?= $row['banglish_param'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        
                        <td>
                            <button type="submit" name="retrain">Retrain</button>
                            <a href="retrain-every-date.php">Discard</a>
                        </td>
                    </tr>
                </form>
                <?php endforeach; ?>
                </tbody>
            </table>


        </div>
        
        
        </div>
    </div>
   <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
</div>

<!--<p class="small text-center text-muted my-5">
    <em>More table examples coming soon...</em>
</p>
-->


<?php require_once 'footer.php' ?>
