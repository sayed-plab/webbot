<?php

require_once 'app/general/functions.php';
require_once 'app/query-controller.php';

//unauthorizedUserRedirect('login.php');

if(isset($_POST['add_query'])) {
    
    $query_info = array(
        'intent' => $_POST['intent'],
        'greet_en' => $_POST['greet_en'],
        'greet_bn' => $_POST['greet_bn'],
        'param' => $_POST['param'],
        'response_text_en' => $_POST['response_text_en'],
        'response_text_bn' => $_POST['response_text_bn'],
        'carousels' => $_POST['carousels'],
        'buttons' => $_POST['buttons'],
        'quick_replies' => $_POST['quick_replies'],
        'keywords' => $_POST['keywords'],
        );
        
    $status = addQuery($query_info);
}


?>

<?php setPageTitle('Add Query'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>

<style>
    
    input {
        border-radius: 0px;
    }
    textarea {
        border-radius: 0px;
    }
    
</style>

<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <div class="float-left">
            <i class="fas fa-table"></i>
            Add Response Query
        </div>
       <!-- <div class="float-right">
            <strong>From </strong> 12-10-2018 <strong>To </strong> 12-10-2018
        </div>-->

    </div>

    <div class="card-body">
        
        <?php if(isset($status) & $status == true): ?>
        <div class="alert alert-success text-center">
            Query Added Successfully
        </div>
        <?php endif; ?>

        
       <form action="add-query.php" method="post">
           
          <div class="row mb-2">
            <div class="col">
                <label for="intent">INTENT</label>
              <input type="text" name="intent" id="intent" class="form-control" placeholder="INTENT">
            </div>
              <div class="col">
                  <label for="greet_en">GREET EN</label>
                  <input type="text" name="greet_en" class="form-control" placeholder="GREET BANGLA">
              </div>
            <div class="col">
                <label for="greet_bn">GREET BN</label>
              <input type="text" name="greet_bn" id="greet_bn" class="form-control" placeholder="GREET ENGLISH">
            </div>
              <div class="col">
                  <label for="param">PARAM</label>
                  <input type="text" name="param" id="param" class="form-control" placeholder="PARAM">
              </div>
          </div>
           
          <div class="row mb-2">
            <div class="col">
                <label for="response_text_en">RESPONSE TEXT EN</label>
              <textarea name="response_text_en" id="response_text_en" class="form-control" placeholder="RESPONSE TEXT ENGLISH"></textarea>
            </div>
            <div class="col">
                <label for="response_text_bn">RESPONSE TEXT BN</label>
              <textarea name="response_text_bn" id="response_text_bn" class="form-control" placeholder="RESPONSE TEXT BANGLA"></textarea>
            </div>
          </div>

           <div class="row mb-2">
               <div class="col">
                   <label for="carousels">CAROUSELS</label>
                   <input type="text" name="carousels" id="carousels" class="form-control" placeholder="CAROUSELS">
               </div>
               <div class="col">
                   <label for="buttons">BUTTONS</label>
                   <input type="text" name="buttons" id="buttons" class="form-control" placeholder="BUTTONS">
               </div>
               <div class="col">
                   <label for="quick_replies">QUICK REPLIES</label>
                   <input type="text" name="quick_replies" id="quick_replies" class="form-control" placeholder="QUICK REPLIES">
               </div>
           </div>

           <div class="row mb-2">
               <div class="col">
                   <label for="keywords">KEYWORDS</label>
                   <textarea name="keywords" id="keywords" class="form-control" placeholder="KEYWORDS"></textarea>
               </div>
           </div>

          <div class="row mb-2">
              <div class="col">
                <button type="submit" name="add_query" class="btn btn-primary float-right">Add Query</button>
              </div>
          </div>
          
        </form>
        
    </div>
    
    <!--<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
</div>

<!--<p class="small text-center text-muted my-5">
    <em>More table examples coming soon...</em>
</p>
-->





<?php require_once 'footer.php' ?>