<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/auth-controller.php';

unauthorizedUserRedirect('login.php');

if (isset($_POST['translate'])) {
    
    $text = strtolower($_POST['text']);
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://axiata.messenger.care/airtel/avro-script/avro.php');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('text' => $text));
    $data = json_decode(curl_exec($ch), true);
    $translate = $data['text'];
    curl_close($ch);
    
}

?>


<?php setPageTitle('Avro Translate'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>


<div class="row justify-content-center">
    <div class="col-lg-8">
        <div class="card mb-3">
            <div class="card-header">
                <div class="float-left">
                    <i class="fas fa-user"></i>
                    Avro Translation
                </div>
               <!-- <div class="float-right">
                    <strong>From </strong> 12-10-2018 <strong>To </strong> 12-10-2018
                </div>-->

            </div>

            <div class="card-body">
                <form method="post" enctype="multipart/form-data" action="avro-translate.php">
                    <div class="form-group">
                        <label for="text">Text</label>
                        <input type="text" name="text" class="form-control" id="text" placeholder="Enter text" autofocus>
                    </div>
                    <button type="submit" name="translate" class="btn btn-primary float-right">Translate</button>
                    <a href="avro-translate.php" class="btn btn-primary float-right  mr-3">Again!</a>
                </form>
            </div>
            <!--<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
        </div>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-lg-8">
                    
                            
            <?php if(!is_null($translate)): ?>
            <div class="card">
                <div class="card-body">
                    <?= $translate ?? null ?>
                </div>
                
            </div>
            <?php endif; ?>
    </div>
</div>
<!--<p class="small text-center text-muted my-5">
    <em>More table examples coming soon...</em>
</p>
-->


<?php require_once 'footer.php' ?>
