<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/acm-controller.php';

$home = true;

if (isset($_POST['submit'])) {

    $data = array_filter(array(
        'msisdn' => $_POST['msisdn'],
        'status' => $_POST['status'],
        'from_date' => $_POST['from_date'],
        'to_date' => $_POST['to_date']
    ));

    header('Location: recharge.php?'.http_build_query($data));
}

if (isset($_GET['msisdn']) || isset($_GET['status']) || isset($_GET['from_date']) || isset($_GET['to_date'])) {
    $home = false;
}

$rechargeHistory = rechargeHistory();
$total = totalRechargeHistory($rechargeHistory, $home);
$topRecharge = topRecharge();

?>


<?php setPageTitle('Dashboard'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>


<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>

        Recharge History <strong>( Total <?= $total ?> TK )</strong>
        <a href="recharge.php" class="btn btn-sm btn-secondary float-right ml-3" type="button">RESET</a>
        <a href="#" class="btn btn-sm btn-secondary float-right" data-toggle="modal" data-target="#exampleModal" type="button">Top 5</a>

    </div>


    <div class="card-body">

        <form action="recharge.php" method="post">
            <div class="row">

                <div class="col mb-3">
                    <label for="to_date">MSISDN</label>
                    <input type="text" class="form-control" name="msisdn" id="msisdn" placeholder="msisdn" value="<?php (isset($_GET['msisdn'])) ? print $_GET['msisdn'] : '' ?>">
                </div>

                <div class="col mb-3">
                    <label for="reply">STATUS</label>
                    <select class="form-control" name="status" id="status">

                        <?php if (isset($_GET['status']) && !empty($_GET['status'])): ?>
                            <option value="<?= $_GET['status'] ?>"><?= $_GET['status'] ?></option>
                        <?php else: ?>
                            <option value="">Select Filter</option>
                        <?php endif; ?>
                        <option value="success">success</option>
                        <option value="failed">failed</option>
                    </select>
                </div>

                <div class="col mb-3">
                    <label for="from_date">FROM DATE</label>
                    <input type="date" class="form-control" name="from_date" id="from_date" value="<?php (isset($_GET['from_date'])) ? print $_GET['from_date'] : '' ?>">
                </div>

                <div class="col mb-3">
                    <label for="to_date">TO DATE</label>
                    <input type="date" class="form-control" name="to_date" id="to_date" value="<?php (isset($_GET['to_date'])) ? print $_GET['to_date'] : '' ?>">
                </div>

                <div class="col mb-3">
                    <button style="margin-top: 30px" class="btn btn-primary form-control" type="submit" name="submit" id="submit">SUBMIT</button>
                </div>
            </div>
        </form>


        <div class="table-responsive" id="recharge">
            <table class="table table-bordered"  width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th style="width: 30px">SL</th>
                    <th>MSISDN</th>
                    <th>AMOUNT</th>
                    <th>DATE</th>
                    <th>STATUS</th>
                </tr>
                </thead>
                <tbody>
                <?php $sl = 0; ?>
                <?php foreach ($rechargeHistory as $row): ?>
                <?php $sl++ ?>
                <tr>
                    <td><?= $sl ?></td>
                    <td><?= safeOutput($row['mobile']) ?></td>
                    <td><?= safeOutput($row['amount']) ?></td>
                    <td><?= date('d M, Y',strtotime($row['date'])) ?></td>
                    <td><?= safeOutput($row['status']) ?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer small text-muted"></div>
</div>

<p class="small text-center text-muted my-5">
    <em></em>
</p>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Top Recharge</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="card-body">


                    <div class="table-responsive" >
                        <table class="table table-bordered"  width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>MSISDN</th>
                                <th>AMOUNT</th>
                                <th>COUNT</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $sl = 0; ?>
                            <?php foreach ($topRecharge as $row): ?>
                                <?php $sl++ ?>
                                <tr>
                                    <td><?= $sl ?></td>
                                    <td><?= safeOutput($row['msisdn']) ?></td>
                                    <td><?= safeOutput($row['total']) ?></td>
                                    <td><?= safeOutput($row['count']) ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<?php require_once 'footer.php' ?>




