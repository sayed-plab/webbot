<?php

require_once 'app/auth-controller.php';
require_once 'app/general/functions.php';

if(isset($_POST['register']))
{
    $values = array(
        'name' => $_POST['name'],
        'email' => $_POST['email'],
        'password' => $_POST['password']
    );
    userReg($values);
}

?>

<?php setPageTitle('Register');  require_once 'header.php';?>

<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Register
                </div>
                <div class="card-body">


                    <?php

                    if(isset($_GET['status']))
                    {
                        switch ($_GET['status'])
                        {
                            case 'email_invalid':
                                echo alert('Invalid email address!', 'warning');
                                break;
                            case 'fields_empty':
                                echo alert('Fields can not be empty!', 'warning');
                                break;
                            case 'reg_success':
                                echo alert('Thank you for registration!', 'success');
                                break;

                        }
                    }

                    ?>

                    <form action="register.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="What's your full name?" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" name="email" placeholder="What's your email address?" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                        <button type="submit" name="register" class="btn btn-primary">Register</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>

<?php require_once 'footer.php' ?>