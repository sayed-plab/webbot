<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/security/functions.php';
require_once 'app/dash-controller.php';

unauthorizedUserRedirect('login.php');

date_default_timezone_set('Asia/Dhaka');

if(!empty($_GET['start_date']) && !empty($_GET['end_date'])) {

    $start_date = date('Y-m-d', strtotime($_GET['start_date']));
    $end_date = date('Y-m-d', strtotime($_GET['end_date']));

} else {
    $start_date = date('Y-m-d', strtotime('today - 30 days'));;
    $end_date = date('Y-m-d');
}

$dataPurchaseRevenue = dataPurchaseRevenueDateGroup($start_date, $end_date);
$todaysDataPurchaseRevenue = todaysDataPurchaseRevenue();

$todaysFbUsers = todaysFbUsers();
$fbUsersDateGroup = fbUsersDateGroup($start_date, $end_date);

$todaysUsersInteractionWithBot = todaysUsersInteractionWithBot();
$usersInteractionWithBotDateGroup = usersInteractionWithBotDateGroup($start_date, $end_date);

$dataPurchaseSuccessDateGroup = dataPurchaseSuccessDateGroup($start_date, $end_date);
$topDataPurchasePackagesCount = topDataPurchasePackagesCount($start_date, $end_date);
$todaysDataPurchaseSuccess = todaysDataPurchaseSuccess();

?>

<?php setPageTitle('Dashboard'); ?>
<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>

<!-- Revenue with Date -->

<div class="row">
    <div class="col-lg-3">
        <div class="card">
            <div class="card-header text-center">
                Today's Traffic
            </div>
            <div class="card-body text-center">
                <h4><?= (int)$todaysFbUsers ?></h4>
            </div>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="card">
            <div class="card-header text-center">
                Today's Revenue
            </div>
            <div class="card-body text-center">
                <h4><?= (double)$todaysDataPurchaseRevenue ?> TK.</h4>
            </div>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="card">
            <div class="card-header text-center">
                Today's User Inter (BOT)
            </div>
            <div class="card-body text-center">
                <h4><?= (int)$todaysUsersInteractionWithBot ?></h4>
            </div>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="card">
            <div class="card-header text-center">
                Today's Data Purchased
            </div>
            <div class="card-body text-center">
                <h4><?= (int)$todaysDataPurchaseSuccess ?></h4>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="card ml-3">
        <div class="card-body">
            <div class="col-lg-12">
                <form action="index.php" method="get" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col">
                            <input type="date" name="start_date" class="form-control" value="<?= $start_date ?>">
                        </div>
                        <div class="col">
                            <input type="date" name="end_date" class="form-control" value="<?= $end_date ?>">
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-success">Apply</button>
                        </div>
                        <div class="col">
                            <button type="button" onclick="window.location='index.php'" class="btn btn-success">View All</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                # Traffic
            </div>
            <div class="card-body">
                <div id="users_chart"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                # Revenue
            </div>
            <div class="card-body">
                <div id="revenue_chart"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                # User Interaction with BOT
            </div>
            <div class="card-body">
                <div id="user_interaction_chart"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                # Number of Purchased Data Pack
            </div>
            <div class="card-body">
                <div id="data_pack_purchased"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                # Top 5 Purchased Data Pack
            </div>
            <div class="card-body">
                <div id="data_purchase_pack"></div>
            </div>
        </div>
    </div>


</div>

<?php require_once 'footer.php' ?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script>

    /**
     * Revenue Chart
     */
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(revenue_drawChart);

    function revenue_drawChart() {

        var revenue_data = new google.visualization.DataTable();
        revenue_data.addColumn('date', 'Date');
        revenue_data.addColumn('number', 'Rev');

        revenue_data.addRows([

            <?php

            if(count($dataPurchaseRevenue) > 0) {
                foreach ($dataPurchaseRevenue as $row) {
                    $date = date("Y, m-1, d", strtotime($row["date"]));
                    echo '[new Date('.$date.'),'.$row["revenue"].'],';
                }
            }else {
                echo '[new Date(2018, 01, 01), 0]';
            }

            ?>
        ]);


        var revenue_options = {
            title: "Revenue From <?= date('M d, Y', strtotime($start_date)) ?> to <?= date('M d, Y', strtotime($end_date)) ?>",
            hAxis: {
                format:'MMM d',
                gridlines: {color: 'red', count: 10}
            },
            vAxis: {
                gridlines: {color: 'red'},
                minValue: 0
            }
        };

        var revenue_chart = new google.visualization.LineChart(document.getElementById('revenue_chart'));

        revenue_chart.draw(revenue_data, revenue_options);

    }




    /**
     * Users Chart
     */
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(users_drawChart);

    function users_drawChart() {

        var users_data = new google.visualization.DataTable();
        users_data.addColumn('date', 'Date');
        users_data.addColumn('number', 'Users');

        users_data.addRows([

            <?php

            if(count($fbUsersDateGroup) > 0) {
                foreach ($fbUsersDateGroup as $row) {
                    $date = date("Y, m-1, d", strtotime($row["date"]));
                    echo '[new Date('.$date.'),'.$row["users"].'],';
                }
            }else {
                echo '[new Date(2018, 01, 01), 0]';
            }

            ?>
        ]);


        var users_options = {
            title: "Users <?= date('M d, Y', strtotime($start_date)) ?> to <?= date('M d, Y', strtotime($end_date)) ?> ",
            hAxis: {
                format:'MMM d',
                gridlines: {color: 'red', count: 10}
            },
            vAxis: {
                gridlines: {color: 'red'},
                minValue: 0
            }
        };

        var users_chart = new google.visualization.LineChart(document.getElementById('users_chart'));

        users_chart.draw(users_data, users_options);

    }




    /**
     * User Interaction Chart
     */
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(user_interaction_drawChart);

    function user_interaction_drawChart() {

        var user_interaction_data = new google.visualization.DataTable();
        user_interaction_data.addColumn('date', 'Date');
        user_interaction_data.addColumn('number', 'Inter');

        user_interaction_data.addRows([

            <?php

            if(count($fbUsersDateGroup) > 0) {
                foreach ($fbUsersDateGroup as $row) {
                    $date = date("Y, m-1, d", strtotime($row["date"]));
                    echo '[new Date('.$date.'),'.$row["users"].'],';
                }
            }else {
                echo '[new Date(2018, 01, 01), 0]';
            }

            ?>
        ]);


        var user_interaction_options = {
            title: "Users <?= date('M d, Y', strtotime($start_date)) ?> to <?= date('M d, Y', strtotime($end_date)) ?> ",
            hAxis: {
                format:'MMM d',
                gridlines: {color: 'red', count: 10}
            },
            vAxis: {
                gridlines: {color: 'red'},
                minValue: 0
            }
        };

        var user_interaction_chart = new google.visualization.LineChart(document.getElementById('user_interaction_chart'));

        user_interaction_chart.draw(user_interaction_data, user_interaction_options);

    }




    /**
     * Data Pack Purchased Chart
     */
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(data_pack_purchased_drawChart);

    function data_pack_purchased_drawChart() {

        var data_pack_purchased_data = new google.visualization.DataTable();
        data_pack_purchased_data.addColumn('date', 'Date');
        data_pack_purchased_data.addColumn('number', 'No');

        data_pack_purchased_data.addRows([

            <?php

            if(count($dataPurchaseSuccessDateGroup) > 0) {
                foreach ($dataPurchaseSuccessDateGroup as $row) {
                    $date = date("Y, m-1, d", strtotime($row["date"]));
                    echo '[new Date('.$date.'),'.$row["purchased"].'],';
                }
            }else {
                echo '[new Date(2018, 01, 01), 0]';
            }

            ?>
        ]);


        var data_pack_purchased_options = {
            title: "Users <?= date('M d, Y', strtotime($start_date)) ?> to <?= date('M d, Y', strtotime($end_date)) ?> ",
            hAxis: {
                format:'MMM d',
                gridlines: {color: 'red', count: 10}
            },
            vAxis: {
                gridlines: {color: 'red'},
                minValue: 0
            }
        };

        var data_pack_purchased_chart = new google.visualization.LineChart(document.getElementById('data_pack_purchased'));

        data_pack_purchased_chart.draw(data_pack_purchased_data, data_pack_purchased_options);

    }




    /*
     * Packages and Count
     */

    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(data_purchase_pack_drawBasic);

    function data_purchase_pack_drawBasic() {

        let data_purchase_pack_data = google.visualization.arrayToDataTable([
            ['Package', 'Package',],
            <?php

            if(count($topDataPurchasePackagesCount)) {
                foreach($topDataPurchasePackagesCount as $row){
                    $pack_name = $row["pack_name"];
                    $count = $row["count"];

                    echo '["'.$pack_name.'",'.$count.'],';
                }
            }else {
                echo '["Package",0],';
            }


            ?>
        ]);

        let data_purchase_pack_options = {
            title: "Top 5 Data Purchased Pack From <?= date('M d, Y', strtotime($start_date)) ?> to <?= date('M d, Y', strtotime($end_date)) ?> ",
            chartArea: {width: '50%'},
            hAxis: {
                title: 'Package Purchased',
                minValue: 0
            },
            vAxis: {
                title: 'Data Package'
            }
        };

        let data_purchase_pack_chart = new google.visualization.BarChart(document.getElementById('data_purchase_pack'));

        data_purchase_pack_chart.draw(data_purchase_pack_data, data_purchase_pack_options);
    }



</script>


