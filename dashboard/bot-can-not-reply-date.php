<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/bot-reply-controller.php';

unauthorizedUserRedirect('login.php');

ini_set('memory_limit', -1);

$date = $_GET['date'];

if(empty($date)) {
    header('Location: bot-can-not-reply.php');
}else{
    $botCanNotReplies = botCanNotReplies(date('d-m-Y', strtotime($date)));
}



?>


<?php setPageTitle('Dashboard - Bot Can Not Reply'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>


<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <div class="float-left">
            <i class="fas fa-table"></i>
            Bot Can Not Reply ( <?= date('d M Y', strtotime($date)) ?> )  , <strong>Total <?= count($botCanNotReplies) ?> </strong>
        </div>
       <!-- <div class="float-right">
            <strong>From </strong> 12-10-2018 <strong>To </strong> 12-10-2018
        </div>-->

    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
               <thead>
                   <tr>
                       <th>Serial</th>
                       <th>PSID</th>
                       <th>User</th>
                       <th>Message</th>
                       <th>Date</th>
                       <th>Time</th>
                   </tr>
               </thead>
               <tbody>
                   <?php $idx = 1; ?>
                   <?php foreach($botCanNotReplies as $row): ?>
                   
                   <tr>
                      <td><?= $idx ?></td>
                      <td><?= $row['receipent'] ?></td>
                      <td><img src="<?= fbUserPic($row['receipent']) ?>" width="60px" ></td>
                      <td><?= $row['message_content']?></td>
                      <td><?= $row['date']?></td>
                      <td><?= $row['time']?></td>
                     
                   </tr>
                   <?php $idx++ ?>
                   <?php endforeach; ?>
               </tbody>
            </table>


        </div>
        
        
        </div>
    </div>
   <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
</div>

<!--<p class="small text-center text-muted my-5">
    <em>More table examples coming soon...</em>
</p>
-->


<?php require_once 'footer.php' ?>
