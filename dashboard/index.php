<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/security/functions.php';
require_once 'app/dashboard-controller.php';

unauthorizedUserRedirect('login.php');

date_default_timezone_set('Asia/Dhaka');

if (isset($_GET['filter']) && $_GET['filter'] === 'last_7_days') {
    $selected = 'Last 7 Days';

    $end_date = date('Y-m-d', strtotime('today - 1 days'));
    $start_date =  date('Y-m-d', strtotime('today - 8 days'));
    
    # Total User
    $traffic = trafficDateGroup($start_date, $end_date);
    //$trafficCount = trafficCount($start_date, $end_date);
    $trafficCount = trafficCount($traffic);
    
    $newUserDateRange = newUserDateRange($start_date, $end_date);
    $repeatedUserDateRange = $trafficCount - $newUserDateRange;
    
    
    # REVENUE
    $revenue = revenueDateGroup($start_date, $end_date);
    $revenueCount = revenueCount($revenue);
    
    # DATA PURCAHSED
    $dataPurchased = dataPurchasedDateGroup($start_date, $end_date);
    $dataPurchasedCount = dataPurchasedCount($dataPurchased);
    $dataPackageCount = dataPackageCount($start_date, $end_date);
    
    # USER INTERACTION
    $usersInteractionWithBot = user2BotInteractionDateGroup($start_date, $end_date);
    $usersInteractionWithBotCount = user2BotInteractionCount($usersInteractionWithBot);
    
    # RECHARGE
    $recharge = rechargeDateGroup($start_date, $end_date);
    $rechargeCount = rechargeCount($recharge);

    # BESTOFFER
    $bestOfferHits = bestOfferCampaignHit($start_date, $end_date);
    

}elseif (isset($_GET['filter']) && $_GET['filter'] === 'last_30_days') {
    $selected = 'Last 30 Days';

    $end_date = date('Y-m-d', strtotime('today - 1 days'));
    $start_date =  date('Y-m-d', strtotime('today - 31 days'));
    
     # TRAFFIC
    $traffic = trafficDateGroup($start_date, $end_date);
    //$trafficCount = trafficCount($start_date, $end_date);
    $trafficCount = trafficCount($traffic);
    
    $newUserDateRange = newUserDateRange($start_date, $end_date);
    $repeatedUserDateRange = $trafficCount - $newUserDateRange;
    
     # REVENUE
    $revenue = revenueDateGroup($start_date, $end_date);
    $revenueCount = revenueCount($revenue);
    
    # DATA PURCAHSED
    $dataPurchased = dataPurchasedDateGroup($start_date, $end_date);
    $dataPurchasedCount = dataPurchasedCount($dataPurchased);
    $dataPackageCount = dataPackageCount($start_date, $end_date);
    
    # USER INTERACTION
    $usersInteractionWithBot = user2BotInteractionDateGroup($start_date, $end_date);
    $usersInteractionWithBotCount = user2BotInteractionCount($usersInteractionWithBot);
    
    # RECHARGE
    $recharge = rechargeDateGroup($start_date, $end_date);
    $rechargeCount = rechargeCount($recharge);

    # BESTOFFER
    $bestOfferHits = bestOfferCampaignHit($start_date, $end_date);
    
}
elseif (isset($_GET['start_date']) && isset($_GET['end_date']) && !empty($_GET['start_date']) && !empty($_GET['end_date'])) {
    $selected = 'custom_date';
    
    $start_date =  date('Y-m-d', strtotime($_GET['start_date']));
    $end_date = date('Y-m-d', strtotime($_GET['end_date']));
    
    # TRAFFIC
    $traffic = trafficDateGroup($start_date, $end_date);
   // $trafficCount = trafficCount($start_date, $end_date);
    $trafficCount = trafficCount($traffic);
    $newUserDateRange = newUserDateRange($start_date, $end_date);
    $repeatedUserDateRange = $trafficCount - $newUserDateRange;
    
     # REVENUE
    $revenue = revenueDateGroup($start_date, $end_date);
    $revenueCount = revenueCount($revenue);
    
    # DATA PURCAHSED
    $dataPurchased = dataPurchasedDateGroup($start_date, $end_date);
    $dataPurchasedCount = dataPurchasedCount($dataPurchased);
    $dataPackageCount = dataPackageCount($start_date, $end_date);
    
    # USER INTERACTION
    $usersInteractionWithBot = user2BotInteractionDateGroup($start_date, $end_date);
    $usersInteractionWithBotCount = user2BotInteractionCount($usersInteractionWithBot);
    
    # RECHARGE
    $recharge = rechargeDateGroup($start_date, $end_date);
    $rechargeCount = rechargeCount($recharge);

    # BESTOFFER
    $bestOfferHits = bestOfferCampaignHit($start_date, $end_date);
    
}
elseif (isset($_GET['month']) && isset($_GET['year']) && !empty($_GET['month']) && !empty($_GET['year'])) {
    
    $selected = 'month';
    
    $month = date("n",strtotime($_GET['month']));
    $month_year = $_GET['month'].' '.$_GET['year'];
    $year = $_GET['year'];

    $end_date = date('Y-m-d', strtotime('today - 1 days'));
    $start_date =  date('Y-m-d', strtotime('today - 31 days'));
    
    #TRAFFIC
    $traffic = trafficMonth($month, $year);
    //$trafficCount = trafficCountMonth($month, $year);
    $trafficCount = trafficCount($traffic);
    $newUserDateRange = newUserMonth($month, $year);
    $repeatedUserDateRange = $trafficCount - $newUserDateRange;
    
    
    #REVENUE
    $revenue = revenueMonth($month, $year);
    $revenueCount = revenueCount($revenue);
    
    # DATA PURCHASED
    $dataPurchased = dataPurchasedMonth($month, $year);
    $dataPurchasedCount = dataPurchasedCount($dataPurchased);
    $dataPackageCount = dataPackageCountMonth($month, $year);
    
    # USER INTERACTION
    $usersInteractionWithBot = user2BotInteractionMonth($month, $year);
    $usersInteractionWithBotCount = user2BotInteractionCount($usersInteractionWithBot);
    
    # RECHARGE
    $recharge = rechargeMonth($month, $year);
    $rechargeCount = rechargeCount($recharge);

    # BESTOFFER
    $bestOfferHits = bestOfferCampaignHitMonth($month, $year);

    
}elseif (isset($_GET['filter']) && $_GET['filter'] === 'till_now') {
    
    $selected = 'Till Today';
    
    
    #TRAFFIC
    $traffic = totalTrafficDateGroup();
    //$trafficCount = totalTraffic();
    $trafficCount = trafficCount($traffic);
    $newUserDateRange = totalNewUser();
    $repeatedUserDateRange = $trafficCount - $newUserDateRange;
    
    #REVENUE
    $revenue = totalRevenueDateGroup();
    $revenueCount = revenueCount($revenue);
    
    # DATA PURCHASED
    $dataPurchased = totalDataPurchasedDateGroup();
    $dataPurchasedCount = dataPurchasedCount($dataPurchased);
    $dataPackageCount = totalDataPackageCount();
    
    # USER INTERACTION
    $usersInteractionWithBot = totalUser2BotInteractionDateGroup();
    $usersInteractionWithBotCount = user2BotInteractionCount($usersInteractionWithBot);
    
    # RECHARGE
    $recharge = totalRechargeDateGroup();
    $rechargeCount = rechargeCount($recharge);

    # BESTOFFER
    $bestOfferHits = totalBestOfferCampaignHitMonth();

    
}else {
    $selected = 'Last 30 Days';

    $end_date = date('Y-m-d', strtotime('today - 1 days'));
    $start_date =  date('Y-m-d', strtotime('today - 31 days'));
    
    # TRAFFIC
    $traffic = trafficDateGroup($start_date, $end_date);
   // $trafficCount = trafficCount($start_date, $end_date);
    $trafficCount = trafficCount($traffic);
    $newUserDateRange = newUserDateRange($start_date, $end_date);
    $repeatedUserDateRange = $trafficCount - $newUserDateRange;
    
     # REVENUE
    $revenue = revenueDateGroup($start_date, $end_date);
    $revenueCount = revenueCount($revenue);
    
    # DATA PURCAHSED
    $dataPurchased = dataPurchasedDateGroup($start_date, $end_date);
    $dataPurchasedCount = dataPurchasedCount($dataPurchased);
    $dataPackageCount = dataPackageCount($start_date, $end_date);
    
    # USER INTERACTION
    $usersInteractionWithBot = user2BotInteractionDateGroup($start_date, $end_date);
    $usersInteractionWithBotCount = user2BotInteractionCount($usersInteractionWithBot);
    
    # RECHARGE
    $recharge = rechargeDateGroup($start_date, $end_date);
    $rechargeCount = rechargeCount($recharge);

    # BESTOFFER
    $bestOfferHits = bestOfferCampaignHit($start_date, $end_date);


}

$todaysTraffic = todaysTraffic();
$todaysNewUser = todaysNewUser();
/*$todaysRepeatedUser = todaysRepeatedUser();*/
$todaysRepeatedUser = $todaysTraffic - $todaysNewUser;
$todaysRevenue = todaysRevenue();
$todayUser2BotInteraction = todayUser2BotInteraction();
$todaysDataPurchased = todaysDataPurchased();
$todaysRecharge = todaysRecharge();
$todaysTrafficGroupByTime = todaysTrafficGroupByTime();
$todaysBestOfferCampaignHit = todaysBestOfferCampaignHit();


?>

<?php setPageTitle('Dashboard'); ?>
<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>

<!-- Revenue with Date -->

<style>

.card {
    margin-bottom: 15px;
    border-radius: 0;
}

.card-header {
    background: white;
    font-size:  20px;
    color: black;
    font-weight: bold;
}

.card-footer {
    background: slategray;
    color:  white;
    font-weight: bold;
}

.today {
    color: red;
    font-size: 20px;
    font-family: Orbitron;
    letter-spacing: 7px;
}
.clock {
    color: red;
    font-size: 25px;
    font-family: Orbitron;
    letter-spacing: 7px;
}

#filter {
   border-radius: 0; 
}

</style>


<div class="row justify-content-center">
    <div class="col-lg-6 text-center">
        <h4>Welcome to Airtel Buzz Dashboard</h4>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-lg-6 text-center mb-2">
        <div id="MyClockDisplay" class="clock"></div>
    </div>
</div>

<div class="row">
    <div class="col-lg-2 mb-3">
        <select class="form-control" id="filter">
            <option value="">Select Filter</option>
            <option value="last_7_days" <?= ($selected === 'Last 7 Days') ? '' : null ?> >Last 7 Days</option>
            <option value="last_30_days" <?= ($selected === 'Last 30 Days') ? '' : null ?> >Last 30 Days</option>
            <option value="month_wise" <?= ($selected === 'month') ? '' : null ?> >Month Wise</option>
            <option value="custom_date" <?= ($selected === 'custom_date') ? '' : null ?> >Custom Date</option>
            <option value="till_now" <?= ($selected === 'till_now') ? '' : null ?> >Till Now</option>
            <option value="none" <?= ($selected === 'none') ? '' : null ?> >None</option>
        </select>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
            <span class="head">Today's Airtel Buzz Status</span>
        </div>
        
        <div class="card-body todays-overview">
            <div class="row">
                
                
                                           
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="fas fa-users fa-2x"></i>
                                </div>
                                <div class="col-lg-8">
                                    <h3><?= number_format($todayUser2BotInteraction, 0, '', ','); ?></h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            User to BOT Interaction
                        </div>
                    </div>
                </div>
                
                
                                                
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="fas fa-users fa-2x"></i>
                                </div>
                                <div class="col-lg-8">
                                    <h3><?= (int)$todaysTraffic ?></h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-center">
                             Total User
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="fas fa-users fa-2x"></i>
                                </div>
                                <div class="col-lg-8">
                                    <h3><?= (int)$todaysRepeatedUser ?></h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-center">
                             Repeated User
                        </div>
                    </div>
                </div>
               
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="far fa-money-bill-alt fa-2x"></i>
                                </div>
                                <div class="col-lg-8">
                                    <h3><?= (double)$todaysRevenue ?> ৳</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            Data Purchased Revenue
                        </div>
                    </div>
                </div>
                
                            
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="fas fa-shopping-cart fa-2x"></i>
                                </div>
                                <div class="col-lg-8">
                                    <h3><?= (int)$todaysDataPurchased ?></h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            No Of Data Purchased
                        </div>
                    </div>
                </div>
                
                
                
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="far fa-money-bill-alt fa-2x"></i>
                                </div>
                                <div class="col-lg-8">
                                    <h3><?= (double)$todaysRecharge['amount'] ?> ৳</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            Recharge Revenue
                        </div>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="far fa-money-bill-alt fa-2x"></i>
                                </div>
                                <div class="col-lg-8">
                                    <h3><?= $todaysBestOfferCampaignHit ?></h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            #bestoffer Hits
                        </div>
                    </div>
                </div>
            
                
<!--                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="fas fa-users fa-2x"></i>
                                </div>
                                <div class="col-lg-8">
                                    <h3></h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-center">
                             Total User
                        </div>
                    </div>
                </div>-->
                
            </div>
        </div>
    </div>
</div>

</div>



<div class="row">
    
     <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
            <span class="head">Airtel Buzz Status
            
            (<?php
                if ($selected == 'month') {
                    echo $month_year;
                }elseif ($selected == 'custom_date'){
                    
                    echo date('M d, Y',strtotime($start_date)).' to '.date('M d, Y',strtotime($end_date));
                }else {
                    echo $selected;
                }
            ?>)</span>
        </div>
        
        <div class="card-body custom-overview">
            
              <div class="row">
                  
                  
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="fas fa-users fa-2x"></i>
                                </div>
                                <div class="col-lg-8">
                                    <h3><?= number_format($usersInteractionWithBotCount['total'], 0, '', ','); ?></h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                             User to BOT Interaction
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="fas fa-users fa-2x"></i>
                                </div>
                                <div class="col-lg-8">
                                    <h3><?= $trafficCount ?></h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-center">
                             Total User
                        </div>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="fas fa-users fa-2x"></i>
                                </div>
                                <div class="col-lg-8">
                                    <h3><?= $repeatedUserDateRange ?></h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-center">
                             Repeated User
                        </div>
                    </div>
                </div>
            
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="far fa-money-bill-alt fa-2x"></i>
                                </div>
                                <div class="col-lg-8">
                                    <h3><?= number_format($revenueCount['total'], 0, '', ','); ?> ৳</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            Data Purchased Revenue
                        </div>
                    </div>
                </div>
            
            
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="fas fa-shopping-cart fa-2x"></i>
                                </div>
                                <div class="col-lg-8">
                                    <h3><?= number_format($dataPurchasedCount['total'], 0, '', ',');  ?></h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            No Of Data Purchased
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="far fa-money-bill-alt fa-2x"></i>
                                </div>
                                <div class="col-lg-8">
                                    <h3><?= number_format($rechargeCount['total'], 0, '', ','); ?> ৳</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            Recharge Revenue
                        </div>
                    </div>
                </div>

                  <div class="col-lg-3">
                      <div class="card">
                          <div class="card-body text-center">
                              <div class="row">
                                  <div class="col-lg-4">
                                      <i class="far fa-money-bill-alt fa-2x"></i>
                                  </div>
                                  <div class="col-lg-8">
                                      <h3><?= $bestOfferHits ?></h3>
                                  </div>
                              </div>
                          </div>
                          <div class="card-footer">
                              #bestoffer Hits
                          </div>
                      </div>
                  </div>
                
            </div>
            
        </div>
    </div>
</div>

</div>

        
<div class="row">
    
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                # Total User per Day
                
                 (<?php
                if($selected == 'month') {
                    echo $month_year;
                }elseif($selected == 'custom_date'){
                    
                    echo date('M d, Y',strtotime($start_date)).' to '.date('M d, Y',strtotime($end_date));
                }else {
                    echo $selected;
                }
            ?>)
            </div>
            <div class="card-body">
                <div id="users_chart"></div>
            </div>
        </div>
    </div>
    


    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                # Data Purchased Revenue
                
                 (<?php
                if($selected == 'month') {
                    echo $month_year;
                }elseif($selected == 'custom_date'){
                    
                    echo date('M d, Y',strtotime($start_date)).' to '.date('M d, Y',strtotime($end_date));
                }else {
                    echo $selected;
                }
            ?>)
            
            </div>
            <div class="card-body">
                <div id="revenue_chart"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                # User Interaction with BOT
                
                (<?php
                if($selected == 'month') {
                    echo $month_year;
                }elseif($selected == 'custom_date'){
                    
                    echo date('M d, Y',strtotime($start_date)).' to '.date('M d, Y',strtotime($end_date));
                }else {
                    echo $selected;
                }
            ?>)
            
            </div>
            <div class="card-body">
                <div id="user_interaction_chart"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                # Number of Purchased Data Pack
                
                 (<?php
                if($selected == 'month') {
                    echo $month_year;
                }elseif($selected == 'custom_date'){
                    
                    echo date('M d, Y',strtotime($start_date)).' to '.date('M d, Y',strtotime($end_date));
                }else {
                    echo $selected;
                }
            ?>)
            
            </div>
            <div class="card-body">
                <div id="data_pack_purchased"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                # Top Purchased Data Pack
                
                 (<?php
                if($selected == 'month') {
                    echo $month_year;
                }elseif($selected == 'custom_date'){
                    
                    echo date('M d, Y',strtotime($start_date)).' to '.date('M d, Y',strtotime($end_date));
                }else {
                    echo $selected;
                }
            ?>)
            
            </div>
            <div class="card-body">
                <div id="data_purchase_pack"></div>
            </div>
        </div>
    </div>
    
    
    
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                # Recharge
                
                 (<?php
                if($selected == 'month') {
                    echo $month_year;
                }elseif($selected == 'custom_date'){
                    
                    echo date('M d, Y',strtotime($start_date)).' to '.date('M d, Y',strtotime($end_date));
                }else {
                    echo $selected;
                }
            ?>)
            
            </div>
            <div class="card-body">
                <div id="rechargeChart"></div>
            </div>
        </div>
    </div>
    


</div>

<?php require_once 'footer.php' ?>

<script>

        $( "#filter" ).on('change',function() {

            let filter = $(this).val();

           if(filter === 'last_7_days'){
                let url = "index.php?filter=last_7_days";
                $( location ).attr("href", url);
            }else if(filter === 'last_30_days'){
                let url = "index.php?filter=last_30_days";
                $( location ).attr("href", url);
            }
            else if(filter === 'none'){
                let url = "index.php";
                $( location ).attr("href", url);
            }
            else if(filter === 'till_now'){
                let url = "index.php?filter=till_now";
                $( location ).attr("href", url);
            }else if(filter === 'custom_date') {
                $('#date_filter').modal('show');
            }else if(filter === 'month_wise') {
                $('#month_filter').modal('show');
            }

        });
        
        
        

    /**
     * Revenue Chart
     */
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(revenue_drawChart);

    function revenue_drawChart() {

        var revenue_data = new google.visualization.DataTable();
        revenue_data.addColumn('date', 'Date');
        revenue_data.addColumn('number', 'Taka');

        revenue_data.addRows([

            <?php

            if(count($revenue) > 0) {
                foreach ($revenue as $row) {
                    $date = date("Y, m-1, d", strtotime($row["date"]));
                    echo '[new Date('.$date.'),'.$row["revenue"].'],';
                }
            }else {
                echo '[new Date(2018, 01, 01), 0]';
            }

            ?>
        ]);


        var revenue_options = {
            title: "Revenue",
            hAxis: {
                format:'MMM d',
                gridlines: {color: 'red', count: 10}
            },
            vAxis: {
                gridlines: {color: 'red'},
                minValue: 0
            }
        };

        var revenue_chart = new google.visualization.LineChart(document.getElementById('revenue_chart'));
    
        <?php if(count($revenue)): ?>
        revenue_chart.draw(revenue_data, revenue_options);
        <?php else: ?>
        document.getElementById('revenue_chart').innerHTML = '<small>No data available</small>';
        <?php endif; ?>

    }




    /**
     * Total User
     */
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(users_drawChart);

    function users_drawChart() {

        var users_data = new google.visualization.DataTable();
        users_data.addColumn('date', 'Date');
        users_data.addColumn('number', 'Users');

        users_data.addRows([

            <?php

            if(count($traffic) > 0) {
                foreach ($traffic as $row) {
                    $date = date("Y, m-1, d", strtotime($row["date"]));
                    echo '[new Date('.$date.'),'.$row["users"].'],';
                }
            }/*else {
                echo '[new Date(2018, 01, 01), 0]';
            }*/

            ?>
        ]);


        var users_options = {
            title: "Total User",
            hAxis: {
                format:'MMM d',
                gridlines: {color: 'red', count: 10}
            },
            vAxis: {
                gridlines: {color: 'red'},
                minValue: 0
            }
        };

        var users_chart = new google.visualization.LineChart(document.getElementById('users_chart'));

        users_chart.draw(users_data, users_options);

    }




    /**
     * User Interaction Chart
     */
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(user_interaction_drawChart);

    function user_interaction_drawChart() {

        var user_interaction_data = new google.visualization.DataTable();
    
        user_interaction_data.addColumn('date', 'Date');
        user_interaction_data.addColumn('number', 'Int');
        //user_interaction_data.addColumn({type: 'number', role: 'annotation'});

        user_interaction_data.addRows([
           
            <?php

            if(count($usersInteractionWithBot) > 0) {
                foreach ($usersInteractionWithBot as $row) {
                    $date = date("Y, m-1, d", strtotime($row["date"]));
                    echo '[new Date('.$date.'),'.$row["users"].'],';
                }
            }else {
                echo '[new Date(2018, 01, 01), 0]';
            }

            ?>
        ]);


        var user_interaction_options = {
            title: "User to Bot Interaction",
            hAxis: {
                format:'MMM d',
                gridlines: {color: 'red', count: 10}
            },
            vAxis: {
                gridlines: {color: 'red'},
                minValue: 0
            }
        };

        var user_interaction_chart = new google.visualization.LineChart(document.getElementById('user_interaction_chart'));

        user_interaction_chart.draw(user_interaction_data, user_interaction_options);

    }




    /**
     * Data Pack Purchased Chart
     */
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(data_pack_purchased_drawChart);

    function data_pack_purchased_drawChart() {

        var data_pack_purchased_data = new google.visualization.DataTable();
        data_pack_purchased_data.addColumn('date', 'Date');
        data_pack_purchased_data.addColumn('number', 'Purchased');

        data_pack_purchased_data.addRows([

            <?php

            if(count($dataPurchased) > 0) {
                foreach ($dataPurchased as $row) {
                    $date = date("Y, m-1, d", strtotime($row["date"]));
                    echo '[new Date('.$date.'),'.$row["purchased"].'],';
                }
            }else {
                echo '[new Date(2018, 01, 01), 0]';
            }

            ?>
        ]);


        var data_pack_purchased_options = {
            title: "Data Pack Purchased",
            hAxis: {
                format:'MMM d',
                gridlines: {color: 'red', count: 10}
            },
            vAxis: {
                gridlines: {color: 'red'},
                minValue: 0
            }
        };

        var data_pack_purchased_chart = new google.visualization.LineChart(document.getElementById('data_pack_purchased'));
        
        <?php if(count($dataPurchased)): ?>
            data_pack_purchased_chart.draw(data_pack_purchased_data, data_pack_purchased_options);
        <?php else: ?>
            document.getElementById('data_pack_purchased').innerHTML = '<small>No data available</small>';
        
        <?php endif; ?>

    }




    /*
     * Packages and Count
     */

    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(data_purchase_pack_drawBasic);

    function data_purchase_pack_drawBasic() {

        let data_purchase_pack_data = google.visualization.arrayToDataTable([
            ['Package', 'Package',],
            <?php

            if(count($dataPackageCount)) {
                foreach($dataPackageCount as $row){
                    $pack_name = $row["pack_name"];
                    $count = $row["count"];

                    echo '["'.$pack_name.'",'.$count.'],';
                }
            }else {
                echo '["Package",0],';
            }


            ?>
        ]);

        let data_purchase_pack_options = {
            title: "Top Data Purchased Packages",
            chartArea: {width: '50%'},
            hAxis: {
                title: 'Package Purchased',
                minValue: 0
            },
            vAxis: {
                title: 'Data Package'
            }
        };

        let data_purchase_pack_chart = new google.visualization.BarChart(document.getElementById('data_purchase_pack'));
        
        <?php if(count($dataPackageCount) > 1): ?>
         data_purchase_pack_chart.draw(data_purchase_pack_data, data_purchase_pack_options);
        <?php elseif(count($dataPackageCount) === 1): ?>
        
        
        document.getElementById('data_purchase_pack').innerHTML = "<table class='table table-borderd'><tr><td>Package</td><td>Purchased</td></tr><tr><td><?= $dataPackageCount[0]['pack_name'] ?></td><td><?= $dataPackageCount[0]['count'] ?></td></tr></table>";
        
        <?php else: ?>
        document.getElementById('data_purchase_pack').innerHTML = '<small>No data available</small>';
        <?php endif;?>
    
    }
    
    
    /**
     * Recharge
     */
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(recharge_drawChart);

    function recharge_drawChart() {

        var recharge_data = new google.visualization.DataTable();
        recharge_data.addColumn('date', 'Date');
        recharge_data.addColumn('number', 'Taka');

        recharge_data.addRows([

            <?php

            if(count($recharge) > 0) {
                foreach ($recharge as $row) {
                    $date = date("Y, m-1, d", strtotime($row["date"]));
                    echo '[new Date('.$date.'),'.$row["amount"].'],';
                }
            }else {
                echo '[new Date(2018, 01, 01), 0]';
            }

            ?>
        ]);


        var recharge_options = {
            title: "Recharge Revenue",
            hAxis: {
                format:'MMM d',
                gridlines: {color: 'red', count: 10}
            },
            vAxis: {
                gridlines: {color: 'red'},
                minValue: 0
            }
        };

        var recharge_chart = new google.visualization.LineChart(document.getElementById('rechargeChart'));

        
        
        
         <?php if(count($recharge) > 1): ?>
         recharge_chart.draw(recharge_data, recharge_options);
        <?php elseif(count($recharge) === 1): ?>
        document.getElementById('recharge').innerHTML = "<table class='table table-borderd'><tr><td>Date</td><td>Amount</td></tr><tr><td><?= date('M d, Y',strtotime($recharge[0]['date'])) ?></td><td><?= $recharge[0]['amount'] ?></td></tr></table>";
        <?php else: ?>
        document.getElementById('recharge').innerHTML = '<small>No data available</small>';
        <?php endif;?>

    }


    
    
    // Clock
    
    function showTime(){
    var date = new Date();
    var h = date.getHours(); // 0 - 23
    var m = date.getMinutes(); // 0 - 59
    var s = date.getSeconds(); // 0 - 59
    var session = "AM";
    
    if(h == 0){
        h = 12;
    }
    
    if(h > 12){
        h = h - 12;
        session = "PM";
    }
    
    h = (h < 10) ? "0" + h : h;
    m = (m < 10) ? "0" + m : m;
    s = (s < 10) ? "0" + s : s;
    
    var time = h + ":" + m + ":" + s + " " + session;
    document.getElementById("MyClockDisplay").innerText = time;
    document.getElementById("MyClockDisplay").textContent = time;
    
    setTimeout(showTime, 1000);
    
}

//showTime();



</script>


