<?php

require_once 'app/general/functions.php';
require_once 'app/auth-controller.php';

if(isset($_POST['login'])) {

    $values = array(
         'email' => $_POST['email'],
        'password' => $_POST['password']
    );

    $ststus = userLogin($values);
}

authorizedUserRedirect('index.php');

?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login - Airtel Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/admin.css" rel="stylesheet">

      <link rel="shortcut icon" href="images/airtel_logo.png">

    <style>
    .card-login {
    	margin-top: 150px;
    }

    .card-header {
    	color: #f81416;
    	background: #fff;
    	font-family: sans-serif;
    	font-size: 20px;
    	font-weight: 600 !important;
    	margin-top: 10px;
    	border-bottom: 0;
    }

	.input-group-prepend span{
		width: 50px;
		background-color: #f81416;
		color: #fff;
		border:0 !important;
	}

	input:focus{
		outline: 0 0 0 0  !important;
		box-shadow: 0 0 0 0 !important;
	}

	.login_btn{
		width: 130px;
	}

	.login_btn:hover{
		color: #fff;
		background-color: #f81416;
	}

</style>
  </head>

  <body>
  	<div class="container">
      <div class="card card-login mx-auto text-center">
        <div class="card-header mx-auto">
        	<span> <img src="images/airtel_logo.png" alt="Logo"> </span> <br/>
        	<span class="logo_title"> Login Airtel Dashboard </span>
        	
        </div>
        <div class="card-body">
          	<form method="post" action="login.php" enctype="multipart/form-data">

                <?php
                if(isset($_POST['login'])) {
                    if ($ststus === 'passw_wrong'){
                        echo alert('Email or password is wrong.', 'warning');
                    }elseif($ststus === 'fields_empty') {
                        echo alert('Email and password are required.', 'warning');
                    }elseif($ststus === 'exists_false') {
                        echo alert('Email does not exists.', 'warning');
                    }
                }

                ?>

				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-user"></i></span>
					</div>
					<input type="text" name="email" class="form-control" placeholder="Email" required>
				</div>

				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-key"></i></span>
					</div>
					<input type="password" name="password" class="form-control" placeholder="Password" required>
				</div>

				<div class="form-group">
					<input type="submit" name="login" value="Login" class="btn btn-outline-danger float-right login_btn">
				</div>

			</form>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/admin.min.js"></script>

  </body>

</html>
