<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/acm-controller.php';

if (isset($_GET['filter']) && $_GET['filter'] === 'success') {
    $dataPurchaseHistory = successDataPurchaseHistory();
} elseif (isset($_GET['filter']) && $_GET['filter'] === 'failed') {
    $dataPurchaseHistory = failedDataPurchaseHistory();
} else {
    $dataPurchaseHistory = dataPurchaseHistory();
}

$total = totalDataPurchased();


?>


<?php setPageTitle('Dashboard'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>


<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Data Purchase <strong>( Till Now <?= $total ?> TK )</strong></div>
    <div class="card-body">
        
        <div class="row">
            <div class="col-lg-2 mb-3">
                <select class="form-control" id="filter">
                <option value="">Status Filter</option>
                <option value="success">Sucess</option>
                <option value="failed">Failed</option>
                <option value="none">None</option>
                </select>
            </div>
        </div>
        
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>MSISDN</th>
                    <th>PACK NAME</th>
                    <th>PACK PRICE</th>
                    <th>STATUS</th>
                    <th>DATE</th>
                    <th>TIME</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($dataPurchaseHistory as $row): ?>
                <tr>
                    <td><?= safeOutput($row['msisdn']) ?></td>
                    <td><?= safeOutput($row['pack_name']) ?></td>
                    <td><?= safeOutput($row['pack_price']) ?></td>
                    <td><?= ($row['deducted_amount'] > 0) ? safeOutput('success') :  safeOutput('failed') ?></td>
                    <td><?= safeOutput($row['date']) ?></td>
                    <td><?= safeOutput($row['time']) ?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer small text-muted"></div>
</div>

<!--<p class="small text-center text-muted my-5">
    <em>More table examples coming soon...</em>
</p>
-->


<?php require_once 'footer.php' ?>


<script>

    
        $( "#filter" ).on('change',function() {

            let filter = $(this).val();

           if(filter === 'success'){
                let url = "data-purchase.php?filter=success";
                $( location ).attr("href", url);
            }else if(filter === 'failed'){
                let url = "data-purchase.php?filter=failed";
                $( location ).attr("href", url);
            }
            else if(filter === 'none'){
                let url = "data-purchase.php";
                $( location ).attr("href", url);
            }
        });


</script>

