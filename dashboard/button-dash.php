<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/security/functions.php';
require_once 'app/query-controller.php';

//unauthorizedUserRedirect('login.php');

ini_set('memory_limit', -1);

$buttonsAll = buttonsAll();

?>



<?php setPageTitle('Buttons'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>


<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <div class="float-left">
            <i class="fas fa-table"></i>
            Buttons
        </div>
    </div>
    
    <div class="card-body">

        <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <td>ID</td>
                <th>MAPPING</th>
                <th>BUTTON TYPE</th>
                <th>URL/TITLE</th>
                <th>TITLE/PAYLOAD</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($buttonsAll as $emp):?>

                <tr>
                    <td><?php echo $emp['id'] ?></td>
                    <td><?php echo $emp['mapping'] ?></td>
                    <td><?php echo $emp['btn_type'] ?></td>
                    <td><?php echo $emp['url_or_title'] ?></td>
                    <td><?php echo $emp['title_or_payload'] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        </div>
        
        
        </div>
    </div>
    <!--<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
</div>


<?php require_once 'footer.php';?>
