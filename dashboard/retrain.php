<?php include 'connection/connection.php';
// include 'connection/session.php';
include 'header.php';
mysqli_set_charset($link, "utf8"); 
$i=0;
$sql="SELECT id,date, COUNT(*) AS total FROM `bot_cannot_reply` GROUP BY date ORDER BY id DESC";
$record = mysqli_query($link, $sql);

?>
<!--    [Custom Stlesheet]-->
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

   <link href="jse/styles.css" rel="stylesheet">
<script src="jse/lumino.glyphs.js"></script>

 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript" src="//code.jquery.com/jquery-git.js"></script>
 <link rel="stylesheet" type="text/css" href="/css/result-light.css">
<style>
 .header-top {
    margin-top: -50px !important;
}
.no-padding {
	padding: 0px !important; margin: 10px !important;
}
.widget-right {
    padding: 12px !important;}
    
.text-muted {
    color: #222!important;
    padding: 5px !important;
    font-weight: 900 !important;
}    
button{
        width: 12%;
        
    margin-left: 23%;
    height: 43px;
    line-height: 42px;
    font-weight: bold;
    background: #CE2E35;
    border: 1px solid #CE2E35;
    color: #fff;
    transition: .3s;
    cursor: pointer;
    margin-top: 19px;
}
    .target1{
        margin-left: 110px;
    top: -50px;
}
.end{
        margin-left: 280px;
    margin-top: -65px;
}

.target2{
        margin-left: 380px;
    top: -50px;
}
.possible{
        margin-left: 155px;
    margin-top: 20px;
    margin-bottom: 25px;
}
.naim{
        margin-left: 230px;
}
.date1{
    width: 10%;
    margin-left: 245px;
    margin-top: -43px;
}
.month1{
    width: 10%;
    margin-left: 315px;
    margin-top: -38px;
}

.year1{
    width: 13%;
    margin-left: 385px;
    margin-top: -38px;
}

.date2{
    width: 10%;
    margin-left: 245px;
    margin-top: -43px;
}
.month2{
    width: 10%;
    margin-left: 315px;
    margin-top: -38px;
}

.year2{
    width: 13%;
    margin-left: 385px;
    margin-top: -38px;
}
</style>

<!--    [ Start Section Area]-->
<section id="summery" class="body-part">
    <div class="container">
    <div class="logo text-center"> 
        <img src="assets/img/robi_logo_white.png" width="100px">
    </div>
<br/>
    <h2 class="text-uppercase text-center form-group">RETRAIN QUERIES BOT CAN'T REPLY ON DAILY BASIS</h2>
    
              
         
  
		

		
		<div class="row">
		    	<div style="margin:10px;"> </div>
		    
		</div>
		
		
    
    <div class="user-table">
        <table class="table table-bordered">
            <thead>
            <tr>
               
                <th>ID</th>
                <th>DATE</th>
                <th>DAY</th>
             
            </tr>
            </thead>
            <tbody>
            <?php while ($emp = mysqli_fetch_assoc($record)) { $i++ ?>

                <tr>

                    
                    <td><?php echo $i ?></td>
                    <td><?php echo $emp['date'] ?></td>
                    <td>
                        
                        <?php
   
                        $date = $emp['date'];
                        $unixTimestamp = strtotime($date);
                        echo $dayOfWeek = date("l", $unixTimestamp);
                    
                    ?>
                        
                    </td>
                    
                 
                    <td>
                        <div class="btn-group">
                          
                            <a target="_blank" href="retrain-every-date.php?date=<?php echo $emp['date']?>" type="button"  class="btn btn-warning">Retrain</a>
                           
                        </div>
                    </td> 
                </tr>
            <?php  } ?>
            </tbody>
        </table>
        
    </div>
</div>
</div>
<!--    [Finish Section Area]-->
<?php include 'footer.php';?>
<script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () {
        $('.user-table table').DataTable();
    } );
</script>
