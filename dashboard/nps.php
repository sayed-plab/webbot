<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/nps-controller.php';

unauthorizedUserRedirect('login.php');

ini_set('memory_limit', -1);


if (isset($_POST['submit'])) {

    $data = array_filter(array(
        'reply' => $_POST['reply'],
        'from_date' => $_POST['from_date'],
        'to_date' => $_POST['to_date']
    ));

    header('Location: nps.php?'.http_build_query($data));
}


if (isset($_GET['page_no'])) {
    $page_no = $_GET['page_no'];
} else {
    $page_no = 1;
}
$no_of_records_per_page = 50;
$offset = ($page_no-1) * $no_of_records_per_page;

$total_rows = totalNPSFiltered();
$total_rows = 1 * $total_rows;
$total_pages = ceil($total_rows / $no_of_records_per_page);

$nps = npsFiltered($offset, $no_of_records_per_page);




?>


<?php setPageTitle('Dashboard - ICE NPS'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>


<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <div class="float-left">
            <i class="fas fa-table"></i>
            NPS
        </div>
       <!-- <div class="float-right">
            <strong>From </strong> 12-10-2018 <strong>To </strong> 12-10-2018
        </div>-->

    </div>

    <div class="card-body">

        <form action="nps.php" method="post">
            <div class="row">

                <div class="col mb-3">
                    <label for="reply">REPLY</label>
                    <select class="form-control" name="reply" id="reply">

                        <?php if (isset($_GET['reply']) && !empty($_GET['reply'])): ?>
                            <option value="<?= $_GET['reply'] ?>"><?= $_GET['reply'] ?></option>
                        <?php else: ?>
                            <option value="">Select Filter</option>
                        <?php endif; ?>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                        <option value="Comment">Comment</option>
                        <option value="NoReply">NoReply</option>
                    </select>
                </div>

                <div class="col mb-3">
                    <label for="from_date">FROM DATE</label>
                    <input type="date" class="form-control" name="from_date" id="from_date" value="<?php (isset($_GET['from_date'])) ? print $_GET['from_date'] : '' ?>">
                </div>

                <div class="col mb-3">
                    <label for="to_date">TO DATE</label>
                    <input type="date" class="form-control" name="to_date" id="to_date" value="<?php (isset($_GET['to_date'])) ? print $_GET['to_date'] : '' ?>">
                </div>

                <div class="col mb-3">
                    <button style="margin-top: 30px" class="btn btn-primary form-control" type="submit" name="submit" id="submit">SUBMIT</button>
                </div>
            </div>
        </form>


        <div class="table-responsive" id="iceNPS" >
            <table class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>RECIPIENT</th>
                    <th>MESSAGE</th>
                    <th>SENT DATE</th>
                    <th>SENT TIME</th>
                    <th>ANSWER</th>
                    <th>REPLY DATE</th>
                    <th>REPLY TIME</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($nps as $row): ?>
                <tr>
                    <td>
                        <?= $row['id']?>
                    </td>
                    <td>
                        <?= fbUserName($row['sender']) ?>
                    </td>

                    <td><?= $row['title']?> </td>

                    <td>
                        <?= $row['date'] ?>
                    </td>

                    <td>
                        <?= $row['time'] ?>
                    </td>

                    <td>
                        <?= $row['answer'] ?>
                    </td>

                    <td>
                        <?= $row['s_date'] ?>
                    </td>
                    
                    <td>
                        <?= $row['s_time'] ?>
                    </td>
                 </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </div>

        <div class="row mt-3">
            <div class="col-md-5"></div>
            <div class="col-md-4 float-right">
                <ul class="pagination">
                    <li style="padding: 10px; font-size: 19px; font-weight: bolder; " ><a href="?page_no=1<?= (isset($_GET['reply']) ? "&reply=".$_GET['reply'] : '') ?><?= (isset($_GET['from_date']) ? "&from_date=".$_GET['from_date'] : '')?><?= (isset($_GET['to_date']) ? "&to_date=".$_GET['to_date'] : '')?>">First</a></li>
                    <li style="padding: 10px; font-size: 19px; font-weight: bolder; " class="<?php if($page_no <= 1){ echo 'disabled'; } ?>">
                        <a href="<?php if($page_no <= 1){ echo '#'; } else { echo "?page_no=".($page_no - 1); } ?><?= (isset($_GET['reply']) ? "&reply=".$_GET['reply'] : '') ?><?= (isset($_GET['from_date']) ? "&from_date=".$_GET['from_date'] : '')?><?= (isset($_GET['to_date']) ? "&to_date=".$_GET['to_date'] : '')?>">Prev</a>
                    </li>
                    <li style="padding: 10px; font-size: 19px; font-weight: bolder; " class="<?php if($page_no >= $total_pages){ echo 'disabled'; } ?>">
                        <a href="<?php if($page_no >= $total_pages){ echo '#'; } else { echo "?page_no=".($page_no + 1); } ?><?= (isset($_GET['reply']) ? "&reply=".$_GET['reply'] : '') ?><?= (isset($_GET['from_date']) ? "&from_date=".$_GET['from_date'] : '')?><?= (isset($_GET['to_date']) ? "&to_date=".$_GET['to_date'] : '')?>">Next</a>
                    </li>
                    <li style="padding: 10px; font-size: 19px; font-weight: bolder; " ><a href="?page_no=<?php echo $total_pages; ?><?= (isset($_GET['reply']) ? "&reply=".$_GET['reply'] : '') ?><?= (isset($_GET['from_date']) ? "&from_date=".$_GET['from_date'] : '')?><?= (isset($_GET['to_date']) ? "&to_date=".$_GET['to_date'] : '')?>">Last</a></li>
                </ul>
            </div>
            <div class="col-md-3">Page <?= ($page_no ?? $page_no) ?>/<?= $total_pages?></div>
        </div>

    </div>
    <div class="card-footer small text-muted"></div>
</div>

<!--<p class="small text-center text-muted my-5">
    <em>More table examples coming soon...</em>
</p>
-->


<?php require_once 'footer.php' ?>
