<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

ini_set('memory_limit','-1');


require_once 'app/general/functions.php';
require_once 'app/acm-controller.php';
/*
if (isset($_POST['submit'])) {
    
    $data = array_filter(array(
        'msisdn' => $_POST['msisdn'],
        'status' => $_POST['status'],
        'pack_name' => $_POST['pack_name'],
        'from_date' => $_POST['from_date'],
        'to_date' => $_POST['to_date']
        ));
    
    header('Location: data-purchase.php?'.http_build_query($data));
}*/

$dataPurchaseHistory = dataPurchaseHistory();
/*
$dataPackages = dataPackages();
$total = totalDataPurchased($dataPurchaseHistory);*/

?>


<?php setPageTitle('Dashboard'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>

<style>
    #submit {
        margin-top: 30px;
    }
    label {
        font-weight: bold;
    }
    
</style>

<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Data Purchase
        
        <a href="data-purchase.php" class="btn btn-sm btn-secondary float-right" type="button">RESET</a>
        
        </div>
        
    <div class="card-body">

        <div class="table-responsive">
            <table class="table table-bordered" id="dataPurchase" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>USER</th>
                    <th>MSISDN</th>
                    <th>PLAN</th>
                    <th>PRICE</th>
                    <th>STATUS</th>
                    <th>DATE</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($dataPurchaseHistory as $row): ?>
                <tr>
                    <td><?= safeOutput($row['id']) ?></td>
                    <td><?= safeOutput($row['user']) ?></td>
                    <td><?= safeOutput($row['msisdn']) ?></td>
                    <td><?= safeOutput($row['plan']) ?></td>
                    <td><?= safeOutput($row['price']) ?></td>
                    <td><?= safeOutput($row['status']) ?></td>
                    <td><?= safeOutput($row['date']) ?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer small text-muted"></div>
</div>

<!--<p class="small text-center text-muted my-5">
    <em>More table examples coming soon...</em>
</p>
-->


<?php require_once 'footer.php' ?>

