<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/dash-controller.php';

unauthorizedUserRedirect('login.php');

date_default_timezone_set('Asia/Dhaka');

if(!empty($_GET['start_date']) && !empty($_GET['end_date'])) {
    
   $start_date = date('Y-m-d', strtotime($_GET['start_date']));
   $end_date = date('Y-m-d', strtotime($_GET['end_date']));
   
} else {
    $start_date = '2018-02-10';
    $end_date = date('Y-m-d');
}


$totalFbUsers = totalFbUsers($start_date, $end_date);
$todaysFbUsers = todaysFbUsers();

$totalFbMessages = totalFbMessages($start_date, $end_date);
$todaysFbMessages = todaysFbMessages();

$totalFbMaleUsers = totalFbMaleUsers($start_date, $end_date);
$totalFbFemaleUsers = totalFbFemaleUsers($start_date, $end_date);

$totalAnsweredByAgent = totalAnsweredByAgent($start_date, $end_date);
$totalAnsweredByBot = totalAnsweredByBot($start_date, $end_date);

$dailyFbMessages = dailyFbMessages($start_date, $end_date);
$weeklyFbMessages = weeklyFbMessages($start_date, $end_date);
$monthlyFbMessages = monthlyFbMessages($start_date, $end_date);

$totalAccManagementUsers = totalAccManagementUsers();
$totalDataPurchaseUsers = totalDataPurchaseUsers($start_date, $end_date);
$totalDataPurchaseRevenue = totalDataPurchaseRevenue($start_date, $end_date);
$totalDataPurchaseSuccess = totalDataPurchaseSuccess($start_date, $end_date);
$totalDataPurchaseFailed = totalDataPurchaseFailed($start_date, $end_date);

$totalDataPurchaseFailedUsers = totalDataPurchaseFailedUsers($start_date, $end_date);
$totalDataPurchaseSuccessUsers = totalDataPurchaseSuccessUsers($start_date, $end_date);
$topDataPurchasePackagesCount = topDataPurchasePackagesCount($start_date, $end_date);

?>


<?php setPageTitle('Airtel Dashboard'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>

<style>

    .count {
        font-weight: bold;
        font-size:  24px;
        color: ghostwhite;
    }
</style>

<div class="row">
    <div class="col-lg-8 mb-3">
        
       <form action="index.php" method="get" enctype="multipart/form-data">
          <div class="form-row">
            <div class="col">
              <input type="date" name="start_date" class="form-control" value="<?= $start_date ?>">
            </div>
            <div class="col">
              <input type="date" name="end_date"  class="form-control"  value="<?= $end_date ?>">
            </div>
            <div class="col">
              <button type="submit" class="btn" style="background:#f81416;color:white;font-weight:bold">Filter</button>
            </div>
            <div class="col">
              <a href="index.php" class="btn" style="background:#f81416;color:white;font-weight:bold;width:90px">View All</a>
            </div>
          </div>
        </form>
    
    </div>
</div>

<div class="row">
    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white robi_card_bg o-hidden h-80">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-user-friends"></i>
                </div>
                <div class="mr-5 count"><?= (int)$totalFbUsers ?></div>
                <div class="mr-5">Total Users</div>
            </div>
            <!--<div class="card-footer text-white clearfix small z-1">
                <span class="float-left">Total Facebook Users</span>
            </div>-->
        </div>
    </div>


    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white robi_card_bg o-hidden h-80">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-user-friends"></i>
                </div>
                <div class="mr-5 count"><?= (int)$todaysFbUsers ?></div>
                <div class="mr-5">Today's Users</div>
            </div>
           <!-- <div class="card-footer text-white clearfix small z-1">
                <span class="float-left">Today's Facebook Users</span>
            </div>-->
        </div>
    </div>


    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white robi_card_bg o-hidden h-80">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-comments"></i>
                </div>
                <div class="mr-5 count"><?= (int)$totalFbMessages ?></div>
                <div class="mr-5">Total Messages</div>
            </div>
            <!--<div class="card-footer text-white clearfix small z-1">
                <span class="float-left">Total Facebook Messages</span>
            </div>-->
        </div>
    </div>

    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white robi_card_bg o-hidden h-80">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-comments"></i>
                </div>
                <div class="mr-5 count"><?= (int)$todaysFbMessages ?></div>
                <div class="mr-5">Today's Messages</div>
            </div>
            <!--<div class="card-footer text-white clearfix small z-1">
                <span class="float-left">Today's Facebook Messages</span>
            </div>-->
        </div>
    </div>

    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white robi_card_bg o-hidden h-80">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-male"></i>
                </div>
                <div class="mr-5 count"><?= (int)$totalFbMaleUsers ?></div>
                <div class="mr-5">Male Users</div>
            </div>
            <!--<div class="card-footer text-white clearfix small z-1">
                <span class="float-left">Facebook Male Users</span>
            </div>-->
        </div>
    </div>

    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white robi_card_bg o-hidden h-80">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-female"></i>
                </div>
                <div class="mr-5 count"><?= (int)$totalFbFemaleUsers ?></div>
                <div class="mr-5"> Female Users</div>
            </div>
            <!--<div class="card-footer text-white clearfix small z-1">
                <span class="float-left">Facebook Female Users</span>
            </div>-->
        </div>
    </div>

    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white robi_card_bg o-hidden h-80">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-user-friends"></i>
                </div>
                <div class="mr-5 count"><?= (int)$totalAccManagementUsers ?></div>
                <div class="mr-5">Acc. Manag. Users</div>
            </div>
            <!--<div class="card-footer text-white clearfix small z-1">
                <span class="float-left">Total Account Management Users</span>
            </div>-->
        </div>
    </div>

    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white robi_card_bg o-hidden h-80">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-user-friends"></i>
                </div>
                <div class="mr-5 count"><?= (int)$totalDataPurchaseUsers ?></div>
                <div class="mr-5">Data Purchase Users</div>
            </div>
            <!--<div class="card-footer text-white clearfix small z-1">
                <span class="float-left">Data Purchase Users</span>
            </div>-->
        </div>
    </div>


    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white robi_card_bg o-hidden h-80">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-servicestack"></i>
                </div>
                <div class="mr-5 count"><?= (double)$totalDataPurchaseRevenue ?> TK.</div>
                <div class="mr-5">Data Pack Purchase</div>
            </div>
           <!-- <div class="card-footer text-white clearfix small z-1">
                <span class="float-left">Total Data Purchase Revenue BDT.</span>
            </div>-->
        </div>
    </div>

</div>




<div class="row justify-content-center mt-2 mb-3">
    <div class="col-lg-6 text-center">
        <h4>Chat Bot Analytics</h4>
    </div>
</div>

<div class="row" style="background: rgba(237,97,40,0.22);">
    <div class="col-6 mb-3 mt-3">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist" style="background: #F37020">
                <a class="nav-item nav-link analytics_line_chart_btn active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Daily</a>
                <a class="nav-item nav-link analytics_line_chart_btn" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="true">Weekly</a>
                <a class="nav-item nav-link analytics_line_chart_btn" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="true">Monthly</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div id="daily_message"></div>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div id="weekly_message"></div>
            </div>
            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                <div id="monthly_message"></div>
            </div>
        </div>
    </div>

    <div class="col-6 mb-3 mt-3" id="data_purchase" style="height: 350px;"></div>
</div>

<div class="row" style="background: rgba(237,97,40,0.22);">
    <div class="col-6 mb-3 mt-3" id="data_purchase_pack" style="height: 350px;"></div>
    <div class="col-6 mb-3 mt-3" id="answered_ptg" style="height: 350px;"></div>
</div>





<?php require_once 'footer.php' ?>


<script>
    /*
           Daily , Weekly, Monthly Message Count
    */
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(daily_message_drawChart);

    function daily_message_drawChart() {

        var daily_message_data = new google.visualization.DataTable();
        daily_message_data.addColumn('date', 'Day');
        daily_message_data.addColumn('number', 'Msg');

        daily_message_data.addRows([

            <?php

            if(count($dailyFbMessages) > 0) {
                foreach ($dailyFbMessages as $row) {
                    $week_date = date("Y, m-1, d", strtotime($row["daily_date"]));
                    echo '[new Date('.$week_date.'),'.$row["messages"].'],';
                }
            }else {
                echo '[new Date(1969, 13, 31), 0]';
            }

            ?>

        ]);


        var daily_message_options = {
            title: 'Daily Messages',
            height: 310,
            width: 540,
            hAxis: {
                format:'MMM d, y',
                gridlines: {count: 5},
            },
            vAxis: {
                gridlines: {color: 'none'},
                minValue: 0
            }
        };

        var daily_message_chart = new google.visualization.LineChart(document.getElementById('daily_message'));

        daily_message_chart.draw(daily_message_data, daily_message_options);
    }

    /*
    Weekly Message
     */
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(weekly_message_drawChart);

    function weekly_message_drawChart() {

        var weekly_message_data = new google.visualization.DataTable();
        weekly_message_data.addColumn('date', 'Week');
        weekly_message_data.addColumn('number', 'Msg');

        weekly_message_data.addRows([

            <?php

            if(count($weeklyFbMessages) > 0) {
                foreach ($weeklyFbMessages as $row) {
                    $week_date = date("Y, m-1, d", strtotime($row["weekly_date"]));
                    echo '[new Date('.$week_date.'),'.$row["messages"].'],';
                }
            }else {
                echo '[new Date(1969, 13, 31), 0]';
            }

            ?>

        ]);


        var weekly_message_options = {
            title: 'Weekly Messages',
            height: 310,
            width: 540,
            hAxis: {
                format:'d, MMM',
                gridlines: {count: 7},
            },
            vAxis: {
                gridlines: {color: 'none'},
                minValue: 0
            }
        };

        var weekly_message_chart = new google.visualization.LineChart(document.getElementById('weekly_message'));

        weekly_message_chart.draw(weekly_message_data, weekly_message_options);
    }


    /*
          Monthly Message
   */
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(monthly_message_drawChart);

    function monthly_message_drawChart() {

        var monthly_message_data = new google.visualization.DataTable();
        monthly_message_data.addColumn('date', 'Month');
        monthly_message_data.addColumn('number', 'Msg');

        monthly_message_data.addRows([

            <?php

            if(count($monthlyFbMessages) > 0) {
                foreach ($monthlyFbMessages as $monthly_data) {
                    $week_date = date("Y, m-1, d", strtotime($monthly_data["monthly_date"]));
                    echo '[new Date('.$week_date.'),'.$monthly_data["messages"].'],';
                }
            }else {
                echo '[new Date(1969, 13, 31), 0]';
            }

            ?>

        ]);


        var monthly_message_options = {
            title: 'Month Messages',
            height: 310,
            width: 540,
            hAxis: {
                format:'d,MMM,y',
                gridlines: {count: 5},
            },
            vAxis: {
                gridlines: {color: 'none'},
                minValue: 0
            }
        };

        var monthly_message_chart = new google.visualization.LineChart(document.getElementById('monthly_message'));

        monthly_message_chart.draw(monthly_message_data, monthly_message_options);
    }

    /*  Daily MOnthly Weekly Ends Here */

    /*
      Data Purchase Success and Failed
   */
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(data_purchase_drawChart);
    function data_purchase_drawChart() {
        var data_purchase_data = google.visualization.arrayToDataTable([
            ['Task', 'Data Purchase'],
            ['Success',     <?=
                !empty($totalDataPurchaseSuccess) ? $totalDataPurchaseSuccess : 1
                ?>],
            ['Failed',      <?=
                !empty($totalDataPurchaseFailed) ? $totalDataPurchaseFailed : 1
                ?>],
        ]);

        var data_purchase_options = {
            title: 'Data Purchase',
            pieHole: 0.4,
        };

        var data_purchase_chart = new google.visualization.PieChart(document.getElementById('data_purchase'));
        data_purchase_chart.draw(data_purchase_data, data_purchase_options);
    }

    /*
     * Packages and Count
     */

    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(data_purchase_pack_drawBasic);

    function data_purchase_pack_drawBasic() {

        let data_purchase_pack_data = google.visualization.arrayToDataTable([
            ['Package', 'Package',],
            <?php
            
                if(count($topDataPurchasePackagesCount)) {
                       foreach($topDataPurchasePackagesCount as $row){
                        $pack_name = $row["pack_name"];
                        $count = $row["count"];
    
                        echo '["'.$pack_name.'",'.$count.'],';
                    } 
                }else {
                    echo '["Package",0],';
                }
                

                ?>
        ]);

        let data_purchase_pack_options = {
            title: 'Top 5 Data Purchase Packages',
            chartArea: {width: '50%'},
            hAxis: {
                title: 'Package Purchased',
                minValue: 0
            },
            vAxis: {
                title: 'Data Package'
            }
        };

        let data_purchase_pack_chart = new google.visualization.BarChart(document.getElementById('data_purchase_pack'));

        data_purchase_pack_chart.draw(data_purchase_pack_data, data_purchase_pack_options);
    }

    /*
       Answer By Agent and Bot
    */
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(answered_ptg_drawChart);
    function answered_ptg_drawChart() {
        var answered_ptg_data = google.visualization.arrayToDataTable([
            ['Task', 'Agent/BOT Percentage'],
            ['Agent',     <?=
                empty($totalAnsweredByAgent) ? 1 : $totalAnsweredByAgent

                ?>],
            ['BOT',      <?=
                 empty($totalAnsweredByBot) ? 1 : $totalAnsweredByBot
    
                ?>],
        ]);

        var answered_ptg_options = {
            title: 'Answered by Agent/BOT',
            pieHole: 0.4,
        };

        var answered_ptg_chart = new google.visualization.PieChart(document.getElementById('answered_ptg'));
        answered_ptg_chart.draw(answered_ptg_data, answered_ptg_options);
    }



</script>

