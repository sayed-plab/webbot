-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2018 at 12:38 PM
-- Server version: 5.6.30-1
-- PHP Version: 7.2.4-1+b1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `airtel`
--

-- --------------------------------------------------------

--
-- Table structure for table `coverage_area_bn`
--

CREATE TABLE `coverage_area_bn` (
  `id` int(11) NOT NULL,
  `district` varchar(255) DEFAULT NULL,
  `thana` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coverage_area_bn`
--

INSERT INTO `coverage_area_bn` (`id`, `district`, `thana`) VALUES
(1, 'বাগেরহাট', 'মোল্লাহাট'),
(2, 'বাগেরহাট', 'বাগেরহাট সদর'),
(3, 'বাগেরহাট', 'চিতলমারী'),
(4, 'বাগেরহাট', 'ফকিরহাট'),
(5, 'বাগেরহাট', 'কচুয়া'),
(6, 'বাগেরহাট', 'মোল্লাহাট'),
(7, 'বাগেরহাট', 'মংলা'),
(8, 'বাগেরহাট', 'মোরেলগঞ্জ'),
(9, 'বাগেরহাট', 'রামপাল'),
(10, 'বাগেরহাট', 'শরণখোলা'),
(11, 'বান্দরবান', 'আলীকদম'),
(12, 'বান্দরবান', 'বান্দরবান সদর'),
(13, 'বান্দরবান', 'নাইখাংছড়ি'),
(14, 'বান্দরবান', 'রোয়াংছড়ি'),
(15, 'বান্দরবান', 'রুমা'),
(16, 'বান্দরবান', 'থানচি'),
(17, 'বরগুনা', 'আমতলী'),
(18, 'বরগুনা', 'বামনা'),
(19, 'বরগুনা', 'বরগুনা সদর'),
(20, 'বরগুনা', 'বেতাগী'),
(21, 'বরগুনা', 'পাথরঘাটা'),
(22, 'বরিশাল', 'আগৈলঝাড়া'),
(23, 'বরিশাল', 'বাবুগঞ্জ'),
(24, 'বরিশাল', 'বাকেরগঞ্জ'),
(25, 'বরিশাল', 'বানারীপাড়া'),
(26, 'বরিশাল', 'বরিশাল সদর (কোতোয়ালী)'),
(27, 'বরিশাল', 'গৌরনদী'),
(28, 'বরিশাল', 'হিজলা'),
(29, 'বরিশাল', 'মেহেদিগঞ্জ'),
(30, 'বরিশাল', 'মুলাদী'),
(31, 'বরিশাল', 'ওয়াজিরপুর'),
(32, 'ভোলা', 'ভোলা সদর'),
(33, 'ভোলা', 'বোরহান উদ্দিন'),
(34, 'ভোলা', 'চরফ্যাশন'),
(35, 'ভোলা', 'দৌলত খান'),
(36, 'ভোলা', 'লালমোহন'),
(37, 'ভোলা', 'মনপুরা'),
(38, 'ভোলা', 'তজুমদ্দিন'),
(39, 'বগুড়া', 'আদমদিঘী'),
(40, 'বগুড়া', 'বগুড়া সদর'),
(41, 'বগুড়া', 'ধুনট'),
(42, 'বগুড়া', 'ধুপচাঁচিয়া'),
(43, 'বগুড়া', 'গাবতলী'),
(44, 'বগুড়া', 'কাহালু'),
(45, 'বগুড়া', 'নন্দিগ্রাম'),
(46, 'বগুড়া', 'সারিয়াকান্দি'),
(47, 'বগুড়া', 'শাহজাহানপুর'),
(48, 'বগুড়া', 'শেরপুর'),
(49, 'বগুড়া', 'শিবগঞ্জ'),
(50, 'বগুড়া', 'সোনাতলা'),
(51, 'ব্রাহ্মণবাড়িয়া', 'আখাউড়া'),
(52, 'ব্রাহ্মণবাড়িয়া', 'আশুগঞ্জ'),
(53, 'ব্রাহ্মণবাড়িয়া', 'বাঞ্ছারামপুর'),
(54, 'ব্রাহ্মণবাড়িয়া', 'বিজয়নগর'),
(55, 'ব্রাহ্মণবাড়িয়া', 'ব্রাহ্মণবাড়িয়া সদর'),
(56, 'ব্রাহ্মণবাড়িয়া', 'কসবা'),
(57, 'ব্রাহ্মণবাড়িয়া', 'নবীনগর'),
(58, 'ব্রাহ্মণবাড়িয়া', 'নাসিরনগর'),
(59, 'ব্রাহ্মণবাড়িয়া', 'সরাইল'),
(60, 'ব্রাহ্মণবাড়িয়া', 'ব্রাহ্মণবাড়িয়া সদর'),
(61, 'ব্রাহ্মণবাড়িয়া', 'নাসিরনগর'),
(62, 'চাঁদপুর', 'চাঁদপুর সদর'),
(63, 'চাঁদপুর', 'ফরিদ্গঞ্জ'),
(64, 'চাঁদপুর', 'হাইমচর'),
(65, 'চাঁদপুর', 'হাজীগঞ্জ'),
(66, 'চাঁদপুর', 'কচুয়া'),
(67, 'চাঁদপুর', 'মতলব দক্ষিণ'),
(68, 'চাঁদপুর', 'শাহরাস্তি'),
(69, 'চাঁদপুর', 'উত্তর মতলব'),
(70, 'চট্টগ্রাম', 'আনোয়ারা'),
(71, 'চট্টগ্রাম', 'বাকলিয়া'),
(72, 'চট্টগ্রাম', 'বাঁশখালি'),
(73, 'চট্টগ্রাম', 'বায়েজিদ বোস্তামী'),
(74, 'চট্টগ্রাম', 'বোয়ালখালী'),
(75, 'চট্টগ্রাম', 'চন্দনাইশ'),
(76, 'চট্টগ্রাম', 'চাঁদগাও'),
(77, 'চট্টগ্রাম', 'চট্টগ্রাম বন্দর'),
(78, 'চট্টগ্রাম', 'ডাবল মুরিং'),
(79, 'চট্টগ্রাম', 'ফটিকছড়ি'),
(80, 'চট্টগ্রাম', 'হালিশহর'),
(81, 'চট্টগ্রাম', 'হাটহাজারী'),
(82, 'চট্টগ্রাম', 'খুলশী'),
(83, 'চট্টগ্রাম', 'কোতোয়ালী'),
(84, 'চট্টগ্রাম', 'লোহাগড়া'),
(85, 'চট্টগ্রাম', 'মিরসরাই'),
(86, 'চট্টগ্রাম', 'পাহাড়তলী'),
(87, 'চট্টগ্রাম', 'পাঁচলাইশ'),
(88, 'চট্টগ্রাম', 'পতেঙ্গা'),
(89, 'চট্টগ্রাম', 'পটিয়া'),
(90, 'চট্টগ্রাম', 'রাঙ্গুনিয়া'),
(91, 'চট্টগ্রাম', 'রাউজান'),
(92, 'চট্টগ্রাম', 'সন্দ্বীপ'),
(93, 'চট্টগ্রাম', 'সাতকানিয়া'),
(94, 'চট্টগ্রাম', 'সীতাকুণ্ড'),
(95, 'চুয়াডাঙ্গা', 'আলমডাঙ্গা'),
(96, 'চুয়াডাঙ্গা', 'চুয়াডাঙ্গা সদর'),
(97, 'চুয়াডাঙ্গা', 'দামুরহুদা'),
(98, 'চুয়াডাঙ্গা', 'জীবননগর'),
(99, 'কুমিল্লা', 'বড়ুরা'),
(100, 'কুমিল্লা', 'ব্রাহ্মনপাড়া'),
(101, 'কুমিল্লা', 'বুড়িচং'),
(102, 'কুমিল্লা', 'চান্দিনা'),
(103, 'কুমিল্লা', 'চৌদ্দগ্রাম'),
(104, 'কুমিল্লা', 'কুমিল্লা আদর্শ সদর'),
(105, 'কুমিল্লা', 'কুমিল্লা সদর দক্ষিণ'),
(106, 'কুমিল্লা', 'দাউদকান্দি'),
(107, 'কুমিল্লা', 'দেবিদ্বার'),
(108, 'কুমিল্লা', 'হোমনা'),
(109, 'কুমিল্লা', 'লাকসাম'),
(110, 'কুমিল্লা', 'মনোহরগঞ্জ'),
(111, 'কুমিল্লা', 'মেঘনা'),
(112, 'কুমিল্লা', 'মুরাদনগর'),
(113, 'কুমিল্লা', 'নাঙ্গলকোট'),
(114, 'কুমিল্লা', 'তিতাস'),
(115, 'কক্সবাজার', 'চকরিয়া'),
(116, 'কক্সবাজার', 'কক্সবাজার সদর'),
(117, 'কক্সবাজার', 'কুতুবদিয়া'),
(118, 'কক্সবাজার', 'মহেশখালী'),
(119, 'কক্সবাজার', 'পেকুয়া'),
(120, 'কক্সবাজার', 'রামু'),
(121, 'কক্সবাজার', 'টেকনাফ'),
(122, 'কক্সবাজার', 'উখিয়া'),
(123, 'ঢাকা', 'আদাবর'),
(124, 'ঢাকা', 'বাড্ডা'),
(125, 'ঢাকা', 'বংশাল'),
(126, 'ঢাকা', 'বিমান বন্দর থানা'),
(127, 'ঢাকা', 'ক্যান্টনমেন্ট'),
(128, 'ঢাকা', 'চকবাজার'),
(129, 'ঢাকা', 'দক্ষিণখান'),
(130, 'ঢাকা', 'দারুস সালাম'),
(131, 'ঢাকা', 'ডেমরা'),
(132, 'ঢাকা', 'ধামরাই'),
(133, 'ঢাকা', 'ধানমন্ডি'),
(134, 'ঢাকা', 'দোহার'),
(135, 'ঢাকা', 'গেন্ডারিয়া'),
(136, 'ঢাকা', 'গুলশান'),
(137, 'ঢাকা', 'হাজারীবাগ'),
(138, 'ঢাকা', 'যাত্রাবাড়ী'),
(139, 'ঢাকা', 'কদমতলী'),
(140, 'ঢাকা', 'কাফরুল'),
(141, 'ঢাকা', 'কলাবাগান'),
(142, 'ঢাকা', 'কামরাঙ্গীরচর'),
(143, 'ঢাকা', 'কেরানীগঞ্জ'),
(144, 'ঢাকা', 'খিলগাঁও'),
(145, 'ঢাকা', 'খিলক্ষেত'),
(146, 'ঢাকা', 'কোতোয়ালী'),
(147, 'ঢাকা', 'লালবাগ'),
(148, 'ঢাকা', 'মিরপুর'),
(149, 'ঢাকা', 'মোহাম্মদপুর'),
(150, 'ঢাকা', 'মতিঝিল'),
(151, 'ঢাকা', 'নবাবগঞ্জ'),
(152, 'ঢাকা', 'নিউ মার্কেট'),
(153, 'ঢাকা', 'পল্লবী'),
(154, 'ঢাকা', 'পল্টন'),
(155, 'ঢাকা', 'রমনা'),
(156, 'ঢাকা', 'রামপুরা'),
(157, 'ঢাকা', 'সবুজবাগ'),
(158, 'ঢাকা', 'সাভার'),
(159, 'ঢাকা', 'শাহ আলী'),
(160, 'ঢাকা', 'শাহবাগ'),
(161, 'ঢাকা', 'শের-এ-বাংলা নগর'),
(162, 'ঢাকা', 'শ্যামপুর'),
(163, 'ঢাকা', 'সুত্রাপুর'),
(164, 'ঢাকা', 'তেজগাঁও'),
(165, 'ঢাকা', 'তেজগাঁও শিল্প অঞ্চল'),
(166, 'ঢাকা', 'তুরাগ'),
(167, 'ঢাকা', 'উত্তরখান'),
(168, 'ঢাকা', 'উত্তরা'),
(169, 'ঢাকা', 'নালিতাবাড়ী'),
(170, 'দিনাজপুর', 'বিরল'),
(171, 'দিনাজপুর', 'বিক্রমপুর'),
(172, 'দিনাজপুর', 'বীরগঞ্জ'),
(173, 'দিনাজপুর', 'বোচাগঞ্জ'),
(174, 'দিনাজপুর', 'চিরিরবন্দর'),
(175, 'দিনাজপুর', 'দিনাজপুর সদর'),
(176, 'দিনাজপুর', 'ফুলবাড়ি'),
(177, 'দিনাজপুর', 'ঘোড়াঘাট'),
(178, 'দিনাজপুর', 'হাকিমপুর'),
(179, 'দিনাজপুর', 'কাহারোল'),
(180, 'দিনাজপুর', 'খানসামা'),
(181, 'দিনাজপুর', 'নবাবগঞ্জ'),
(182, 'দিনাজপুর', 'পার্বতীপুর'),
(183, 'ফরিদপুর', 'আলফাডাঙ্গা'),
(184, 'ফরিদপুর', 'ভাঙ্গা'),
(185, 'ফরিদপুর', 'বোয়ালমারী'),
(186, 'ফরিদপুর', 'চর ভদ্রাসন'),
(187, 'ফরিদপুর', 'ফরিদপুর সদর'),
(188, 'ফরিদপুর', 'মধুখালী'),
(189, 'ফরিদপুর', 'নগরকান্দা'),
(190, 'ফরিদপুর', 'সদরপুর'),
(191, 'ফরিদপুর', 'সালথা'),
(192, 'ফেনী', 'ছাগলনাইয়া'),
(193, 'ফেনী', 'দাগনভূঁইয়া'),
(194, 'ফেনী', 'ফেনী সদর'),
(195, 'ফেনী', 'ফুলগাজী'),
(196, 'ফেনী', 'পরশুরাম'),
(197, 'ফেনী', 'সোনাগাজী'),
(198, 'গাইবান্ধা', 'ফুলছড়ি'),
(199, 'গাইবান্ধা', 'গাইবান্ধা সদর'),
(200, 'গাইবান্ধা', 'গোবিন্দগঞ্জ'),
(201, 'গাইবান্ধা', 'পলাশবাড়ী'),
(202, 'গাইবান্ধা', 'সাদুল্লাপুর'),
(203, 'গাইবান্ধা', 'সাঘাটা'),
(204, 'গাইবান্ধা', 'সুন্দরগঞ্জ'),
(205, 'গাজীপুর', 'গাজীপুর সদর'),
(206, 'গাজীপুর', 'কালিয়াকৈর'),
(207, 'গাজীপুর', 'কালীগঞ্জ'),
(208, 'গাজীপুর', 'কাপাসিয়া'),
(209, 'গাজীপুর', 'শ্রীপুর'),
(210, 'গোপালগঞ্জ', 'গোপালগঞ্জ সদর'),
(211, 'গোপালগঞ্জ', 'কাশিয়ানী'),
(212, 'গোপালগঞ্জ', 'কোটালীপাড়া'),
(213, 'গোপালগঞ্জ', 'মাকসুদপুর'),
(214, 'গোপালগঞ্জ', 'টুংগীপাড়া'),
(215, 'গোপালগঞ্জ', 'গোপালগঞ্জ সদর'),
(216, 'গোপালগঞ্জ', 'মাকসুদপুর'),
(217, 'হবিগঞ্জ', 'আজমিরিগঞ্জ'),
(218, 'হবিগঞ্জ', 'বাহুবল'),
(219, 'হবিগঞ্জ', 'বানিয়াচং'),
(220, 'হবিগঞ্জ', 'চুনারুঘাট'),
(221, 'হবিগঞ্জ', 'হবিগঞ্জ সদর'),
(222, 'হবিগঞ্জ', 'লাখাই'),
(223, 'হবিগঞ্জ', 'মাধবপুর'),
(224, 'হবিগঞ্জ', 'নবীগঞ্জ'),
(225, 'জামালপুর', 'বকশিগঞ্জ'),
(226, 'জামালপুর', 'দেওয়ানগঞ্জ'),
(227, 'জামালপুর', 'ইসলামপুর'),
(228, 'জামালপুর', 'জামালপুর সদর'),
(229, 'জামালপুর', 'মাদারগঞ্জ'),
(230, 'জামালপুর', 'মেলান্দহ'),
(231, 'জামালপুর', 'সরিষাবাড়ী'),
(232, 'জামালপুর', 'ইসলামপুর'),
(233, 'যশোর', 'অভয়নগর'),
(234, 'যশোর', 'বাঘেরপাড়া'),
(235, 'যশোর', 'চৌগাছা'),
(236, 'যশোর', 'যশোর সদর'),
(237, 'যশোর', 'ঝিকরগাছা'),
(238, 'যশোর', 'কেশবপুর'),
(239, 'যশোর', 'মনিরামপুর'),
(240, 'যশোর', 'শার্শা'),
(241, 'ঝালকাঠি', 'ঝালকাঠি সদর'),
(242, 'ঝালকাঠি', 'কাঁঠালিয়া'),
(243, 'ঝালকাঠি', 'নলছিটি'),
(244, 'ঝালকাঠি', 'রাজাপুর'),
(245, 'ঝিনাইদহ', 'হরিণাকুন্ড'),
(246, 'ঝিনাইদহ', 'ঝিনাইদহ সদর'),
(247, 'ঝিনাইদহ', 'কালীগঞ্জ'),
(248, 'ঝিনাইদহ', 'কোটচাঁদপুর'),
(249, 'ঝিনাইদহ', 'মহেশপুর'),
(250, 'ঝিনাইদহ', 'শৈলকূপা'),
(251, 'জয়পুরহাট', 'আক্কেলপুর'),
(252, 'জয়পুরহাট', 'জয়পুরহাট সদর'),
(253, 'জয়পুরহাট', 'কালাই'),
(254, 'জয়পুরহাট', 'ক্ষেতলাল'),
(255, 'জয়পুরহাট', 'পঞ্চবিবি'),
(256, 'খাগড়াছড়ি', 'দিঘীনালা'),
(257, 'খাগড়াছড়ি', 'খাগড়াছড়ি সদর'),
(258, 'খাগড়াছড়ি', 'লক্ষীছড়া'),
(259, 'খাগড়াছড়ি', 'মহালছড়ি'),
(260, 'খাগড়াছড়ি', 'মানিকছড়ি'),
(261, 'খাগড়াছড়ি', 'মাটিরাঙ্গা'),
(262, 'খাগড়াছড়ি', 'রামগড়'),
(263, 'খুলনা', 'বটিয়াঘাটা'),
(264, 'খুলনা', 'দাকোপ'),
(265, 'খুলনা', 'দৌলতপুর'),
(266, 'খুলনা', 'দিঘলিয়া'),
(267, 'খুলনা', 'ডুমুরিয়া'),
(268, 'খুলনা', 'খালিশপুর'),
(269, 'খুলনা', 'খান জাহান আলী'),
(270, 'খুলনা', 'খুলনা সদর'),
(271, 'খুলনা', 'কয়রা'),
(272, 'খুলনা', 'পাইকগাছা'),
(273, 'খুলনা', 'ফুলতলা'),
(274, 'খুলনা', 'রূপসা'),
(275, 'খুলনা', 'সোনাডাঙ্গা'),
(276, 'খুলনা', 'তেরোখাদা'),
(277, 'খুলনা', 'বটিয়াঘাটা'),
(278, 'কিশোরগঞ্জ', 'অষ্টগ্রাম'),
(279, 'কিশোরগঞ্জ', 'বাজিতপুর'),
(280, 'কিশোরগঞ্জ', 'ভৈরব'),
(281, 'কিশোরগঞ্জ', 'হোসাইনপুর'),
(282, 'কিশোরগঞ্জ', 'ইটনা'),
(283, 'কিশোরগঞ্জ', 'করিমগঞ্জ'),
(284, 'কিশোরগঞ্জ', 'কটিয়াদী'),
(285, 'কিশোরগঞ্জ', 'কিশোরগঞ্জ সদর'),
(286, 'কিশোরগঞ্জ', 'কুলিয়ারচর'),
(287, 'কিশোরগঞ্জ', 'মিঠামইন'),
(288, 'কিশোরগঞ্জ', 'নিকলী'),
(289, 'কিশোরগঞ্জ', 'পাকুন্দিয়া'),
(290, 'কিশোরগঞ্জ', 'তারাইল'),
(291, 'কুড়িগ্রাম', 'ভুরুঙ্গামারী'),
(292, 'কুড়িগ্রাম', 'চর রাজিবপুর'),
(293, 'কুড়িগ্রাম', 'চিলমারী'),
(294, 'কুড়িগ্রাম', 'কুড়িগ্রাম সদর'),
(295, 'কুড়িগ্রাম', 'নাগেশ্বরী'),
(296, 'কুড়িগ্রাম', 'ফুলবাড়ি'),
(297, 'কুড়িগ্রাম', 'রাজারহাট'),
(298, 'কুড়িগ্রাম', 'রৌমারী'),
(299, 'কুড়িগ্রাম', 'উলিপুর'),
(300, 'কুষ্টিয়া', 'ভেড়ামারা'),
(301, 'কুষ্টিয়া', 'দৌলতপুর'),
(302, 'কুষ্টিয়া', 'খোকসা'),
(303, 'কুষ্টিয়া', 'কুমারখালী'),
(304, 'কুষ্টিয়া', 'কুষ্টিয়া সদর'),
(305, 'কুষ্টিয়া', 'মিরপুর'),
(306, 'লালমনিরহাট', 'আদিতমারী'),
(307, 'লালমনিরহাট', 'হাতীবান্ধা'),
(308, 'লালমনিরহাট', 'কালিগঞ্জ'),
(309, 'লালমনিরহাট', 'লালমনিরহাট সদর'),
(310, 'লালমনিরহাট', 'পাটগ্রাম'),
(311, 'লক্ষ্মীপুর', 'কামালনগর'),
(312, 'লক্ষ্মীপুর', 'লক্ষীপুর সদর'),
(313, 'লক্ষ্মীপুর', 'রামগঞ্জ'),
(314, 'লক্ষ্মীপুর', 'রামগতি'),
(315, 'লক্ষ্মীপুর', 'রায়পুর'),
(316, 'মাদারীপুর', 'কালকিনি'),
(317, 'মাদারীপুর', 'মাদারীপুর সদর'),
(318, 'মাদারীপুর', 'রাজৈর'),
(319, 'মাদারীপুর', 'শিবচর'),
(320, 'মাগুরা', 'মাগুরা সদর'),
(321, 'মাগুরা', 'মোহাম্মদপুর'),
(322, 'মাগুরা', 'শালিখা'),
(323, 'মাগুরা', 'শ্রীপুর'),
(324, 'মানিকগঞ্জ', 'দৌলতপুর'),
(325, 'মানিকগঞ্জ', 'ঘিওর'),
(326, 'মানিকগঞ্জ', 'হরিরামপুর'),
(327, 'মানিকগঞ্জ', 'মানিকগঞ্জ সদর'),
(328, 'মানিকগঞ্জ', 'সাটুরিয়া'),
(329, 'মানিকগঞ্জ', 'শিবালয়'),
(330, 'মানিকগঞ্জ', 'শিঙ্গাইর'),
(331, 'মেহেরপুর', 'গাঙনী'),
(332, 'মেহেরপুর', 'মেহেরপুর সদর'),
(333, 'মেহেরপুর', 'মুজিবনগর'),
(334, 'মৌলভীবাজার', 'বড়লেখা'),
(335, 'মৌলভীবাজার', 'জুড়ী'),
(336, 'মৌলভীবাজার', 'কামালগঞ্জ'),
(337, 'মৌলভীবাজার', 'কুলাউড়া'),
(338, 'মৌলভীবাজার', 'মৌলভীবাজার সদর'),
(339, 'মৌলভীবাজার', 'রাজনগর'),
(340, 'মৌলভীবাজার', 'শ্রীমঙ্গল'),
(341, 'মুন্সিগঞ্জ', 'গজারিয়া'),
(342, 'মুন্সিগঞ্জ', 'লৌহজং'),
(343, 'মুন্সিগঞ্জ', 'মুন্সিগঞ্জ সদর'),
(344, 'মুন্সিগঞ্জ', 'সিরাজদিখান'),
(345, 'মুন্সিগঞ্জ', 'শ্রীনগর'),
(346, 'মুন্সিগঞ্জ', 'টঙ্গিবাড়ী'),
(347, 'ময়মনসিংহ', 'ভালুকা'),
(348, 'ময়মনসিংহ', 'ধোবাউড়া'),
(349, 'ময়মনসিংহ', 'ফুলবাড়িয়া'),
(350, 'ময়মনসিংহ', 'গফরগাঁও'),
(351, 'ময়মনসিংহ', 'গৌরীপুর'),
(352, 'ময়মনসিংহ', 'হালুয়াঘাট'),
(353, 'ময়মনসিংহ', 'ঈশ্বরগঞ্জ'),
(354, 'ময়মনসিংহ', 'মুক্তাগাছা'),
(355, 'ময়মনসিংহ', 'ময়মনসিংহ সদর'),
(356, 'ময়মনসিংহ', 'নন্দাইল'),
(357, 'ময়মনসিংহ', 'ফুলপুর'),
(358, 'ময়মনসিংহ', 'ত্রিশাল'),
(359, 'ময়মনসিংহ', 'ভালুকা'),
(360, 'ময়মনসিংহ', 'গৌরীপুর'),
(361, 'নওগাঁ', 'আত্রাই'),
(362, 'নওগাঁ', 'বদলগাছী'),
(363, 'নওগাঁ', 'ধামইরহাট'),
(364, 'নওগাঁ', 'মহাদেবপুর'),
(365, 'নওগাঁ', 'মান্দা'),
(366, 'নওগাঁ', 'নওগাঁ সদর'),
(367, 'নওগাঁ', 'নিয়ামতপুর'),
(368, 'নওগাঁ', 'পত্নীতলা'),
(369, 'নওগাঁ', 'পোরশা'),
(370, 'নওগাঁ', 'রানীনগর'),
(371, 'নওগাঁ', 'সাপাহার'),
(372, 'নড়াইল', 'কালিয়া'),
(373, 'নড়াইল', 'লোহাগড়া'),
(374, 'নড়াইল', 'নড়াইল সদর'),
(375, 'নারায়ণগঞ্জ', 'আড়াইহাজার'),
(376, 'নারায়ণগঞ্জ', 'বন্দর'),
(377, 'নারায়ণগঞ্জ', 'নারায়ণগঞ্জ সদর'),
(378, 'নারায়ণগঞ্জ', 'রূপগঞ্জ'),
(379, 'নারায়ণগঞ্জ', 'সোনারগাঁও'),
(380, 'নাটোর', 'বাগাতিপাড়া'),
(381, 'নাটোর', 'বড়াইগ্রাম'),
(382, 'নাটোর', 'গুরুদাসপুর'),
(383, 'নাটোর', 'লালপুর'),
(384, 'নাটোর', 'নাটোর সদর'),
(385, 'নাটোর', 'সিংড়া'),
(386, 'নবাবগঞ্জ', 'ভোলাহাট'),
(387, 'নবাবগঞ্জ', 'গোমস্তাপুর'),
(388, 'নবাবগঞ্জ', 'নাচোল'),
(389, 'নবাবগঞ্জ', 'নবাবগঞ্জ সদর'),
(390, 'নবাবগঞ্জ', 'শিবগঞ্জ'),
(391, 'নেত্রকোনা', 'আটপাড়া'),
(392, 'নেত্রকোনা', 'বারহাট্টা'),
(393, 'নেত্রকোনা', 'দূর্গাপুর'),
(394, 'নেত্রকোনা', 'কলমাকান্দা'),
(395, 'নেত্রকোনা', 'কেন্দুয়া'),
(396, 'নেত্রকোনা', 'খালিয়াজুড়ি'),
(397, 'নেত্রকোনা', 'মদন'),
(398, 'নেত্রকোনা', 'মোহনগঞ্জ'),
(399, 'নেত্রকোনা', 'নেত্রকোনা সদর'),
(400, 'নেত্রকোনা', 'পূর্বধলা'),
(401, 'নেত্রকোনা', 'মদন'),
(402, 'নীলফামারী', 'ডিমলা'),
(403, 'নীলফামারী', 'ডোমার'),
(404, 'নীলফামারী', 'জলঢাকা'),
(405, 'নীলফামারী', 'কিশোরগঞ্জ'),
(406, 'নীলফামারী', 'নীলফামারী সদর'),
(407, 'নীলফামারী', 'সৈয়দপুর'),
(408, 'নোয়াখালী', 'বেগমগঞ্জ'),
(409, 'নোয়াখালী', 'চাটখিল'),
(410, 'নোয়াখালী', 'কোম্পানীগঞ্জ'),
(411, 'নোয়াখালী', 'হাতিয়া'),
(412, 'নোয়াখালী', 'কবিরহাট'),
(413, 'নোয়াখালী', 'নোয়াখালী সদর'),
(414, 'নোয়াখালী', 'সেনবাগ'),
(415, 'নোয়াখালী', 'সোনাইমুড়ি'),
(416, 'নোয়াখালী', 'সুবর্ণচর'),
(417, 'নোয়াখালী', 'কোম্পানিগঞ্জ'),
(418, 'নরসিংদী', 'বেলাবো'),
(419, 'নরসিংদী', 'মনোহরদী'),
(420, 'নরসিংদী', 'নরসিংদী সদর'),
(421, 'নরসিংদী', 'পলাশ'),
(422, 'নরসিংদী', 'রায়পুরা'),
(423, 'নরসিংদী', 'শিবপুর'),
(424, 'পাবনা', 'আটঘরিয়া'),
(425, 'পাবনা', 'বেড়া'),
(426, 'পাবনা', 'ভাঙ্গুড়া'),
(427, 'পাবনা', 'চাটমোহর'),
(428, 'পাবনা', 'ফরিদপুর'),
(429, 'পাবনা', 'ঈশ্বরদী'),
(430, 'পাবনা', 'পাবনা সদর'),
(431, 'পাবনা', 'সাঁথিয়া'),
(432, 'পাবনা', 'সুজানগর'),
(433, 'পাবনা', 'সাঁথিয়া'),
(434, 'পঞ্চগড়', 'আটোয়ারী'),
(435, 'পঞ্চগড়', 'বোদা'),
(436, 'পঞ্চগড়', 'দেবীগঞ্জ'),
(437, 'পঞ্চগড়', 'পঞ্চগড় সদর'),
(438, 'পঞ্চগড়', 'তেঁতুলিয়া'),
(439, 'পটুয়াখালী', 'বাউফল'),
(440, 'পটুয়াখালী', 'দশমিনা'),
(441, 'পটুয়াখালী', 'ডুমকি'),
(442, 'পটুয়াখালী', 'গলাচিপা'),
(443, 'পটুয়াখালী', 'কালাপাড়া'),
(444, 'পটুয়াখালী', 'মির্জাগঞ্জ'),
(445, 'পটুয়াখালী', 'পটুয়াখালী সদর'),
(446, 'পিরোজপুর', 'ভান্ডারিয়া'),
(447, 'পিরোজপুর', 'কাউখালী'),
(448, 'পিরোজপুর', 'মাঠবাড়িয়া'),
(449, 'পিরোজপুর', 'নাজিরপুর'),
(450, 'পিরোজপুর', 'নেসারাবাদ(স্বরূপকাঠি)'),
(451, 'পিরোজপুর', 'পিরোজপুর সদর'),
(452, 'পিরোজপুর', 'জিয়ানগর'),
(453, 'রাজবাড়ী', 'বালিয়াকান্দি'),
(454, 'রাজবাড়ী', 'গোয়ালন্দা'),
(455, 'রাজবাড়ী', 'কালুখালী'),
(456, 'রাজবাড়ী', 'পাংশা'),
(457, 'রাজবাড়ী', 'রাজবাড়ী সদর'),
(458, 'রাজশাহী', 'বাঘা'),
(459, 'রাজশাহী', 'বাঘমারা'),
(460, 'রাজশাহী', 'বোয়ালিয়া'),
(461, 'রাজশাহী', 'চরঘাট'),
(462, 'রাজশাহী', 'দূর্গাপুর'),
(463, 'রাজশাহী', 'গোদাগাড়ী'),
(464, 'রাজশাহী', 'মতিহার'),
(465, 'রাজশাহী', 'মোহনপুর'),
(466, 'রাজশাহী', 'পবা'),
(467, 'রাজশাহী', 'পুঠিয়া'),
(468, 'রাজশাহী', 'রাজপাড়া'),
(469, 'রাজশাহী', 'শাহ মখদুম'),
(470, 'রাজশাহী', 'তানোর'),
(471, 'রাজশাহী', 'দূর্গাপুর'),
(472, 'রাঙ্গামাটি', 'বাঘাইছড়ি'),
(473, 'রাঙ্গামাটি', 'বেলাইছড়ি'),
(474, 'রাঙ্গামাটি', 'কাপ্তাই'),
(475, 'রাঙ্গামাটি', 'কাউখালী (বাতাবুনিয়া)'),
(476, 'রাঙ্গামাটি', 'নানিয়ারচর'),
(477, 'রাঙ্গামাটি', 'রাজস্থলী'),
(478, 'রাঙ্গামাটি', 'রাঙ্গামাটি সদর'),
(479, 'রংপুর', 'বদরগঞ্জ'),
(480, 'রংপুর', 'গঙ্গাচড়া'),
(481, 'রংপুর', 'কাউনিয়া'),
(482, 'রংপুর', 'মিঠাপুকুর'),
(483, 'রংপুর', 'পীরগাছা'),
(484, 'রংপুর', 'পীরগঞ্জ'),
(485, 'রংপুর', 'রংপুর সদর'),
(486, 'রংপুর', 'তারাগঞ্জ'),
(487, 'সাতক্ষীরা', 'আশাশুনি'),
(488, 'সাতক্ষীরা', 'দেবহাটা'),
(489, 'সাতক্ষীরা', 'কলারোয়া'),
(490, 'সাতক্ষীরা', 'কালীগঞ্জ'),
(491, 'সাতক্ষীরা', 'সাতক্ষীরা সদর'),
(492, 'সাতক্ষীরা', 'শ্যামনগর'),
(493, 'সাতক্ষীরা', 'তালা'),
(494, 'শরীয়তপুর', 'ভেদরগঞ্জ'),
(495, 'শরীয়তপুর', 'দামুদিয়া'),
(496, 'শরীয়তপুর', 'গোসাইরহাট'),
(497, 'শরীয়তপুর', 'নাড়িয়া'),
(498, 'শরীয়তপুর', 'শরীয়তপুর সদর'),
(499, 'শরীয়তপুর', 'জাজিরা'),
(500, 'শরীয়তপুর', 'শরীয়তপুর সদর'),
(501, 'শেরপুর', 'ঝিনাইগাতী'),
(502, 'শেরপুর', 'নকলা'),
(503, 'শেরপুর', 'নালিতাবাড়ী'),
(504, 'শেরপুর', 'শেরপুর সদর'),
(505, 'শেরপুর', 'শ্রীবরদী'),
(506, 'শেরপুর', 'শ্রীবরদী'),
(507, 'সিরাজগঞ্জ', 'বেলকুচি'),
(508, 'সিরাজগঞ্জ', 'চৌহালী'),
(509, 'সিরাজগঞ্জ', 'কামারখন্দ'),
(510, 'সিরাজগঞ্জ', 'কাজীপুর'),
(511, 'সিরাজগঞ্জ', 'রায়গঞ্জ'),
(512, 'সিরাজগঞ্জ', 'শাহজাদপুর'),
(513, 'সিরাজগঞ্জ', 'সিরাজগঞ্জ সদর'),
(514, 'সিরাজগঞ্জ', 'তরাস'),
(515, 'সিরাজগঞ্জ', 'উল্লাপাড়া'),
(516, 'সিরাজগঞ্জ', 'শাহজাদপুর'),
(517, 'সুনামগঞ্জ', 'বিশ্বম্ভরপুর'),
(518, 'সুনামগঞ্জ', 'ছাতক'),
(519, 'সুনামগঞ্জ', 'দক্ষিণ সুনামগঞ্জ'),
(520, 'সুনামগঞ্জ', 'দিরাই'),
(521, 'সুনামগঞ্জ', 'ধর্মপাশা'),
(522, 'সুনামগঞ্জ', 'দোয়ারাবাজার'),
(523, 'সুনামগঞ্জ', 'জগন্নাথপুর'),
(524, 'সুনামগঞ্জ', 'জামালগঞ্জ'),
(525, 'সুনামগঞ্জ', 'সুল্লা'),
(526, 'সুনামগঞ্জ', 'সুনামগঞ্জ সদর'),
(527, 'সুনামগঞ্জ', 'তাহেরপুর'),
(528, 'সিলেট', 'বেলাগঞ্জ'),
(529, 'সিলেট', 'বিয়ানীবাজার'),
(530, 'সিলেট', 'বিশ্বনাথ'),
(531, 'সিলেট', 'কোম্পানিগঞ্জ'),
(532, 'সিলেট', 'দক্ষিণ সুরমা'),
(533, 'সিলেট', 'ফেঞ্চুগঞ্জ'),
(534, 'সিলেট', 'গোলাবগঞ্জ'),
(535, 'সিলেট', 'গোয়াইনঘাট'),
(536, 'সিলেট', 'জৈন্তাপুর'),
(537, 'সিলেট', 'কানাইঘাট'),
(538, 'সিলেট', 'সিলেট সদর'),
(539, 'সিলেট', 'জাকিগঞ্জ'),
(540, 'টাঙ্গাইল', 'বাসাইল'),
(541, 'টাঙ্গাইল', 'ভুয়াপুর'),
(542, 'টাঙ্গাইল', 'দেলদুয়ার'),
(543, 'টাঙ্গাইল', 'ঘাটাইল'),
(544, 'টাঙ্গাইল', 'গোপালপুর'),
(545, 'টাঙ্গাইল', 'কালিহাতী'),
(546, 'টাঙ্গাইল', 'মধুপুর'),
(547, 'টাঙ্গাইল', 'মির্জাপুর'),
(548, 'টাঙ্গাইল', 'নাগরপুর'),
(549, 'টাঙ্গাইল', 'সখীপুর'),
(550, 'টাঙ্গাইল', 'টাঙ্গাইল সদর'),
(551, 'ঠাকুরগাঁও', 'বালিয়াডাঙ্গী'),
(552, 'ঠাকুরগাঁও', 'হরিপুর'),
(553, 'ঠাকুরগাঁও', 'পীরগঞ্জ'),
(554, 'ঠাকুরগাঁও', 'রানীশংকৈল'),
(555, 'ঠাকুরগাঁও', 'ঠাকুরগাঁও সদর');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coverage_area_bn`
--
ALTER TABLE `coverage_area_bn`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coverage_area_bn`
--
ALTER TABLE `coverage_area_bn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=556;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
