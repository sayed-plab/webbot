<?php 
include 'connection/connection.php';

mysqli_set_charset($link, "utf8"); 

$sql = "SELECT * FROM `contact_agent_buy` order BY id DESC";
$record = mysqli_query($link, $sql);

?>



<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--    [Site Title] -->
    <title>Airtel Admin</title>


    <!--    [Bootstrap Css] -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!--    [Animate Css]-->
    <link rel="stylesheet" href="assets/css/animate.css">

    <!--    [FontAwesome Css] -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!--    [IcoFont Css] -->
    <link rel="stylesheet" href="assets/css/icofont.css">

    <!--    [OwlCarousel Css]-->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">

    <!--    [Custom Stlesheet]-->
    <link rel="stylesheet" href="assets/css/nav.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">


    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/logo.png">


</head>

<body>
    <!--PRELOADER START-->

    <!--    [ Strat Preloader Area]-->

    <div class="pre-loader-area">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>


    <!--    [Finish Poreloader Area]-->
    <!--    [ Strat Header Area]-->
    <header>
    
        <div id="" class="side-menu-width">
            <div class="brand-ico-right">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
            <!--    [Finish Logo Area]-->
        </div>
        <!--    [Finish MENU Area]-->
    </header>
    <!--    [Finish Header Area]-->

<div class="container">
    <div class="logo text-center">
        <img src="assets/img/logo.png" width="100px">
    </div>

    <h2 class="text-uppercase text-center form-group">Agent Daily Panel List</h2>

    <div class="user-table">
        <table class="table table-bordered">
            <thead>
            <tr>
             
                
                <th>ID</th>
               
                <!--<th>User to Bot hit count</th>-->
             
                <th>User to Bot , excluding Contact agent and buy it count</th>
                 
                <th style="width: 10%;">DATE</th>
                <th style="width: 10%;">MONTH</th>
               
            </tr>
            </thead>
            <tbody>
            <?php while ($emp = mysqli_fetch_assoc($record)) { ?>

                <tr>
<?php if($emp['bot2user'] != NULL) { $new_var_bot_2user = round($emp['bot2user'] * $emp['user2bot'],0); } else  $new_var_bot_2user = $emp['user2bot']; ?>
                    <td><?php echo $emp['id'] ?></td>
                   
                    <!--<td><?php echo $emp['user2bot'] ?></td>-->
                  
                     <td><?php 
                    $xclud_user2bot = $new_var_bot_2user - ($emp['contact_agent']+$emp['buy']) ;
                    if($xclud_user2bot>0){
                          echo $xclud_user2bot;
                    }
                  
                    else{
                        //echo "<b>UNDEFINED DUE TO DATABASE CHANGE</b>"; 
                        echo $xclud_user2bot;
                    }
                    ?></td>
                  
                    
                    <td><b style="color:#CE2E35;"><?php echo $emp['date'] ?></b></td>
                    <td><b style="color:#CE2E35;"><?php 
                    $date = $emp['date'];
                    echo date("F", strtotime($date));
                    
                    ?></b></td>
                    
                   
                </tr>   
                   
            <?php } ?>
            


                    
  <!--              </td>-->
  <!--          </tr>-->
            </tbody>
        </table>
      
    </div>
</div>





<?php // include 'footer.php';?>

<!--    [ Start Footer Area]-->
<footer>
    <div class="copy-right">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>&copy; 2018. All Rights Reserved. Developed By <span>preneurlab.com</span></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--    [Finish Footer Area]-->

<!--SCROLL TOP BUTTON-->
<a href="#" class="top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>




<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<!--    [jQuery]-->
<script src="assets/js/jquery-3.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!--    [Popper Js] -->
<script src="assets/js/popper.min.js"></script>

<!--    [Bootstrap Js] -->
<script src="assets/js/bootstrap.min.js"></script>

<!--    [OwlCarousel Js]-->
<script src="assets/js/owl.carousel.min.js"></script>

<!--    [Navbar Fixed Js] -->
<script src="assets/js/navbar-fixed.js"></script>

<!--    [Main Custom Js] -->
<script src="assets/js/main.js"></script>
</body>

</html>

