<?php

require_once 'app/dashboard-controller.php';

$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
$date= $dt->format('d-m-Y');
$tim=$dt->format('h:i a');

$records = buzzXReports();

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <title>Airtel Buzz-x-report</title>
</head>
<body>

<div class="container-fluid mb-3 mt-3">
    <div class="col-lg-12 text-center">
        <div>
            <img src="https://forbangladesh.com/airtel_messengerx/dashboardx/assets/img/logo.png" alt="Airtel Logo">
        </div>
        <h2 class="mt-2">Agent Daily Panel List</h2>
        <p class="mt-2"><small>*These data are being audited and will be refreshed if required.</small></p>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">Agent Daily Panel</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-stripped table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Date</th>
                                <th scope="col">Month</th>
                                <th scope="col">No. of User</th>
                                <th scope="col">User to Bot Excluding Contact Agent</th>
                                <th scope="col">User to Bot Including Contact Agent</th>
                                <th scope="col">Bot to User</th>
                                <th scope="col">Response Rate (Bot to User/User to Bot)</th>
                                <th scope="col">Recharge Hit</th>
                                <th scope="col">Data Pack Purchase Hit</th>
                                <th scope="col">Tariff Plan</th>
                                <th scope="col">Prepaid Balance</th>
                                <th scope="col">Postpaid Balance</th>
                                <th scope="col">Add FnF</th>
                                <th scope="col">Check FnF</th>
                                <th scope="col">Delete FnF</th>
                                <th scope="col">Active Caller Tune</th>
                                <th scope="col">Deactive Caller Tune</th>
                                <th scope="col">Subs Caller Tune</th>
                                <th scope="col">Total Inbox Messages</th>
                                <th scope="col">Talkativity</th>
                                <th scope="col">Complaint</th>
                                <th scope="col">Winback Offer</th>
                            </tr>
                        </thead>
                        
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Date</th>
                                <th scope="col">Month</th>
                                <th scope="col">No. of User</th>
                                <th scope="col">User to Bot Excluding Contact Agent</th>
                                <th scope="col">User to Bot Including Contact Agent</th>
                                <th scope="col">Bot to User</th>
                                <th scope="col">Response Rate (Bot to User/User to Bot)</th>
                                <th scope="col">Recharge Hit</th>
                                <th scope="col">Data Pack Purchase Hit</th>
                                <th scope="col">Tariff Plan</th>
                                <th scope="col">Prepaid Balance</th>
                                <th scope="col">Postpaid Balance</th>
                                <th scope="col">Add FnF</th>
                                <th scope="col">Check FnF</th>
                                <th scope="col">Delete FnF</th>
                                <th scope="col">Active Caller Tune</th>
                                <th scope="col">Deactive Caller Tune</th>
                                <th scope="col">Subs Caller Tune</th>
                                <th scope="col">Total Inbox Messages</th>
                                <th scope="col">Talkativity</th>
                                <th scope="col">Complaint</th>
                                <th scope="col">Winback Offer</th>
                        </tfoot>
                        <tbody>
                        <?php foreach ($records as $emp): ?>

                            <tr>
                                <!-- id -->
                                <td><?= $emp['id'] ?></td>
                                <!-- date -->
                                <th scope="row"><?= $emp['date'] ?></th>
                                <!-- month -->
                                <th scope="row"><?= date("F", strtotime($emp['date'])); ?></th>
                                <!-- user -->
                                <td>
                                    <?php
                                        if($emp['date'] == "10-07-2018"){
                                            echo $a=$emp['user']+250;
                                        } else{
                                            echo $emp['user'];
                                        }
                                    ?>
                                </td>

                                <!-- user to bot excluding contact agent -->
                                <td><?php

                                    $user2bot = $emp['user_only']-$emp['user2agent'];

                                    if($emp['date'] == "10-07-2018"){
                                        echo $user2bot+810;
                                    } else{
                                        echo $user2bot;
                                    }

                                    ?></td>

                                <!-- user to bot including contact agent -->
                                <td><?php

                                    $user2bot = $emp['user_only'];

                                    if($emp['date'] == "10-07-2018"){
                                        echo $user2bot+810;
                                    } else{
                                        echo $user2bot;
                                    }

                                    ?></td>

                                <!-- bot to user -->
                                <td><?= $emp['user2bot'] ?></td>
                                <!-- response rate -->
                                <td><?= round($emp['user_only']/$emp['user2bot'], 2); ?>%</td>
                                <td><?php echo $emp['recharge_click'] ?></td>
                                <td><?php echo $emp['purchase_internet'] ?></td>
                                <td><?php echo $emp['tariff_plan'] ?></td>
                                <td><?php echo $emp['balance'] ?></td>
                                <td><?php echo $emp['postpaid_balance'] ?></td>
                                <td><?php echo $emp['add_fnf'] ?></td>
                                <td><?php echo $emp['check_fnf'] ?></td>
                                <td><?php echo $emp['delete_fnf'] ?></td>
                                <td><?php echo $emp['activate_caller_tune'] ?></td>
                                <td><?php echo $emp['deactivate_caller_tune'] ?></td>
                                <td><?php echo $emp['subs_caller_tune'] ?></td>
                                <td><?= $emp['total_message']; ?></td>
                                <!-- talkativity -->
                                <td><?php (!empty($emp['bot_talks']) || !empty($emp['user_talks'])) ? $value = (float)($emp['bot_talks']/$emp['user_talks']) : $value=0; $value= round($value, 2);echo $value."X";  ?></td>
                                <td><?php echo $emp['complaint'] ?></td>
                                <td><?php echo $emp['winback'] ?></td>

                        <?php endforeach;?>

                        </tbody>
                    </table>
                </div>

            </div>
            <div class="text-center card-footer">
                <small>Powered By Preneur Lab Ltd. © <?= date('Y')?></small>
            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready( function () {
        $('#myTable').DataTable( {
            "order": [[ 0, "desc" ]]
        });
    } );
</script>
</body>
</html>