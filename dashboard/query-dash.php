<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/security/functions.php';
require_once 'app/query-controller.php';

//unauthorizedUserRedirect('login.php');

ini_set('memory_limit', -1);

$messengerQuery = messengerQuery();

?>



<?php setPageTitle('Query'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>


<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <div class="float-left">
            <i class="fas fa-table"></i>
            Response Query
        </div>
    </div>
    
    <div class="card-body">
        
        <a href="add-query.php" class="btn btn-primary mb-2" style="width:80px">Add</a>
         
        <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <td>ID</td>
                <th>GREET EN</th>
                <th>GREET BN</th>
                <th>INTENT</th>
                <th>PARAM</th>
                <th>KEYWORDS</th>
                <th>REPLY TEXT EN</th>
                <th>REPLY TEXT BN</th>
                <th>CAROUSELS</th>
                <th>BUTTONS</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($messengerQuery as $emp):?>

                <tr>
                    <td><?php echo $emp['id'] ?></td>
                    <td><?php echo $emp['greet_en'] ?></td>
                    <td><?php echo $emp['greet_bn'] ?></td>
                    <td><?php echo $emp['intent'] ?></td>
                    <td><?php echo $emp['param_en'] ?></td>
                    <td><?php echo $emp['keywords'] ?></td>
                    <td><?php echo $emp['reply_text_en'] ?></td>
                    <td><?php echo $emp['reply_text_bn'] ?></td>
                    <td><?php echo $emp['carousels'] ?></td>
                    <td><?php echo $emp['buttons'] ?></td>
                    <td><?php echo $emp['quick_replies'] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        </div>
        
        
        </div>
    </div>
    <!--<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
</div>


<?php require_once 'footer.php';?>
