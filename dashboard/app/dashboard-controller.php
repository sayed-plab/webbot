<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:26 PM
 */

require_once 'core/Dashboard.php';
require_once 'security/functions.php';
require_once 'db/db.php';

use \dashboard\Dashboard as Dash;

# SESSION WILL INITIATE IF NO SESSION EXISTS
if(session_status() == PHP_SESSION_NONE){
    //There is no active session
    session();
}

# DATABASE CONFIG
Dash::dbConfig($db);

# TRAFFIC
function trafficDateGroup($start_date, $end_date){
    return Dash::trafficDateGroup($start_date, $end_date);
}

function trafficMonth($month, $year) {
    return Dash::trafficMonth($month, $year);
}


function todaysTraffic(){
    return Dash::todaysTraffic();
}

function todaysNewUser() {
    return Dash::todaysNewUser();
}

function todaysRepeatedUser() {
    return Dash::todaysRepeatedUser();
}

function newUserDateRange($start_date, $end_date) {
    return Dash::newUserDateRange($start_date, $end_date);
}

function newUserMonth($month, $year) {
    return Dash::newUserMonth($month, $year);
}

function totalNewUser() {
    return Dash::totalNewUser();
}

/*function trafficCount($start_date, $end_date){
    return Dash::trafficCount($start_date, $end_date);
}*/

function trafficCount($values) {
    $count = 0;
    foreach($values as $row) {
        $count += $row['users'];
    }

    return $count;
}

function trafficCountMonth($month, $year) {
    return Dash::trafficCountMonth($month, $year);
}

function todaysTrafficGroupByTime() {
    return Dash::todaysTrafficGroupByTime();
}

function totalTraffic() {
    return Dash::totalTraffic();
}

function totalTrafficDateGroup() {
    return Dash::totalTrafficDateGroup();
}

//var_dump(totalTrafficDateGroup());

# REVENUE
function revenueDateGroup($start_date, $end_date) {
    return Dash::revenueDateGroup($start_date, $end_date);
}

function revenueMonth($month, $year) {
    return Dash::revenueMonth($month, $year);
}

function todaysRevenue(){
    return Dash::todaysRevenue();
}

function totalRevenueDateGroup() {
    return Dash::totalRevenueDateGroup();
}

function revenueCount($values) {
    $count = 0;
    foreach($values as $row) {
        $count += $row['revenue'];
    }
    
    return array('total' => $count, 'average' => $count/count($values));
}

# DATA PURCHASED
function dataPurchasedDateGroup($start_date, $end_date) {
    return Dash::dataPurchasedDateGroup($start_date, $end_date);
}

function totalDataPurchasedDateGroup() {
    return Dash::totalDataPurchasedDateGroup();
}

function todaysDataPurchased() {
    return Dash::todaysDataPurchased();
}

function dataPurchasedMonth($month, $year){
    return Dash::dataPurchasedMonth($month, $year);
}

function dataPurchasedCount($values) {
    $count = 0;
    foreach($values as $row) {
        $count += $row['purchased'];
    }
    
    return array('total' => $count, 'average' => $count/count($values));
}

function dataPackageCount($start_date, $end_date){
    return Dash::dataPackageCount($start_date, $end_date);
}

function totalDataPackageCount() {
    return Dash::totalDataPackageCount();
}

function dataPackageCountMonth($month, $year){
    return Dash::dataPackageCountMonth($month, $year);
}

# USER INTERACTION
function todayUser2BotInteraction() {
    return Dash::todayUser2BotInteraction();
}

function user2BotInteractionDateGroup($start_date, $end_date) {
    return Dash::user2BotInteractionDateGroup($start_date, $end_date);
}

function totalUser2BotInteractionDateGroup() {
    return Dash::totalUser2BotInteractionDateGroup();
}

function user2BotInteractionMonth($month, $year) {
    return Dash::user2BotInteractionMonth($month, $year);
}

function user2BotInteractionCount($values) {
    $count = 0;
    foreach($values as $row) {
        $count += $row['users'];
    }
    
    return array('total' => $count, 'average' => $count/count($values));
}


#RECHARGE

function todaysRecharge() {
    
    return Dash::todaysRecharge();
}

function rechargeDateGroup($start_date, $end_date) {
    
    return Dash::rechargeDateGroup($start_date, $end_date);
}

function totalRechargeDateGroup() {
    return Dash::totalRechargeDateGroup();
}

function rechargeMonth($month, $year) {
    
    return Dash::rechargeMonth($month, $year);
}

function rechargeCount($values) {
    $count = 0;
    foreach($values as $row) {
        $count += $row['amount'];
    }
    
    return array('total' => $count, 'average' => $count/count($values));
}

# Buzz-x-report

function buzzXReports() {
    return Dash::buzzXReports();
}

# bestoffer campaign

function todaysBestOfferCampaignHit() {
    return Dash::todaysBestOfferCampaignHit();
}

function bestOfferCampaignHit($start_date, $end_date) {
    return Dash::bestOfferCampaignHit($start_date, $end_date);
}

function bestOfferCampaignHitMonth($month, $year) {
    return Dash::bestOfferCampaignHitMonth($month, $year);
}

function totalBestOfferCampaignHitMonth() {
    return Dash::totalBestOfferCampaignHitMonth();
}

