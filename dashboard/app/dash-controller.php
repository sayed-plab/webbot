<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:26 PM
 */

require_once 'core/Dash.php';
require_once 'security/functions.php';
require_once 'db/db.php';

use \dash\Dash;

# SESSION WILL INITIATE IF NO SESSION EXISTS
if(session_status() == PHP_SESSION_NONE){
    //There is no active session
    session();
}

# DATABASE CONFIG
Dash::dbConfig($db);

/**
 * Show Count Numbers Users, Messages, Account Management
 */


# Users
function totalFbUsers($start_date, $end_date) {
    return Dash::totalFbUsers($start_date, $end_date);
}

function fbUsersDateGroup($start_date, $end_date) {
    return Dash::fbUsersDateGroup($start_date, $end_date);
}

function todaysFbUsers() {
    return Dash::todaysFbUsers();
}

# Messages
function totalFbMessages($start_date, $end_date) {
    return Dash::totalFbMessages($start_date, $end_date);
}

function todaysFbMessages() {
    return Dash::todaysFbMessages();
}

function usersInteractionWithBotDateGroup($start_date, $end_date) {
    return Dash::usersInteractionWithBotDateGroup($start_date, $end_date);
}

function todaysUsersInteractionWithBot() {
    return Dash::todaysUsersInteractionWithBot();
}

# Gender Users
function totalFbMaleUsers($start_date, $end_date) {
    return Dash::totalFbMaleUsers($start_date, $end_date);
}

function totalFbFemaleUsers($start_date, $end_date) {
    return Dash::totalFbFemaleUsers($start_date, $end_date);
}

# Messages

function totalAnsweredByAgent($start_date, $end_date) {
    return Dash::totalAnsweredByAgent($start_date, $end_date);
}

function totalAnsweredByBot($start_date, $end_date) {
    return Dash::totalAnsweredByBot($start_date, $end_date);
}

function dailyFbMessages($start_date, $end_date) {
    return Dash::dailyFbMessages($start_date, $end_date);
}

function weeklyFbMessages($start_date, $end_date) {
    return Dash::weeklyFbMessages($start_date, $end_date);
}

function monthlyFbMessages($start_date, $end_date) {
    return Dash::monthlyFbMessages($start_date, $end_date);
}

# Account Management Users
function totalAccManagementUsers() {
    return Dash::totalAccManagementUsers();
}

function totalDataPurchaseUsers($start_date, $end_date) {
    return Dash::totalDataPurchaseUsers($start_date, $end_date);
}

function totalDataPurchaseRevenue($start_date, $end_date) {
    return Dash::totalDataPurchaseRevenue($start_date, $end_date);
}

function todaysDataPurchaseRevenue() {
    return Dash::todaysDataPurchaseRevenue();
}

function dataPurchaseRevenueDateGroup($start_date, $end_date) {
    return Dash::dataPurchaseRevenueDateGroup($start_date, $end_date);
}

function totalDataPurchaseSuccess($start_date, $end_date) {
    return Dash::totalDataPurchaseSuccess($start_date, $end_date);
}

function totalDataPurchaseFailed($start_date, $end_date) {
    return Dash::totalDataPurchaseFailed($start_date, $end_date);
}

function topDataPurchasePackagesCount($start_date, $end_date) {
    return Dash::topDataPurchasePackagesCount($start_date, $end_date);
}

function totalDataPurchaseSuccessUsers($start_date, $end_date) {
    return Dash::totalDataPurchaseSuccessUsers($start_date, $end_date);
}

function totalDataPurchaseFailedUsers($start_date, $end_date) {
    return Dash::totalDataPurchaseFailedUsers($start_date, $end_date);
}

function dataPurchaseSuccessDateGroup($start_date, $end_date) {
    return Dash::dataPurchaseSuccessDateGroup($start_date, $end_date);
}

function todaysDataPurchaseSuccess() {
    return Dash::todaysDataPurchaseSuccess();
}




