<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/29/18
 * Time: 11:48 AM
 */


/**********************************************************************
 *                              authentication
 **********************************************************************/

function unauthorizedUserRedirect($url) {
    if (!isset($_SESSION['authorized']))
    {
        header('Location:'.$url);
    }
}

function authorizedUserRedirect($url) {
    if (isset($_SESSION['authorized']))
    {
        header('Location:'.$url);
    }
}


function session($strict = false) {
    //http://php.net/manual/en/session.security.ini.php

    //session settings
   /* ini_set('session.use_only_cookies', 1);
    ini_set('session.use_strict_mode', 1);
    ini_set('session.hash_function', 'sha256');
    ini_set('session.use_trans_sid', 0);
    ini_set('session.sid_length', 48);
    ini_set('session.sid_bits_per_character', 6);*/

    $life_time = (60*60*24) * 30; // 30 Days
    $secure = isset($_SERVER["HTTPS"]) ? 1 :  0;

    session_name('_xvn');
    //session_set_cookie_params($life_time, '/', '', $secure, 1);
    session_start();

   /* if($strict)  {
        if (empty($_SESSION['initiated']))  {
            session_regenerate_id(true);
        }

        if (isset($_SESSION['HTTP_USER_AGENT']))  {
            if ($_SESSION['HTTP_USER_AGENT'] != encryption($_SERVER['REMOTE_ADDR']))  {
                logoutMe();
            }
        }
        else  {
            $_SESSION['HTTP_USER_AGENT'] = encryption($_SERVER['REMOTE_ADDR']);
        }
    }*/


}

function logoutMe() {
    session_unset();
    session_destroy();

    if (!isset($_SESSION['authorized']))
    {
        header('Location: index.php');
    }
}


/**********************************************************************
 *                              Cryptography
 **********************************************************************/

function encryption($string) {
    return openssl_encrypt($string,"AES-128-ECB",'#$@APmj09');
}

function decryption($string) {
    return openssl_decrypt($string,"AES-128-ECB",'#$@APmj09');
}



/**********************************************************************
 *                              CSRF
 **********************************************************************/
function CSRFToken() {
    return "<input type='hidden' name='csrf_token' value='".$_SESSION['csrf_token']."'>";
}

function verifyCSRFToken($form_csrf_token) {
    return ($form_csrf_token === 'ZjlWSu/QAg0F4aKV40hsCA==') ? true : false;
}



/**********************************************************************
 *                              validate and sanitize
 **********************************************************************/
function safeString($string) {
    return filter_var(stripslashes(strip_tags($string)), FILTER_SANITIZE_STRING);
}

function safeEmail($email) {
    if(filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        $safe_email = safeString(filter_var($email, FILTER_SANITIZE_EMAIL));
        return array('status' => 'safe', 'safe_email' => $safe_email);
    }
    else
    {
        return array('status' => 'invalid', 'invalid_email' => $email);
    }
}

function validateSanitizeURL($url) {
    if(filter_var($url, FILTER_VALIDATE_URL))
    {
        $valid_url = safeString(filter_var($url, FILTER_SANITIZE_URL));
        return array('status' => 'valid', 'valid_url' => $valid_url);
    }
    else
    {
        return array('status' => 'invalid', 'invalid_url' => null);
    }
}

function safeOutput($string) {
    return htmlspecialchars($string, ENT_QUOTES);
}

function sanitizeNumber($string) {
    return filter_var($string, FILTER_SANITIZE_NUMBER_INT);
}

