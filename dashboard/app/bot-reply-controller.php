<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:26 PM
 */

require_once 'core/BotReply.php';
require_once 'security/functions.php';
require_once 'db/db.php';

use \bot_reply\BotReply;

# SESSION WILL INITIATE IF NO SESSION EXISTS
if(session_status() == PHP_SESSION_NONE){
    //There is no active session
    session();
}

# DATABASE CONFIG
BotReply::dbConfig($db);

/**
 * @param $from_date
 * @param $to_date
 * @param $start_from
 * @param $record_per_page
 * @return mixed
 */


# Success Data Purchase

/*function botCanNotRepliesID($date) {
    return BotReply::botCanNotRepliesID($date);
}

function botCanNotReplyUserMessage($recipent, $id) {
    
    $max_id = BotReply::BotCanNotReplyMaxID($recipent, $id);
    
    return BotReply::fbUserMessageBotCanNotReply($max_id);
}
*/

function botCanNotReplies($date) {
    return BotReply::botCanNotReplies($date);
}

function botCanNotReplyGroupByDate() {
    return BotReply::botCanNotReplyGroupByDate();
}

function fbUserPic($fb_id) {

    $where = array(
        'fb_id =?' => $fb_id
    );
    $user_info = BotReply::fbUserInfo($where);

    return $user_info['profile_pic'];
}
