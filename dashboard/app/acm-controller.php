<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:26 PM
 */

require_once 'core/ACM.php';
require_once 'security/functions.php';
require_once 'db/db.php';

use \acm\ACM;

date_default_timezone_set('Asia/Dhaka');


# SESSION WILL INITIATE IF NO SESSION EXISTS
if(session_status() == PHP_SESSION_NONE){
    //There is no active session
    session();
}

# DATABASE CONFIG
ACM::dbConfig($db);

/**
 * Show Account Management
 */


# Data Purchase

function dataPurchaseHistory() {
    return ACM::dataPurchaseHistory();
}

function successDataPurchaseHistory() {
    return ACM::successDataPurchaseHistory();
}

function failedDataPurchaseHistory() {
    return ACM::failedDataPurchaseHistory();
}

/*function totalDataPurchased() {
    return ACM::totalDataPurchased();
}
*/
function totalDataPurchased($values) {
    
    $count = 0;
    
    foreach ($values as $data) {
        $count += $data['deducted_amount'];
    }
    
    return $count;
}

function dataPackages() {
    return ACM::dataPackages();
}

function dataPurchaseFilteredHistory() {
    
    $whereClauses = array(); 
      
    if (! empty($_GET['pack_name'])) {
      $whereClauses[] ="pack_name='".$_GET['pack_name']."'"; 
      $where = '';   
    }
    
    if (! empty($_GET['msisdn'])) {
      $whereClauses[] ="msisdn='".$_GET['msisdn']."'"; 
      $where = '';   
    }
    
     if (! empty($_GET['status'])) {
         
      if ($_GET['status'] == 'success') {
           $whereClauses[] ="deducted_amount>0"; 
           $where = '';   
      } elseif ($_GET['status'] == 'failed') {
           $whereClauses[] ="deducted_amount=0"; 
           $where = '';   
      }   
     
    }
    
    if (!empty($_GET['from_date']) && !empty($_GET['to_date'])) {
      $whereClauses[] = "STR_TO_DATE(date,'%d-%m-%Y') BETWEEN '".date('Y-m-d',strtotime($_GET['from_date']))."' AND '".date('Y-m-d',strtotime($_GET['to_date']))."'";
     // $whereClauses[] ="date<='".date('d-m-Y',strtotime($_GET['to_date']))."'"; 
      $where = '';   
    }
     

    if (count($whereClauses) > 0) 
    { 
      $where = ' WHERE '.implode(' AND ',$whereClauses); 
    } 
    
        //return $where;
        
    return ACM::dataPurchaseFilteredHistory($where);
}
//var_dump(dataPurchaseFilteredHistory());

# Recharge

/*function rechargeHistory() {
   return ACM::rechargeHistory(); 
}
*/

function rechargeHistory() {

    $whereClauses = array();
    $where = '';

    if (! empty($_GET['status'])) {

        if ($_GET['status'] == 'success') {
            $whereClauses[] ="status='success'";
        } elseif ($_GET['status'] == 'failed') {
            $whereClauses[] ="status='failed'";
        }

    }

    if (! empty($_GET['msisdn'])) {
        $whereClauses[] ="mobile='".$_GET['msisdn']."'";
    }

    if (!empty($_GET['from_date']) && !empty($_GET['to_date'])) {
        $whereClauses[] = "STR_TO_DATE(date,'%d-%m-%Y') BETWEEN '".date('Y-m-d',strtotime($_GET['from_date']))."' AND '".date('Y-m-d',strtotime($_GET['to_date']))."'";
    }


    if (count($whereClauses) > 0)
    {
        $where = ' WHERE '.implode(' AND ',$whereClauses);
    }

    //return $where;

    return ACM::rechargeHistory($where);
}

function totalRechargeHistory($rechargeHistory, $home) {

    $sum = 0;

    foreach ($rechargeHistory as $row) {

        if ($home) {

            if ($row['status'] == 'success') {
                $sum += $row['amount'];
            }

        } else {

            if ($row['status'] == 'success') {
                $sum += $row['amount'];
            } elseif ($row['status'] == 'failed') {
                $sum += $row['amount'];
            }
        }



    }

    return $sum;
}

function topRecharge() {
    return ACM::topRecharge();
}

function fbUserPic($fb_id) {

    $where = array(
        'fb_id =?' => $fb_id
    );
    $user_info = ACM::fbUserInfo($where);

    return $user_info['profile_pic'];
}
