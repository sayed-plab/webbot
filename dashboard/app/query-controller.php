<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:26 PM
 */

require_once 'core/Query.php';
require_once 'security/functions.php';
require_once 'db/db.php';

use \query\Query;

# SESSION WILL INITIATE IF NO SESSION EXISTS
if(session_status() == PHP_SESSION_NONE){
    //There is no active session
    session();
}

# DATABASE CONFIG
Query::dbConfig($db);

function messengerQuery() {
    return Query::messengerQuery();
}

function fbUserPic($fb_id) {

    $where = array(
        'fb_id =?' => $fb_id
    );
    $user_info = Query::fbUserInfo($where);

    return $user_info['profile_pic'];
}

function queryById($id) {
    
    $where = array(
        'id =?' => (int)$id
    );
    return Query::queryById($where);
}


function addQuery($values) {
    
    $query_info = array(
        $values['greet_en'],
        $values['greet_bn'],
        $values['intent'],
        $values['param'],
        $values['keywords'],
        $values['response_text_en'],
        $values['response_text_bn'],
        $values['carousels'],
        $values['buttons'],
        $values['quick_replies'],
        );

    return Query::addQuery($query_info);
}

function addButton($values) {

    $button_info = array(
        safeString($values['mapping']),
        safeString($values['button_type']),
        safeString($values['url_title']),
        safeString($values['title_payload']),
    );

    return Query::addButton($button_info);
}

function addQuickReply($values) {

    $button_info = array(
        safeString($values['mapping']),
        safeString($values['button_type']),
        safeString($values['url_title']),
        safeString($values['title_payload']),
    );

    return Query::addQuickReply($button_info);
}

function updateQuery($values) {
    
    $columns = array(
        'intent=?' => $values['intent'],
        'greet_en=?' => $values['greet_en'],
        'greet_bn=?' => $values['greet_bn'],
        'param_en=?' => $values['param'],
        'reply_text_en=?' => $values['response_text_en'],
        'reply_text_bn=?' => $values['response_text_bn'],
        'carousels=?' => $values['carousels'],
        'buttons=?' => $values['buttons'],
        'quick_replies=?' =>$values['quick_replies'],
        'keywords=?' => $values['keywords'],
        );

    $base_columns = array(
        'id=?' => $values['id']
        );
    
    return Query::updateQuery($columns,$base_columns);
}

function buttonsAll() {
    return Query::buttonsAll();
}

function quickRepliesAll() {
    return Query::quickRepliesAll();
}

