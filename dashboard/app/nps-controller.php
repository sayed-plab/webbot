<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:26 PM
 */

require_once 'core/NPS.php';
require_once 'security/functions.php';
require_once 'db/db.php';


# SESSION WILL INITIATE IF NO SESSION EXISTS
if(session_status() == PHP_SESSION_NONE){
    //There is no active session
    session();
}

# DATABASE CONFIG
NPS::dbConfig($db);


function nps_all() {
    return NPS::nps_all();
}


function fbUserPic($fb_id) {

    $where = array(
        'fb_id =?' => $fb_id
    );
    $user_info = NPS::fbUserInfo($where);

    return $user_info['profile_pic'];
}

function fbUserName($fb_id) {

    $where = array(
        'fb_id =?' => $fb_id
    );
    $user_info = NPS::fbUserInfo($where);

    return $user_info['first_name']. " " .$user_info['last_name'];
}

function totalNPSFiltered() {

    $whereClauses = array();
    $where = '';
    $today = date('d-m-Y');

    if (! empty($_GET['reply'])) {

        if ($_GET['reply'] == 'Yes') {
            $whereClauses[] ="answer='yes'";
            $where = '';
        } elseif ($_GET['reply'] == 'No') {
            $whereClauses[] ="answer='no'";
            $where = '';
        } elseif ($_GET['reply'] == 'Comment') {
            $whereClauses[] ="answer !='no' and answer != 'yes' and answer !=''";
            $where = '';

        } elseif ($_GET['reply'] == 'NoReply') {
            $whereClauses[] ="answer = ''";
            $where = '';
        }
    }

    if (!empty($_GET['from_date']) && !empty($_GET['to_date'])) {
        $whereClauses[] = "STR_TO_DATE(date,'%d-%m-%Y') BETWEEN '".date('Y-m-d',strtotime($_GET['from_date']))."' AND '".date('Y-m-d',strtotime($_GET['to_date']))."'";
        $where = '';
    }


    if (count($whereClauses) > 0)
    {
        $where = ' WHERE '.implode(' AND ',$whereClauses);
    }

    if (empty($_GET['from_date']) && empty($_GET['to_date'])) {
        $where = " WHERE date = '".$today."'";
    }

    return NPS::totalNPSFiltered($where);
}

function npsFiltered($offset, $no_of_records_per_page) {

    $whereClauses = array();
    $where = '';
    $today = date('d-m-Y');

    if (! empty($_GET['reply'])) {

        if ($_GET['reply'] == 'Yes') {
            $whereClauses[] ="answer='yes'";
            $where = '';
        } elseif ($_GET['reply'] == 'No') {
            $whereClauses[] ="answer='no'";
            $where = '';
        } elseif ($_GET['reply'] == 'Comment') {
            $whereClauses[] ="answer !='no' and answer != 'yes' and answer !=''";
            $where = '';

        } elseif ($_GET['reply'] == 'NoReply') {
            $whereClauses[] ="answer = ''";
            $where = '';
        }
    }

    if (!empty($_GET['from_date']) && !empty($_GET['to_date'])) {
        $whereClauses[] = "STR_TO_DATE(date,'%d-%m-%Y') BETWEEN '".date('Y-m-d',strtotime($_GET['from_date']))."' AND '".date('Y-m-d',strtotime($_GET['to_date']))."'";
        $where = '';
    }


    if (count($whereClauses) > 0)
    {
        $where = ' WHERE '.implode(' AND ',$whereClauses);
    }

    if (empty($_GET['from_date']) && empty($_GET['to_date'])) {
        $where = " WHERE date = '".$today."'";
    }

    return NPS::npsFiltered($where, $offset, $no_of_records_per_page);
}

function iceAnalytics($date, $filter) {
    $date = date('d-m-Y',strtotime($date));
    return NPS::iceAnalytics($date, $filter);
}

function npsEveryDate($date) {
    return NPS::npsEveryDate($date);
}

function iceNPSGroupByDate() {
    return NPS::iceNPSGroupByDate();
}
