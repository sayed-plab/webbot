<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:26 PM
 */

require_once 'core/Retrain.php';
require_once 'security/functions.php';
require_once 'db/db.php';

use \retrain\Retrain;

# SESSION WILL INITIATE IF NO SESSION EXISTS
if(session_status() == PHP_SESSION_NONE){
    //There is no active session
    session();
}

# DATABASE CONFIG
Retrain::dbConfig($db);

# Success Data Purchase

function untrainedReplies($date) {
    return Retrain::untrainedReplies($date);
}

function totalUntrainedReplies($date) {
    return Retrain::totalUntrainedReplies($date);
}

function parameters($lang) {
    return Retrain::parameters($lang);
}

function addRetrain($values) {
    
         
    $retrain = array(
        NULL,
        $values['param'],
        $values['tag'],
        $values['message']
        );
        
    $lang = $values['lang'];
    $id = $values['id'];
        
    $status = Retrain::addRetrain($retrain, $lang);
    
    if ($status) {
       Retrain::updateTrainStatus(array('action =?' => 'trained'), array('id=?' => $id)); 
    }
}

function discardRetrain($id) {
    Retrain::updateTrainStatus(array('action =?' => 'discard'), array('id=?' => $id));
}

function deleteRetrain($date) {
    $date = date('d-m-Y', strtotime($date));
    Retrain::updateTrainStatus(array('action =?' => 'discard'), array('date=?' => $date));
}

