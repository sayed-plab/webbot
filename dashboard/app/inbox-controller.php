<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:26 PM
 */

require_once 'core/Inbox.php';
require_once 'security/functions.php';
require_once 'db/db.php';

use \inbox\Inbox;

# SESSION WILL INITIATE IF NO SESSION EXISTS
if(session_status() == PHP_SESSION_NONE){
    //There is no active session
    session();
}

# DATABASE CONFIG
Inbox::dbConfig($db);

/**
 * @param $from_date
 * @param $to_date
 * @param $start_from
 * @param $record_per_page
 * @return mixed
 */


# Success Data Purchase

function inboxFiltered($start_date, $end_date) {
    return Inbox::inboxFiltered($start_date, $end_date);
}

function showSingleInbox($user) {
    return Inbox::showSingleInbox($user);
}