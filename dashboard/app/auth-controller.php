<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/21/18
 * Time: 2:30 PM
 */


require_once 'core/Auth.php';
require_once 'security/functions.php';
require_once 'db/db.php';

use auth\Auth as auth;

# SESSION WILL INITIATE IF NO SESSION EXISTS
if(session_status() == PHP_SESSION_NONE){
    //There is no active session
    session();
}

# DATABASE CONFIG
auth::dbConfig($db);

# USER LOGIN
function userLogin($values)
{
    $not_empty = !empty($values['email']) && !empty($values['password']);

    # CONTINUE IF FIELDS ARE FILLED
    if($not_empty == true)
    {
        $where = array(
            'email = ?' => $values['email']
        );

        $user = auth::users_by_email($where);

        # CONTINUE IF USER EXISTS
        if(!empty($user))
        {
            if(password_verify($values['password'], $user['password']))
            {
                $_SESSION['initiated'] = true;
                $_SESSION['authorized'] = $user['id'];
                $_SESSION['csrf_token'] = encryption(uniqid(true));

                if(isset($_SESSION['authorized']))
                {
                    header('Location: index.php');
                }

            }
            else
            {
                # STATUS - PASSWORD WRONG
                return 'passw_wrong';
            }
        }
        else
        {
            # STATUS - USER DOES NOT EXISTS
            return 'exists_false';
        }

    }

    else
    {
        # STATUS - EMPTY FIELDS
        return 'fields_empty';
    }


}

function userReg($values)
{
    $name = safeString($values['name']);
    $email = safeEmail($values['email']);
    $safe_email = $email['safe_email'];
    $password = password_hash($values['password'], 1);

    $not_empty = !empty($values['name']) && !empty($values['email']) && !empty($values['password']);

    if($not_empty == true)
    {
        if($email['status'] === 'safe')
        {
            $where = array(
                'email = ?' => $values['email']
            );

            $user = auth::users_by_email($where);

            if(empty($user)) {

                $user_info = array(
                    null,
                    $name,
                    $safe_email,
                    $password
                );

                $reg_status = auth::user_reg($user_info);

                if($reg_status == true)
                {
                    return 'reg_success';
                }
            }else {
                # STATUS - USER EXISTS
                return 'exists_true';
            }


        }
        else
        {
            # STATUS - INVALID EMAIL
            return 'email_invalid';
        }

    }
    else
    {
        # STATUS - EMPTY FIELDS
        return 'fields_empty';
    }
}

