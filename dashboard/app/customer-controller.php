<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/21/18
 * Time: 2:30 PM
 */


require_once 'core/Customer.php';
require_once 'security/functions.php';
require_once 'db/db.php';

use customer\Customer as customer;

# SESSION WILL INITIATE IF NO SESSION EXISTS
if(session_status() == PHP_SESSION_NONE){
    //There is no active session
    session();
}

# DATABASE CONFIG
customer::dbConfig($db);


function getUserName($user) {
    return customer::getUserName($user);
}
