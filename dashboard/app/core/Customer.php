<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 9/5/18
 * Time: 1:12 PM
 */


namespace customer;

require_once 'Core.php';

use core\Core;

class Customer extends Core {

    public static function getUserName($user) {

        $sql = "SELECT name FROM `customer` WHERE id = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($user));
        $resp = $stmt->fetch(2);
        return $resp['name'];
    }

}