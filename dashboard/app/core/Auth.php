<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 9/5/18
 * Time: 1:12 PM
 */


namespace auth;

require_once 'Core.php';

use core\Core;

class Auth extends Core {

    public static function users_by_email($where) {

        return self::dbFetchWithCond('users_', $where, 'single');

    }

    public static function user_reg($values) {

        return self::dbInsert('users_', $values);

    }

    public static function getUserName($user) {

        $sql = "SELECT name FROM `customer` WHERE id = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($user));
        $resp = $stmt->fetch(2);
        return $resp['name'];
    }

}