<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:28 PM
 */

namespace inbox;

require_once 'Core.php';

use \core\Core;

date_default_timezone_set('Asia/Dhaka');

class Inbox extends Core {

    public static function inboxFiltered($start_date, $end_date) {

        $sql = "SELECT * FROM `messages` WHERE STR_TO_DATE(date, '%Y-%m-%d') BETWEEN  ? AND ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($start_date, $end_date));
        return $stmt->fetchAll(2);
    }

    public static function showSingleInbox($user) {

        $sql = "SELECT * FROM `messages` WHERE user=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($user));
        return $stmt->fetchAll(2);
    }
}