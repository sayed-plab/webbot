<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:28 PM
 */

namespace acm;

require_once 'Core.php';

use \core\Core;

date_default_timezone_set('Asia/Dhaka');

class ACM extends Core {

    public static function dataPurchaseHistory() {

        $sql = "SELECT * FROM `data_purchase`";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(2);
    }
    
    public static function successDataPurchaseHistory() {
        $sql = "SELECT * FROM `successful_data_purchse_hit` WHERE deducted_amount > 0 GROUP BY msisdn HAVING COUNT(DISTINCT msisdn) > 1";
        //$sql = "SELECT * FROM successful_data_purchse_hit WHERE deducted_amount > 0 ORDER BY id DESC";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();
        
        return $stmt->fetchAll(2);
    }
    
    public static function failedDataPurchaseHistory() {
        $sql = "SELECT * FROM successful_data_purchse_hit WHERE deducted_amount <= 0 ORDER BY id DESC";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();
        
        return $stmt->fetchAll(2);
    }
    
    public static function dataPackages() {

        $sql = "SELECT DISTINCT pack_name FROM `successful_data_purchse_hit`";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(2);
    }
    
    public static function dataPurchaseFilteredHistory($where) {
        $sql = "SELECT * FROM successful_data_purchse_hit " .$where . " ORDER BY id DESC";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();
        
        return $stmt->fetchAll(2);
    }
    
    public static function totalDataPurchased() {
        $sql = "SELECT SUM(deducted_amount) AS total FROM successful_data_purchse_hit";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();
        $res = $stmt->fetch(2);
        
        return $res['total'];
    }
    
  /*  public static function rechargeHistory() {
        return self::dbFetchWithOutCond('save_payment', 'multiple', 'id', 'DESC');
    }*/

    public static function rechargeHistory($where) {

        $sql = "SELECT * FROM `save_payment` " .$where . " ORDER BY id DESC";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(2);
    }

    public static function topRecharge() {
        $sql = "(SELECT count(amount) as count, mobile as msisdn, SUM(amount) as total FROM `save_payment` WHERE status = 'success' GROUP BY mobile) ORDER BY total DESC LIMIT 0, 5";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(2);
    }

}