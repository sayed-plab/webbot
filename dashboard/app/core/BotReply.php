<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:28 PM
 */

namespace bot_reply;

require_once 'Core.php';

use \core\Core;

date_default_timezone_set('Asia/Dhaka');

class BotReply extends Core {

/*    public static function botCanNotRepliesID($date) {
        $sql = "SELECT id,receipent FROM facebook_bot_messages WHERE (message_content LIKE '%sorry, we%' OR message_content LIKE '%দুঃখিত, এই%' ) AND sender='BOT'
AND date= ? ORDER BY id ASC LIMIT 0,50";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($date));
        return $stmt->fetchAll(2);
    }*/
    
 /*   public static function BotCanNotReplyMaxID($recipent, $id) {
        $sql="SELECT MAX(id) AS maid FROM bot_cannot_reply WHERE sender= ? AND id < ?  ";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($recipent, $id));
        $res = $stmt->fetch(2);
        return $res['maid'];
    }*/
    
  /*  public static function fbUserMessageBotCanNotReply($id) {
        $sql="SELECT sender, message_content, date FROM bot_cannot_reply WHERE  id = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($id));
        return $stmt->fetch(2);
    }
    */
    
    public static function botCanNotReplies($date) {
        $sql = "SELECT receipent, message_content,date,time FROM `bot_cannot_reply` WHERE date =?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($date));
        return $stmt->fetchAll(2);
        
    }
    
    public static function botCanNotReplyGroupByDate() {
        $sql="SELECT id,date, COUNT(*) AS total FROM `bot_cannot_reply` GROUP BY date ORDER BY id DESC";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(2);
    } 
}