<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:28 PM
 */

namespace dashboard;

require_once 'Core.php';

use \core\Core;

date_default_timezone_set('Asia/Dhaka');

class Dashboard extends Core {
    
    # TRAFFIC
    public static function trafficDateGroup($start_date, $end_date) {
        
        $sql="SELECT COUNT(DISTINCT(sender)) as users, STR_TO_DATE(date,'%d-%m-%Y') as date FROM `facebook_bot_messages` WHERE receipent = ? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? ) GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('BOT',$start_date, $end_date));
        return $stmt->fetchAll(2);

    }
    
    public static function trafficCount($start_date, $end_date) {
        
        $sql="SELECT DISTINCT(sender) FROM `facebook_bot_messages` WHERE receipent = ? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('BOT',$start_date, $end_date));
        return $stmt->rowCount();

    }
    
    public static function trafficCountMonth($month, $year) {
        
        $sql="SELECT DISTINCT(sender) FROM `facebook_bot_messages` WHERE receipent = ? AND MONTH(STR_TO_DATE(date,'%d-%m-%Y')) =? AND YEAR(STR_TO_DATE(date,'%d-%m-%Y')) =?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('BOT',$month, $year));
        return $stmt->rowCount();

    }
    
    public static function trafficMonth($month, $year) {
        
        $sql="SELECT COUNT(DISTINCT(sender)) as users, STR_TO_DATE(date,'%d-%m-%Y') as date FROM `facebook_bot_messages` WHERE receipent = ? AND MONTH(STR_TO_DATE(date,'%d-%m-%Y')) =? AND YEAR(STR_TO_DATE(date,'%d-%m-%Y')) =? GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('BOT',$month, $year));
        return $stmt->fetchAll(2);

    }
    
    public static function todaysTraffic() {

        $sql="SELECT DISTINCT(sender) FROM `facebook_bot_messages` WHERE receipent = ? AND date = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('bot', date('d-m-Y')));
        return $stmt->rowCount();
    }
    
    public static function todaysRepeatedUser() {
        
        $sql = "SELECT DISTINCT(sender) FROM `facebook_bot_messages` INNER JOIN `messenger_info` ON facebook_bot_messages.sender = messenger_info.fb_id 
        WHERE facebook_bot_messages.receipent = ? AND facebook_bot_messages.date = ? AND messenger_info.first_join != ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('bot', date('d-m-Y'), date('d-m-Y')));
        return $stmt->rowCount();
        
    }
    
    
    public static function newUserDateRange($start_date, $end_date) {
        
        $sql="SELECT id FROM `messenger_info` WHERE (STR_TO_DATE(first_join,'%d-%m-%Y') BETWEEN ? AND ? )";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($start_date, $end_date));
        return $stmt->rowCount();
        
    }
    
    public static function newUserMonth($month, $year) {
        
        $sql="SELECT id FROM `messenger_info` WHERE MONTH(STR_TO_DATE(first_join,'%d-%m-%Y')) =? AND YEAR(STR_TO_DATE(first_join,'%d-%m-%Y')) =?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($month, $year));
        return $stmt->rowCount();
        
    }
    
    public static function totalNewUser() {
        
        $sql="SELECT id FROM `messenger_info`";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();
        return $stmt->rowCount();
        
    }
    
    public static function todaysNewUser() {
        
        $sql="SELECT * FROM `messenger_info` WHERE first_join = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(date('d-m-Y')));
        return $stmt->rowCount();
    }
    
    public static function todaysTrafficGroupByTime() {
        $sql = "SELECT COUNT(DISTINCT(sender)) as users, time,date2timestamp FROM `facebook_bot_messages` WHERE receipent = ? AND date= ? GROUP BY hour(time) ORDER BY time DESC";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('bot', date('d-m-Y')));
        return $stmt->fetchAll(2);
    }
    
    public static function totalTraffic() {
        
        $sql="SELECT DISTINCT(sender) FROM `facebook_bot_messages` WHERE receipent = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('bot'));
        return $stmt->rowCount();
    }
    
    public static function totalTrafficDateGroup() {
        
        $sql="SELECT COUNT(DISTINCT(sender)) as users, STR_TO_DATE(date,'%d-%m-%Y') as date FROM `facebook_bot_messages` WHERE receipent = ? GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('BOT'));
        return $stmt->fetchAll(2);

    }
    
    # REVENUE
    public static function revenueDateGroup($start_date, $end_date) {
        $sql="SELECT STR_TO_DATE(date,'%d-%m-%Y') as date, SUM(deducted_amount) as revenue FROM `successful_data_purchse_hit` WHERE deducted_amount > ? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? ) GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0,$start_date, $end_date));
        return $stmt->fetchAll(2);
    }
    
    public static function totalRevenueDateGroup() {
        $sql="SELECT STR_TO_DATE(date,'%d-%m-%Y') as date, SUM(deducted_amount) as revenue FROM `successful_data_purchse_hit` WHERE deducted_amount > ? GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0));
        return $stmt->fetchAll(2);
    }
    
    public static function revenueMonth($month, $year) {
        $sql="SELECT STR_TO_DATE(date,'%d-%m-%Y') as date, SUM(deducted_amount) as revenue FROM `successful_data_purchse_hit` WHERE deducted_amount > ?  AND MONTH(STR_TO_DATE(date,'%d-%m-%Y')) =? AND YEAR(STR_TO_DATE(date,'%d-%m-%Y')) =? GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0,$month, $year));
        return $stmt->fetchAll(2);
    }
    
    public static function todaysRevenue() {
        $sql="SELECT SUM(deducted_amount) as revenue FROM `successful_data_purchse_hit` WHERE date = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(date('d-m-Y')));
        $res = $stmt->fetch(2);
        return $res['revenue'];
    }
    
    # DATA PURCHASED
    public static function dataPurchasedDateGroup($start_date, $end_date) {
        $sql="SELECT STR_TO_DATE(date,'%d-%m-%Y') as date, COUNT(id) as purchased FROM `successful_data_purchse_hit` WHERE deducted_amount > ? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? ) GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0,$start_date, $end_date));
        return $stmt->fetchAll(2);
    }
    
    public static function totalDataPurchasedDateGroup() {
        $sql="SELECT STR_TO_DATE(date,'%d-%m-%Y') as date, COUNT(id) as purchased FROM `successful_data_purchse_hit` WHERE deducted_amount > ? GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0));
        return $stmt->fetchAll(2);
    }
    
    public static function dataPurchasedMonth($month, $year) {
        $sql="SELECT STR_TO_DATE(date,'%d-%m-%Y') as date, COUNT(id) as purchased FROM `successful_data_purchse_hit` WHERE deducted_amount > ? AND MONTH(STR_TO_DATE(date,'%d-%m-%Y')) =? AND YEAR(STR_TO_DATE(date,'%d-%m-%Y')) =? GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0,$month, $year));
        return $stmt->fetchAll(2);
    }
    
     public static function todaysDataPurchased() {
        $sql="SELECT id FROM `successful_data_purchse_hit` WHERE deducted_amount > ? AND date = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0, date('d-m-Y')));
        return $stmt->rowCount();
    }
    
    public static function dataPackageCount($start_date, $end_date) {
        $sql = "SELECT pack_name, count(pack_name) as count FROM `successful_data_purchse_hit` WHERE deducted_amount > ? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? ) GROUP BY pack_name" ;
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0,$start_date, $end_date));
        return $stmt->fetchAll(2);
    }
    
    public static function totalDataPackageCount() {
        $sql = "SELECT pack_name, count(pack_name) as count FROM `successful_data_purchse_hit` WHERE deducted_amount > ? GROUP BY pack_name" ;
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0));
        return $stmt->fetchAll(2);
    }
    
     public static function dataPackageCountMonth($month, $year) {
        $sql = "SELECT pack_name, count(pack_name) as count FROM `successful_data_purchse_hit` WHERE deducted_amount > ? AND MONTH(STR_TO_DATE(date,'%d-%m-%Y')) =? AND YEAR(STR_TO_DATE(date,'%d-%m-%Y')) =? GROUP BY pack_name" ;
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0,$month, $year));
        return $stmt->fetchAll(2);
    }
    
    # USER INTERACTION
    public static function todayUser2BotInteraction() {
        $sql = "SELECT id as interations, STR_TO_DATE(date,'%d-%m-%Y') as date FROM `facebook_bot_messages` WHERE sender != 'BOT' AND date=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(date('d-m-Y')));
        return $stmt->rowCount();
    }
    
    public static function user2BotInteractionDateGroup($start_date, $end_date) {
        $sql = "SELECT COUNT(id) as users, STR_TO_DATE(date,'%d-%m-%Y') as date  FROM `facebook_bot_messages` WHERE sender != 'BOT' AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? ) GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($start_date, $end_date));
        return $stmt->fetchAll(2);
    }
    
    public static function totalUser2BotInteractionDateGroup() {
        $sql = "SELECT COUNT(id) as users, STR_TO_DATE(date,'%d-%m-%Y') as date  FROM `facebook_bot_messages` WHERE sender != 'BOT' GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(2);
    }
    
    public static function user2BotInteractionMonth($month, $year) {
        $sql = "SELECT COUNT(id) as users, STR_TO_DATE(date,'%d-%m-%Y') as date  FROM `facebook_bot_messages` WHERE sender != 'BOT' AND MONTH(STR_TO_DATE(date,'%d-%m-%Y')) =? AND YEAR(STR_TO_DATE(date,'%d-%m-%Y')) =? GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($month, $year));
        return $stmt->fetchAll(2);
    }
    
    # RECHARGE
    
    public static function todaysRecharge() {
        $sql = "SELECT SUM(amount) as amount FROM `save_payment` WHERE status !=? AND date = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('failed',date('d-m-Y')));
        return $stmt->fetch(2);
    }
    
    public static function rechargeDateGroup($start_date, $end_date) {
        $sql = "SELECT SUM(amount) as amount, STR_TO_DATE(date,'%d-%m-%Y') as date  FROM save_payment WHERE  status !=? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? ) GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('failed',$start_date, $end_date));
        return $stmt->fetchAll(2);
    }
    
    public static function totalRechargeDateGroup() {
        $sql = "SELECT SUM(amount) as amount, STR_TO_DATE(date,'%d-%m-%Y') as date  FROM save_payment WHERE  status !=?  GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('failed'));
        return $stmt->fetchAll(2);
    }
    
    public static function rechargeMonth($month, $year) {
        $sql = "SELECT SUM(amount) as amount, STR_TO_DATE(date,'%d-%m-%Y') as date  FROM save_payment WHERE status !=? AND MONTH(STR_TO_DATE(date,'%d-%m-%Y')) =? AND YEAR(STR_TO_DATE(date,'%d-%m-%Y')) =? GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('failed',$month, $year));
        return $stmt->fetchAll(2);
    }
    
    # Buzz-x-report

    public static function buzzXReports() {
        return self::dbFetchWithOutCond('contact_agent_buy', 'multiple', 'id','DESC');
    }

    # bestoffer campaign

    public static function todaysBestOfferCampaignHit() {

        $sql = "SELECT id FROM `facebook_bot_messages` WHERE date= ? and message_content = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(date('d-m-Y'), 'bestoffer'));
        return $stmt->rowCount();
    }

    public static function bestOfferCampaignHit($start_date, $end_date) {

        $sql = "SELECT id FROM `facebook_bot_messages` WHERE (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )  and message_content = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($start_date, $end_date, 'bestoffer'));
        return $stmt->rowCount();
    }

    public static function bestOfferCampaignHitMonth($month, $year) {

        $sql = "SELECT id FROM `facebook_bot_messages` WHERE MONTH(STR_TO_DATE(date,'%d-%m-%Y')) =? AND YEAR(STR_TO_DATE(date,'%d-%m-%Y')) =?  and message_content = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($month, $year, 'bestoffer'));
        return $stmt->rowCount();
    }

    public static function totalBestOfferCampaignHitMonth() {

        $sql = "SELECT id FROM `facebook_bot_messages` WHERE message_content = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('bestoffer'));
        return $stmt->rowCount();
    }

    
    
}