<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:28 PM
 */

namespace query;

require_once 'Core.php';

use \core\Core;

date_default_timezone_set('Asia/Dhaka');

class Query extends Core {

    public static function messengerQuery() {

        return self::dbFetchWithOutCond('query_response', 'multiple');
    }

    public static function buttonsAll() {

        return self::dbFetchWithOutCond('buttons', 'multiple');
    }

    public static function quickRepliesAll() {

        return self::dbFetchWithOutCond('quick_replies', 'multiple');
    }
    
    public static function queryById($where) {
        
        return self::dbFetchWithCond('query_response', $where, 'single');
        
    }
    
    public static function addQuery($values) {
        
        $sql = "INSERT INTO query_response VALUES(null,?,?,?,?,?,?,?,?,?,?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function addButton($values) {

        $sql = "INSERT INTO buttons VALUES(null,?,?,?,?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function addQuickReply($values) {

        $sql = "INSERT INTO quick_replies VALUES(null,?,?,?,?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }
    
    public static function updateQuery($columns, $base_columns) {
        
        return self::dbUpdate('query_response', $columns, $base_columns);
    }
}