<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:28 PM
 */

require_once 'Core.php';

use \core\Core;

date_default_timezone_set('Asia/Dhaka');

class NPS extends Core {

    public static function npsFiltered($where, $offset, $no_of_records_per_page) {

        $sql = "SELECT * FROM ice_nps_analysis " .$where . " LIMIT ?, ?";
        self::$db->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($offset, $no_of_records_per_page));
        return $stmt->fetchAll(2);
    }


    public static function totalNPSFiltered($where) {

        $sql = "SELECT * FROM ice_nps_analysis " .$where . " ORDER BY id DESC";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();
        return $stmt->rowCount(2);
    }

    public static function iceAnalytics($date, $filter) {

        switch ($filter) {
            case 'yes':
                $sql = "SELECT id FROM ice_nps_analysis WHERE answer = ? and date= ?";
                $stmt = self::$db->prepare($sql);
                $stmt->execute(array('yes', $date));
                return $stmt->rowCount();

            case 'no':
                $sql = "SELECT id FROM ice_nps_analysis WHERE answer = ? and date = ?";
                $stmt = self::$db->prepare($sql);
                $stmt->execute(array('no', $date));
                return $stmt->rowCount();

            case 'empty':
                $sql = "SELECT id FROM ice_nps_analysis WHERE answer = ? and date=?";
                $stmt = self::$db->prepare($sql);
                $stmt->execute(array('', $date));
                return $stmt->rowCount();

            case 'comment':
                $sql = "SELECT id FROM ice_nps_analysis WHERE answer != ? and answer !=? and answer != ? and date=?";
                $stmt = self::$db->prepare($sql);
                $stmt->execute(array('yes', 'no', '', $date));
                return $stmt->rowCount();
        }

        return true;
    }

    public static function iceNPSGroupByDate() {

        $sql="SELECT id,date, COUNT(*) AS total FROM `ice_nps_analysis` GROUP BY date ORDER BY id DESC";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(2);
    }

    public static function npsEveryDate($date) {

        $sql = "SELECT * FROM ice_nps_analysis WHERE date=? ORDER BY id DESC";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($date));
        return $stmt->fetchAll(2);
    }

}