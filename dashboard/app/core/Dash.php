<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:28 PM
 */

namespace dash;

require_once 'Core.php';

use \core\Core;

date_default_timezone_set('Asia/Dhaka');

class Dash extends Core {

    public static function totalFbUsers($start_date, $end_date) {

       // $sql="SELECT id FROM `messenger_info` WHERE (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $sql="SELECT DISTINCT(sender) FROM `facebook_bot_messages` WHERE receipent = ? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('BOT',$start_date, $end_date));
        return $stmt->rowCount();

    }

    public static function fbUsersDateGroup($start_date, $end_date) {

        // $sql="SELECT id FROM `messenger_info` WHERE (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $sql="SELECT COUNT(DISTINCT(sender)) as users, STR_TO_DATE(date,'%d-%m-%Y') as date FROM `facebook_bot_messages` WHERE receipent = ? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? ) GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('BOT',$start_date, $end_date));
        return $stmt->fetchAll(2);

    }

    public static function todaysFbUsers() {

        $sql="SELECT DISTINCT(sender) FROM `facebook_bot_messages` WHERE receipent = ? AND date = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('bot', date('d-m-Y')));
        return $stmt->rowCount();
    }

    public static function totalFbMessages($start_date, $end_date) {
        $sql="SELECT id FROM `facebook_bot_messages` WHERE (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($start_date, $end_date));
        return $stmt->rowCount();
    }

    public static function usersInteractionWithBotDateGroup($start_date, $end_date) {
        $sql = "SELECT COUNT(id) as interations, STR_TO_DATE(date,'%d-%m-%Y') as date  FROM `facebook_bot_messages` WHERE sender != 'BOT' AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? ) GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($start_date, $end_date));
        return $stmt->fetchAll(2);
    }

    public static function todaysUsersInteractionWithBot() {
        $sql = "SELECT id as interations, STR_TO_DATE(date,'%d-%m-%Y') as date FROM `facebook_bot_messages` WHERE sender != 'BOT' AND date=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(date('d-m-Y')));
        return $stmt->rowCount();
    }

    public static function todaysFbMessages() {

        $sql="SELECT id FROM `facebook_bot_messages` WHERE date = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(date('d-m-Y')));
        return $stmt->rowCount();
    }


    public static function totalFbMaleUsers($start_date, $end_date) {
        $sql="SELECT id FROM `messenger_info` WHERE gender=? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('male',$start_date, $end_date));
        return $stmt->rowCount();
    }

    public static function totalFbFemaleUsers($start_date, $end_date) {
        $sql="SELECT id FROM `messenger_info` WHERE gender=? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('female',$start_date, $end_date));
        return $stmt->rowCount();
    }

    public static function totalAnsweredByBot($start_date, $end_date) {
        $sql = "SELECT * FROM `facebook_bot_messages` WHERE sender = 'bot' AND action = '' AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($start_date, $end_date));
        return $stmt->rowCount();
    }

    public static function totalAnsweredByAgent($start_date, $end_date) {
        $sql = "SELECT * FROM `facebook_bot_messages` WHERE receipent = 'bot' AND action != '' AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($start_date, $end_date));
        return $stmt->rowCount();
    }

    public static function dailyFbMessages($start_date, $end_date) {
        $sql="SELECT COUNT(id) as messages, STR_TO_DATE(date,'%d-%m-%Y') as daily_date FROM facebook_bot_messages 
              WHERE (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? ) GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($start_date, $end_date));
        return $stmt->fetchAll();
    }

    public static function weeklyFbMessages($start_date, $end_date) {
        $sql="SELECT COUNT(id) as messages, STR_TO_DATE(date,'%d-%m-%Y') as weekly_date FROM facebook_bot_messages 
              WHERE (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? ) GROUP BY YEARWEEK(STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($start_date, $end_date));
        return $stmt->fetchAll();

    }

    public static function monthlyFbMessages($start_date, $end_date) {

        $sql="SELECT COUNT(id) as messages, STR_TO_DATE(date,'%d-%m-%Y') as monthly_date FROM facebook_bot_messages 
              WHERE (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ?) GROUP BY MONTH(STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($start_date, $end_date));
        return $stmt->fetchAll();
    }

    public static function totalAccManagementUsers() {
        $sql="SELECT id FROM `api_issue`";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();
        return $stmt->rowCount();
    }

    public static function totalDataPurchaseUsers($start_date, $end_date) {
        $sql="SELECT DISTINCT msisdn FROM `successful_data_purchse_hit` WHERE (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($start_date, $end_date));
        return $stmt->rowCount();
    }

    public static function totalDataPurchaseSuccessUsers($start_date, $end_date) {
        $sql="SELECT DISTINCT msisdn FROM `successful_data_purchse_hit` WHERE deducted_amount > ? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0,$start_date, $end_date));
        return $stmt->rowCount();
    }

    public static function totalDataPurchaseFailedUsers($start_date, $end_date) {
        $sql="SELECT DISTINCT msisdn FROM `successful_data_purchse_hit` WHERE deducted_amount = ? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0,$start_date, $end_date));
        return $stmt->rowCount();
    }

    public static function totalDataPurchaseRevenue($start_date, $end_date) {
        $sql="SELECT SUM(deducted_amount) as revenue FROM `successful_data_purchse_hit` WHERE (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($start_date, $end_date));
        $res = $stmt->fetch(2);
        return $res['revenue'];
    }

    public static function todaysDataPurchaseRevenue() {
        $sql="SELECT SUM(deducted_amount) as revenue FROM `successful_data_purchse_hit` WHERE date = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(date('d-m-Y')));
        $res = $stmt->fetch(2);
        return $res['revenue'];
    }

    public static function dataPurchaseRevenueDateGroup($start_date, $end_date) {
        $sql="SELECT STR_TO_DATE(date,'%d-%m-%Y') as date, SUM(deducted_amount) as revenue FROM `successful_data_purchse_hit` WHERE deducted_amount > ? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? ) GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0,$start_date, $end_date));
        return $stmt->fetchAll(2);
    }

    public static function totalDataPurchaseSuccess($start_date, $end_date) {
        $sql="SELECT id FROM `successful_data_purchse_hit` WHERE deducted_amount > ? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0,$start_date, $end_date));
        return $stmt->rowCount();
    }


    public static function dataPurchaseSuccessDateGroup($start_date, $end_date) {
        $sql="SELECT STR_TO_DATE(date,'%d-%m-%Y') as date, COUNT(id) as purchased FROM `successful_data_purchse_hit` WHERE deducted_amount > ? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? ) GROUP BY (STR_TO_DATE(date,'%d-%m-%Y'))";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0,$start_date, $end_date));
        return $stmt->fetchAll(2);
    }

    public static function todaysDataPurchaseSuccess() {
        $sql="SELECT id FROM `successful_data_purchse_hit` WHERE deducted_amount > ? AND date = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0, date('d-m-Y')));
        return $stmt->rowCount();
    }

    public static function totalDataPurchaseFailed($start_date, $end_date) {
        $sql="SELECT id FROM `successful_data_purchse_hit` WHERE deducted_amount = ? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? )";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0,$start_date, $end_date));
        return $stmt->rowCount();
    }

    public static function topDataPurchasePackagesCount($start_date, $end_date) {
        $sql = "SELECT pack_name, count(pack_name) as count FROM `successful_data_purchse_hit` WHERE deducted_amount > ? AND (STR_TO_DATE(date,'%d-%m-%Y') BETWEEN ? AND ? ) GROUP BY pack_name
 LIMIT 0, 5" ;
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array(0,$start_date, $end_date));
        return $stmt->fetchAll(2);
    }



}