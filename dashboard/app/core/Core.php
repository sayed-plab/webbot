<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/19/18
 * Time: 5:05 PM
 */

namespace core;

class Core
{
    protected static $db;

    public static function dbConfig($config)
    {
        self::$db = $config;
    }


    protected static function dbInsert($table, $values)
    {
        $params = array_fill(0, count($values), '?');
        $sql = "INSERT INTO " . $table . " VALUES(" . implode(',', $params) . ")";
        $stmt = self::$db->prepare($sql);
        $stmt->execute($values);
        return ($stmt->rowCount()) ? true : false;

    }

    protected static function dbFetchWithCond($table, $where, $fetch_format,  $order_by_column = 'id', $order_by_method = 'ASC')
    {
        $sql = "SELECT * FROM " . $table . " WHERE " . implode(' ', array_keys($where)). " ORDER BY "." ".$order_by_column." ".$order_by_method;
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array_values($where));

        if($fetch_format === 'single') {
            return $stmt->fetch(2);
        } elseif($fetch_format === 'multiple') {
            return $stmt->fetchAll(2);
        }
    }

    protected static function dbFetchWithOutCond($table, $fetch_format, $order_by_column = 'id', $order_by_method = 'ASC')
    {
        $sql = "SELECT * FROM " . $table. " ORDER BY "." ".$order_by_column." ".$order_by_method;
        $stmt = self::$db->prepare($sql);
        $stmt->execute();

        if($fetch_format === 'single') {
            return $stmt->fetch(2);
        } elseif($fetch_format === 'multiple') {
            return $stmt->fetchAll(2);
        }
    }

    protected static function dbUpdate($table, $columns, $base_columns)
    {
        $final_columns = array_merge($columns, $base_columns);
        $sql = "UPDATE " . $table . " SET " . implode(',', array_keys($columns)) . " WHERE " . implode(',', array_keys($base_columns));
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array_values($final_columns));

        return ($stmt->rowCount()) ? true : false;
    }

    protected static function dbUpdateAll($table, $columns)
    {
        $sql = "UPDATE " . $table . " SET " . implode(',', array_keys($columns));
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array_values($columns));

        return ($stmt->rowCount()) ? true : false;
    }

    public static function dbClose()
    {
        self::$db = null;
    }


    public static function fbUserInfo($where) {

        return self::dbFetchWithCond('messenger_info', $where, 'single');
    }

}
