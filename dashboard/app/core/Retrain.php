<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:28 PM
 */

namespace retrain;
require_once 'Core.php';

use \core\Core;

date_default_timezone_set('Asia/Dhaka');

class Retrain extends Core {
    
    public static function untrainedReplies($date) {
        
        $sql = "SELECT * FROM bot_cannot_reply WHERE receipent != '189729221039415' AND action != ? AND action!= ? AND  date=? AND message_content != '' ORDER BY id ASC LIMIT 0,10";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('trained','discard',$date));
        return $stmt->fetchAll(2);
    }
    
    public static function totalUntrainedReplies($date) {
        
        $sql = "SELECT id FROM bot_cannot_reply WHERE receipent != '189729221039415' AND action != ? AND action!= ? AND  date=? AND message_content != ''";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('trained','discard',$date));
        return $stmt->rowCount();
    }
    
    
    public static function parameters($lang) {
        
        if ($lang=='bangla') {
            
            $sql = "SELECT DISTINCT bangla_param FROM `airtel_messenger_query` WHERE bangla_param!= ?";
            $stmt = self::$db->prepare($sql);
            $stmt->execute(array(''));
            return $stmt->fetchAll(2);
        } elseif ($lang=='english') {
            
            $sql = "SELECT DISTINCT english_param FROM `airtel_messenger_query` WHERE english_param!= ?";
            $stmt = self::$db->prepare($sql);
            $stmt->execute(array(''));
            return $stmt->fetchAll(2);
        } elseif ($lang=='banglish') {
            
            $sql = "SELECT DISTINCT banglish_param FROM `airtel_messenger_query` WHERE banglish_param != ?";
            $stmt = self::$db->prepare($sql);
            $stmt->execute(array(''));
            return $stmt->fetchAll(2);
        }
    }
    
    public static function addRetrain($values, $lang) {
       
        if ($lang=='Bangla') {
            return self::dbInsert('retrain_bot_bangla', $values);
        } elseif ($lang=='English') {
           return self::dbInsert('retrain_bot_english', $values);
        } elseif ($lang=='Banglish') {
            return self::dbInsert('retrain_bot_banglish', $values);
        } 
    }
    
    public static function updateTrainStatus($columns, $base_columns) {
        
        return self::dbUpdate('bot_cannot_reply', $columns, $base_columns);
    }

    public static function deleteRetrain($columns, $base_columns) {

        return self::dbUpdate('bot_cannot_reply', $columns, $base_columns);
    }
    
}