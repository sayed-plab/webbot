<?php

require_once 'app/dashboard-controller.php';

$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
$date= $dt->format('d-m-Y');
$tim=$dt->format('h:i a');

$records = buzzXReports();

?>



<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <title>Airtel-x-only Report</title>
</head>
<body>

<div class="container-fluid mb-3 mt-3">
    <div class="col-lg-12 text-center">
        <div>
            <img src="https://forbangladesh.com/airtel_messengerx/dashboardx/assets/img/logo.png" alt="Airtel Logo">
        </div>
        <h2 class="mt-2">Agent Daily Panel List</h2>
        <p class="mt-2"><small>*These data are being audited and will be refreshed if required.</small></p>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">Agent Daily Panel</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="myTable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>User to Bot , excluding Contact agent and buy it count</th>
                            <th style="width: 10%;">DATE</th>
                            <th style="width: 10%;">MONTH</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($records as $record): ?>

                            <tr>
                                <?php if($record['bot2user'] != NULL) { $new_var_bot_2user = round($record['bot2user'] * $record['user2bot'],0); } else  $new_var_bot_2user = $record['user2bot']; ?>
                                <td><?php echo $record['id'] ?></td>

                                <!--<td><?php echo $record['user2bot'] ?></td>-->

                                <td><?php
                                    $xclud_user2bot = $new_var_bot_2user - ($record['contact_agent']+$record['buy']) ;
                                    if($xclud_user2bot>0){
                                        echo $xclud_user2bot;
                                    }

                                    else{
                                        //echo "<b>UNDEFINED DUE TO DATABASE CHANGE</b>"; 
                                        echo $xclud_user2bot;
                                    }
                                    ?></td>


                                <td><b style="color:#CE2E35;"><?php echo $record['date'] ?></b></td>
                                <td><b style="color:#CE2E35;"><?php
                                        $date = $record['date'];
                                        echo date("F", strtotime($date));

                                        ?></b></td>


                            </tr>

                        <?php endforeach; ?>

                        </tbody>
                    </table>
                </div>

            </div>
            <div class="text-center card-footer">
                <small>Powered By Preneur Lab Ltd. © <?= date('Y')?></small>
            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready( function () {
        $('#myTable').DataTable( {
            "order": [[ 0, "desc" ]]
        });
    } );
</script>
</body>
</html>