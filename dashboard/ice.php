<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 11/6/18
 * Time: 4:15 PM
 */

require_once 'app/general/functions.php';
require_once 'app/nps-controller.php';

unauthorizedUserRedirect('login.php');

ini_set('memory_limit', -1);

$iceNPSGroupByDate = iceNPSGroupByDate();
?>


<?php setPageTitle('Dashboard - ICE Report'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>


<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <div class="float-left">
            <i class="fas fa-table"></i>
            ICE Reports
        </div>
        <!-- <div class="float-right">
             <strong>From </strong> 12-10-2018 <strong>To </strong> 12-10-2018
         </div>-->

    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>DATE</th>
                    <th>DAY</th>
                    <th>VIEW</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($iceNPSGroupByDate as $row): ?>
                    <tr>

                    <tr>

                        <td>
                            <?php echo $row['date'] ?>
                        </td>

                        <td>

                            <?php

                            $date = $row['date'];
                            $unixTimestamp = strtotime($date);
                            echo $dayOfWeek = date("l", $unixTimestamp);

                            ?>

                        </td>


                        <td>
                            <div class="btn-group">

                                <a target="_blank" href="ice-every-date.php?date=<?php echo $row['date']?>" type="button" class="btn btn-success">View All</a>
                            </div>
                        </td>
                    </tr>


                <?php endforeach; ?>
                </tbody>
            </table>


        </div>


    </div>
</div>
<!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
</div>

<!--<p class="small text-center text-muted my-5">
    <em>More table examples coming soon...</em>
</p>
-->


<?php require_once 'footer.php' ?>
