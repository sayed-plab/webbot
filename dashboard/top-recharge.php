<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 3/30/19
 * Time: 1:30 PM
 */


require_once 'app/general/functions.php';
require_once 'app/acm-controller.php';

$top10Recharge = top10Recharge();

?>


<?php setPageTitle('Dashboard'); ?>

<?php require_once 'header.php' ?>
<?php require_once 'navbar.php'?>
<?php require_once 'sidebar.php' ?>


<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Top 10 Recharge History
    </div>


    <div class="card-body">


        <div class="table-responsive" >
            <table class="table table-bordered"  width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>MSISDN</th>
                    <th>AMOUNT</th>
                </tr>
                </thead>
                <tbody>
                <?php $sl = 0; ?>
                <?php foreach ($top10Recharge as $row): ?>
                    <?php $sl++ ?>
                    <tr>
                        <td><?= $sl ?></td>
                        <td><?= safeOutput($row['msisdn']) ?></td>
                        <td><?= safeOutput($row['total']) ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>

    </div>
    <div class="card-footer small text-muted"></div>
</div>

<p class="small text-center text-muted my-5">
    <em></em>
</p>



<?php require_once 'footer.php' ?>




