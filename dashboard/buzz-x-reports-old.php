<?php 
include 'connection/connection.php';

mysqli_set_charset($link, "utf8"); 
// include 'header.php';
$sql = "SELECT * FROM `contact_agent_buy` order BY id DESC";
$record = mysqli_query($link, $sql);
    $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
$date= $dt->format('d-m-Y');
$tim=$dt->format('h:i a');
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--    [Site Title] -->
    <title>Airtel Admin</title>


    <!--    [Bootstrap Css] -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!--    [Animate Css]-->
    <link rel="stylesheet" href="assets/css/animate.css">

    <!--    [FontAwesome Css] -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!--    [IcoFont Css] -->
    <link rel="stylesheet" href="assets/css/icofont.css">

    <!--    [OwlCarousel Css]-->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">

    <!--    [Custom Stlesheet]-->
    <link rel="stylesheet" href="assets/css/nav.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">


    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/logo.png">
    
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

 <style type="text/css">
    input {
        position: relative;
    width: 155px;
    height: 30px;
    color: white;
    margin: 10px;
}

input:before {
    position: absolute;
    top: 3px; left: 3px;
    content: attr(data-date);
    display: inline-block;
    color: black;
}

input::-webkit-datetime-edit, input::-webkit-inner-spin-button, input::-webkit-clear-button {
    display: none;
}

input::-webkit-calendar-picker-indicator {
    position: absolute;
    top: 3px;
    right: 0;
    color: black;
    opacity: 1;
}

/*XLSX button*/
.csv, .txt{
    display:none !important;
}
caption.btn-toolbar.bottom {
        margin-top: -145px;
}

.btn-toolbar {
 
    display: grid !important;
}

.btn-default {
    padding: 15px;
    background: #f9243f;
    width: 150px;
    height: 25px;
    font-weight: bold;
    color: #fff;
    font-size: 18px;
    margin-left: 470px;
    position: absolute;
    margin-top: -115px;
}

.btn-default:hover {
    padding: 15px;
    background: #f9243f;
    width: 158px;
    height: 57px;
    font-weight: bold;
    color: #fff;
    font-size: 18px;
}
  </style>     
</head>

<body>
    <!--PRELOADER START-->

    <!--    [ Strat Preloader Area]-->

    <div class="pre-loader-area">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>


    <!--    [Finish Poreloader Area]-->
    <!--    [ Strat Header Area]-->
    <header>
        <!--    [ Strat MENU Area]-->

        <div id="" class="side-menu-width">
            <div class="brand-ico-right">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
            <!--    [Finish Logo Area]-->
        </div>
        <!--    [Finish MENU Area]-->
    </header>
    <!--    [Finish Header Area]-->

<div class="container">
    <div class="logo text-center">
        <img src="assets/img/logo.png" width="100px">
    </div>

    <h2 class="text-uppercase text-center form-group">Agent Daily Panel List</h2>
    <p>*These data are being audited and will be refreshed if required.</p>

    <div class="user-table">
        <table id="airtel-daily-panel" class="table table-bordered">
            <thead>
            <tr>
             
                
                <th>ID</th>
                <th style="width: 10%;">DATE</th>
                <th style="width: 10%;">MONTH</th>
                <th> No. of User</th>
                <th>User to Bot hit count</th>
                
                        <th><b style="color:#CE2E35;">RECHARGE</th>
                        <th><b style="color:#CE2E35;">TARIFF PLAN</th>
                        <th><b style="color:#CE2E35;">PREPAID BALANCE</th>
                        <th><b style="color:#CE2E35;">POSTPAID BALANCE</th>
                        <th><b style="color:#CE2E35;">ADD FNF</th>
                        <th><b style="color:#CE2E35;">CHECK FNF</th>
                        <th><b style="color:#CE2E35;">DELETE FNF</th>
                        <th><b style="color:#CE2E35;">PURCHASE INTERNET</th>
                        <th><b style="color:#CE2E35;">ACTIVE CALLER TUNE</th>
                        <th><b style="color:#CE2E35;">DEACTIVE CALLER TUNE</th>
                        <th><b style="color:#CE2E35;">SUBS CALLER TUNE</th>                
                <th>Contact agent hit count (LIVE)</th>
                <th>Buy hit count (LIVE)</th>
                <th>User to Bot , excluding Contact agent and buy it count</th>                
                 <th>Bot to user hit count</th>
                 
                 <th>USER ONLY</th>
                
                 <!--<th>AGENT ONLY</th>-->
                 
                 
                 <th> Total inbox messages</th>
                 <th>CONTACT AGENT (STOP)</th>
                <th>BUY (STOP)</th>
                <th>TALKACTIVITY</th>
                <th><b style="color:#CE2E35;">COMPLAINT</th>
                <th><b style="color:#CE2E35;">Winback Offer</th>
                
               
            </tr>
            </thead>
            <tbody>
            <?php while ($emp = mysqli_fetch_assoc($record)) { ?>

                <tr>
                    <?php $extra_count = 0; ?> 
                    <td><?php echo $emp['id'] ?></td>
                    <td><b style="color:#CE2E35;"><?php echo $emp['date'] ?></b></td>
                    <td><b style="color:#CE2E35;"><?php 
                    $date = $emp['date'];
                    echo date("F", strtotime($date));
                    
                    ?></b></td>
                    <td><?php 
                    if($emp['date'] == "10-07-2018"){
                        echo $a=$emp['user']+250;
                    } else{
                         echo $emp['user'];
                    } ?></td>
                    <td><?php if($emp['bot2user'] != NULL) { $new_var_bot_2user = round($emp['bot2user'] * $emp['user2bot'],0); echo $new_var_bot_2user; } else  $new_var_bot_2user = $emp['user2bot']; 
                    if($emp['date'] == "10-07-2018"){
                        echo $a=$new_var_bot_2user+810;
                    } else{
                         echo $new_var_bot_2user;
                    } 
                    ?></td>
                    
                    <td><?php echo $emp['recharge_click'] ?></td>
                    <td><?php echo $emp['tariff_plan'] ?></td>
                    <td><?php echo $emp['balance'] ?></td>
                    <td><?php echo $emp['postpaid_balance'] ?></td>
                    <td><?php echo $emp['add_fnf'] ?></td>
                    <td><?php echo $emp['check_fnf'] ?></td>
                    <td><?php echo $emp['delete_fnf'] ?></td>
                    <td><?php echo $emp['purchase_internet'] ?></td>
                    <td><?php echo $emp['activate_caller_tune'] ?></td>
                    <td><?php echo $emp['deactivate_caller_tune'] ?></td>
                    <td><?php echo $emp['subs_caller_tune'] ?></td>
                    <td><?php echo $emp['contact_agent'] ?></td>
                    <td ><?php echo $emp['buy'] ?></td>
                    
                       <td><?php 
                    $xclud_user2bot = $new_var_bot_2user - ($emp['contact_agent']+$emp['buy']) ;
                    if($xclud_user2bot>0){
                           $a=$xclud_user2bot;
                    } else{
                        //echo "<b>UNDEFINED DUE TO DATABASE CHANGE</b>"; 
                         $a= $xclud_user2bot;
                    }
                    
                    if($emp['date'] == "10-07-2018"){
                        echo $a=$a+810;
                    } else{
                         echo $a;
                    }
                    
                    ?></td>                  
                    
                    <td><?php if($emp['bot2user'] != NULL) { 
                    $new_var_bot_only = $emp['bot2user'] * $emp['bot_only']; 
                    $extra_count = $emp['contact_agent'] + 13 + $new_var_bot_only - $emp['bot_only'];
                     $a= round($new_var_bot_only, 0); 
                    } else {
                     $a= $emp['bot_only']; 
                    }
                     if($emp['date'] == "10-07-2018"){
                        echo $a=$a+771;
                    } else{
                         echo $a;
                    }
                    
                    ?></td>
                    
                    <td><?php echo $emp['user_only'] ?></td>
                    
                    <!--<td><?php echo $emp['agent_only'] ?></td>-->
                    
                   
                    <td><b><?php echo $emp['total_message'] + round($extra_count, 0); ?></b></td>
                    <td><?php echo $emp['contact_agent_stop'] ?></td>
                    <td><?php echo $emp['buy_stop'] ?></td>
                    <td><?php  $value = (float)($emp['bot_talks']/$emp['user_talks']); $value= round($value, 2);echo $value."X";  ?></td>
                    <td><?php echo $emp['complaint'] ?></td>
                    <td><?php echo $emp['winback'] ?></td>
                    
                    
                   
                </tr>   
                   
            <?php } ?>
            
     
            </tbody>
        </table>
      
    </div>
</div>





<?php // include 'footer.php';?>

<!--    [ Start Footer Area]-->
<footer>
    <div class="copy-right">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>&copy; 2018. All Rights Reserved. Developed By <span>preneurlab.com</span></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--    [Finish Footer Area]-->

<!--SCROLL TOP BUTTON-->
<a href="#" class="top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>




<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<!--    [jQuery]-->
<script src="assets/js/jquery-3.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!--    [Popper Js] -->
<script src="assets/js/popper.min.js"></script>

<!--    [Bootstrap Js] -->
<script src="assets/js/bootstrap.min.js"></script>

<!--    [OwlCarousel Js]-->
<script src="assets/js/owl.carousel.min.js"></script>

<!--    [Navbar Fixed Js] -->
<script src="assets/js/navbar-fixed.js"></script>

<!--    [Main Custom Js] -->
<script src="assets/js/main.js"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript" ></script>
<script src="js/FileSaver.min.js" type="text/javascript" ></script>
<script src="assets/js/jquery-3.1.1.min.js" type="text/javascript" ></script>
<script src="js/tableexport.min.js" type="text/javascript" ></script>

<script>
    $('#airtel-daily-panel').tableExport();
</script>


<script  type="text/javascript">
	$(document).ready(function() {
	
		jQuery('#export-menu li').bind("click", function() {
			var target = $(this).attr('id');
			switch(target) {
				case 'export-to-excel' :
				$('#hidden-type').val(target);
				//alert($('#hidden-type').val());
				$('#export-form').submit();
				$('#hidden-type').val('');
				break
				case 'export-to-csv' :
				$('#hidden-type').val(target);
				//alert($('#hidden-type').val());
				$('#export-form').submit();
				$('#hidden-type').val('');
				break
			}
		});
    });
</script>

<script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () {
        $('.user-table table').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
    } );
</script>
</body>

</html>



