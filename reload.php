<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 2/10/19
 * Time: 1:04 PM
 */

require_once 'app/core/WebBot.php';

header('Content-Type: application/json');

$user = $_POST['sender_id'];

$bot = new WebBot();
$messages = $bot->lastMessages($user);
$total_messages_count = $bot->totalMessagesCount($user);
$output = array();

foreach ($messages as $row) {
    array_push($output,$row['response']);
}

echo json_encode(array("count" => $total_messages_count, "content" => $output));


