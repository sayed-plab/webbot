<?php

require_once 'app/core/API.php';
require_once 'app/core/WebBot.php';
require_once 'app/core/Facebook.php';
require_once 'app/core/Users.php';

$mode = 'facebook';

$api = new API();
$wBot = new WebBot();
$users = new Users();

$webBotUrl = 'https://airtelbot.forbangladesh.com/webbot/engine.php';
$api_base_url = "https://airtelbot.forbangladesh.com/webbot/app/";

###########   USER INPUT BOX1 ###############
date_default_timezone_set("Asia/Dhaka");
$now = date('Y-m-d H:i:s');

$input = json_decode(file_get_contents('php://input'), true);
$userQuery = null;
$senderId = null;
$payload= null;
$queryLanguage = "en";

$messageText = $input['entry']['message']['text'];
$payload = $input['entry']['message']['payload'];
$senderId = $input['entry']['sender']['id'];

if (!is_null($messageText)) {
    $userQuery = $messageText;
} elseif (!is_null($payload)) {
    $userQuery = $payload;
}


/*
 * Fetch Allowed Plans
 */

$allowedPlan = $wBot->getAllowedPlan($payload);
$plan = $allowedPlan['plan'];
$price = $allowedPlan['price'];

/**
 * Get API issue
 */

$data = $wBot->getApiIssue($senderId);
$userLang = $data['lang'];

###########   PROCESS USER INPUT BOX2 ###############

function isBengali($str) {
    if (mb_detect_encoding($str) == 'UTF-8') {
        return true;
    } else {
        return false;
    }
}

if (isBengali($userQuery)) {
    $queryLanguage = "bn";
}

if (!empty($plan)) {

    $values = array(
        'user_id' => $senderId,
        'issue' => 'purchase_data_pack',
        'date_time' => date('Y-m-d H:i:s')
    );

    $wBot->recordApiIssue($values);
    $wBot->updateDailyAnalytics('data_purchase');
    $wBot->updateApiIssuePlan($plan, $price, $senderId);

    if ($userLang == 'bn') {
        $text = "অনুগ্রহ করে আপনার এয়ারটেল নম্বর লিখুন";
    } else {

        $text = 'Please enter your Airtel number';
    }

    $response = $wBot->message($senderId, $userQuery, $text);

} elseif (is_numeric($userQuery)) {

    $data = $wBot->getApiIssue($senderId);
    $last_issue = $data['issue'];
    $last_msisdn = $data['msisdn'];

    if (strlen($userQuery) == 11 || strlen($userQuery) == 13 || strlen($userQuery) == 14) {

        if ($wBot->isAirtel($userQuery)) {
            $data = $wBot->getApiIssue($senderId);
            $issue = $data['issue'];
            $plan_name = $data['plan'];

            if ($issue == 'add_fnf_now') {

                $wBot->updateApiIssueAddFnF($userQuery, $senderId);

            } else {

                $wBot->updateApiIssueMSISDN($userQuery, $senderId);
            }

            $data = $wBot->getApiIssue($senderId);
            $msisdn = $data['msisdn'];
            $add_fnf = $data['add_fnf'];
            $plan_price = $data['price'];

            if ($issue == 'recharge_now') {

                $wBot->updateApiIssueISSUE('recharge_now_amount', $senderId);

                if ($userLang == 'en') {
                    $text = "Please enter the amount you want to recharge";
                } else {
                    $text = "আপনি যে পরিমাণ রিচার্জ করতে চান লিখুন";
                }

                $response = $wBot->message($senderId, $userQuery, $text);

            } elseif ($issue == 'add_fnf_now') {

                $add_fnf_url = $api_base_url . '/api.php?query=add_fnf&msisdn=' . $msisdn . '&msisdn_fnf=' . $add_fnf;
                $add_fnf_response = json_decode(file_get_contents($add_fnf_url), true);

                $text = "A confirmation SMS has been sent to your mobile number";
                $response = $wBot->message($senderId, $userQuery, $text);

            } else {

                $otp_response = $api->sendOTP($senderId, $userQuery, $issue);

                if ($otp_response == 'sent') {

                    if ($userLang == 'en') {

                        $text = 'Verification code has been sent to your mobile number. Please enter the PIN below.';
                    } else {

                        $text = "আপনার মোবাইল নম্বরে যাচাই কোড পাঠানো হয়েছে। অনুগ্রহ করে নীচে পিনটি লিখুন ।";
                    }

                    $response = $wBot->message($senderId, $userQuery, $text);

                } elseif ($otp_response == 'already_valid') {

                    if ($issue == 'check_my_balance') {

                        $query_balance_url = $api_base_url . '/api.php?query=query_balance&msisdn=' . $msisdn;
                        $query_balance_response = json_decode(file_get_contents($query_balance_url), true);

                        if ($query_balance_response['status'] == 'success') {

                            $text = 'Your account balance is ' . $query_balance_response['body']['balance'] . ' BDT, valid till ' . $query_balance_response['body']['validity'] . ".";
                            $response = $wBot->message($senderId, $userQuery, $text);

                        } else {


                            if ($userLang == 'en') {
                                $text = "Please enter the code again within 5 minutes";
                            } else {
                                $text = "যাচাই কোডটি ৫ মিনিটের মধ্যে লিখুন";
                            }

                            $response = $wBot->message($senderId, $userQuery, $text);
                        }

                    } elseif ($issue == 'purchase_data_pack') {

                        $purchase_data_pack_url = $api_base_url . '/api.php?query=internet_pack_provisioning&msisdn=' . $msisdn . '&plan_name=' . $plan_name;
                        $query_balance_response = json_decode(file_get_contents($purchase_data_pack_url), true);
                        $plan_state = $query_balance_response['plan_state'];

                        $wBot->dataPurchase(array($senderId, $msisdn, $plan_name, $plan_price, $plan_state, $now));


                        if ($plan_state == 'purchased') {

                            $text = "Congrats! You have successfully purchased " . $plan_name;
                            $response = $wBot->message($senderId, $userQuery, $text);

                        } elseif ($plan_state == 'charging_fail') {

                            $text = "Failed to purchase " . $plan_name . ". Please try again";
                            $response = $wBot->message($senderId, $userQuery, $text);
                        } else {

                            $text = "Please try again.";
                            $response = $wBot->message($senderId, $userQuery, $text);
                        }


                    } elseif ($issue == 'get_fnf') {

                        $get_fnf_url = $api_base_url . '/api.php?query=get_fnf&msisdn=' . $msisdn;
                        $get_fnf_response = json_decode(file_get_contents($get_fnf_url), true);

                        $text = "Here is your FnF List:<br>" . $get_fnf_response['status'];
                        $response = $wBot->message($senderId, $userQuery, $text);

                    } elseif ($issue == 'add_fnf_number') {

                        $wBot->updateApiIssueISSUE('add_fnf_now', $senderId);

                        $text = "Now please enter the number you want to add as FnF";
                        $response = $wBot->message($senderId, $userQuery, $text);

                    } elseif ($issue == 'tariff_plan') {

                        $tariff_plan_url = $api_base_url . '/api.php?query=tarrif_plan&msisdn=' . $msisdn;
                        $tariff_plan_response = json_decode(file_get_contents($tariff_plan_url), true);

                        $text = 'Here is your Tariff Plans:<br>';
                        foreach ($tariff_plan_response['plans'] as $row) {
                            $text .= "Name: " . $row[0]['name'] . ", State: " . $row[0]['state'] . '<br>';
                        }

                        $response = $wBot->message($senderId, $userQuery, $text);

                    }

                }
            }
        } else {

            if ($userLang == 'en') {

                $text = 'Please enter your valid Airtel number.';
            } else {

                $text = "অনুগ্রহ করে আপনার সঠিক এয়ারটেল নম্বর লিখুন।";
            }

            $response = $wBot->message($senderId, $userQuery, $text);
        }


    } elseif (strlen($userQuery) == 5) {

        $data = $wBot->getApiIssue($senderId);
        $msisdn = $data['msisdn'];
        $issue = $data['issue'];
        $plan_name = $data['plan'];
        $plan_price = $data['price'];

        $otp_response = $api->verifyOTP($senderId, $msisdn, $issue, $userQuery);

        if ($otp_response == 'valid') {

            if ($issue == 'check_my_balance') {

                $query_balance_url = $api_base_url.'/api.php?query=query_balance&msisdn='.$msisdn;
                $query_balance_response = json_decode(file_get_contents($query_balance_url), true);

                if ( $query_balance_response ) {

                    if ($query_balance_response['status'] == 'success') {

                        $text = 'Your account balance is ' . $query_balance_response['body']['balance'] . ' BDT, valid till ' . $query_balance_response['body']['validity'] . ".";
                        $response = $wBot->message($senderId, $userQuery, $text);

                    } else {

                        $text = "Please try again.";
                        $response = $wBot->message($senderId, $userQuery, $text);
                    }
                }

            } elseif ($issue == 'purchase_data_pack') {

                $purchase_data_pack_url = $api_base_url.'/api.php?query=internet_pack_provisioning&msisdn='.$msisdn.'&plan_name='.$plan_name;
                $query_data_pack_response = json_decode(file_get_contents($purchase_data_pack_url), true);
                $plan_state = $query_data_pack_response['plan_state'];

                $wBot->dataPurchase(array($senderId, $msisdn, $plan_name, $plan_price, $plan_state, $now));


                if ($plan_state == 'purchased') {

                    $text = "Congrats! You have successfully purchased ".$plan_name;
                    $response = $wBot->message($senderId, $userQuery, $text);

                } elseif($plan_state == 'charging_fail') {

                    $text = "Failed to purchase ".$plan_name.". Please try again";
                    $response = $wBot->message($senderId, $userQuery, $text);
                } else {

                    $text = "Please try again.";
                    $response = $wBot->message($senderId, $userQuery, $text);
                }

            } elseif ($issue == 'get_fnf') {

                $get_fnf_url = $api_base_url.'/api.php?query=get_fnf&msisdn='.$msisdn;
                $get_fnf_response = json_decode(file_get_contents($get_fnf_url), true);

                if (!empty($get_fnf_response['status'])) {
                    $text = "Here is your FnF List:<br>".$get_fnf_response['status'];
                } else {
                    $text = "You don't have any FnF yet!";
                }


                $response = $wBot->message($senderId, $userQuery, $text);

            } elseif ($issue == 'add_fnf_number') {

                $wBot->updateApiIssueISSUE('add_fnf_now', $senderId);

                if ($userLang == 'en') {
                    $text = "Now please enter the number you want to add as FnF";
                } else {
                    $text = "এখন আপনি যে নাম্বারটি যোগ করতে চান তা লিখুন";
                }

                $response = $wBot->message($senderId, $userQuery, $text);

            } elseif ($issue == 'tariff_plan') {

                $tariff_plan_url = $api_base_url.'/api.php?query=tarrif_plan&msisdn='.$msisdn;
                $tariff_plan_response = json_decode(file_get_contents($tariff_plan_url), true);

                $text = "Here is your Tariff Plans:<br>";
                foreach ($tariff_plan_response['plans'] as $row) {
                    $text .= "Name: ".$row[0]['name'].", State: ".$row[0]['state']."<br>";
                }

                $response = $wBot->message($senderId, $userQuery, $text);

            }

        } elseif ($otp_response == 'wrong') {

            if ($userLang == 'en') {
                $text = 'Please enter correct verification code';
            } else {
                $text = "সঠিক যাচাই কোডটি ৫ মিনিটের মধ্যে লিখুন";
            }
            $response = $wBot->message($senderId, $userQuery, $text);

        } elseif ($otp_response == 'expired') {

            if ($userLang == 'en') {
                $text = 'Verification code has been expired. Please try again.';
            } else {
                $text = "যাচাই কোডটির মেয়াদ শেষ। আবার চেষ্টা করুন।";
            }

            $response = $wBot->message($senderId, $userQuery, $text);
        }
    } elseif ($last_issue == 'recharge_now_amount') {

        if ($userQuery < 10 OR $userQuery > 1000) {

            if ($userLang == 'en') {
                $text = "Amount must be a number between 10 to 1000 and not a decimal";
            } else {
                $text = "আপনি সর্বনিম্ন ১০ টাকা থেকে সর্বোচ্চ ১০০০ টাকা রিচার্জ করতে পারবেন এবং পূর্ণ সংখ্যা লিখুন।";
            }
            $response = $wBot->message($senderId, $userQuery, $text);

        } else {

            $recharge_url = $api_base_url.'/payment.php?category=prepaid&msisdn='.$last_msisdn.'&amount='.$userQuery;
            $recharge_response = json_decode(file_get_contents($recharge_url), true);

            if ($recharge_response['status'] == 200) {

                $recharge_button = array(
                                      array(
                                            "type" => "web_url",
                                            "url" =>  $recharge_response['payment_url'],
                                            "title" => "Confirm"
                                        )
                                    );
                if ($userLang == 'en') {

                    $text = "I am arranging to recharge BDT " . $userQuery . " to " . $last_msisdn . ". Please confirm to continue";
                    $recharge_button = array(
                        array(
                            "type" => "web_url",
                            "url" =>  $recharge_response['payment_url'],
                            "title" => "Confirm"
                        )
                    );

                } else {
                    $text = "আমি আপনার " . $last_msisdn . " নাম্বারে " . $userQuery . " টাকা রিচার্জ করার ব্যবস্থা করছি। ";
                    $recharge_button = array(
                        array(
                            "type" => "web_url",
                            "url" =>  $recharge_response['payment_url'],
                            "title" => "নিশ্চিত"
                        )
                    );
                }

                $wBot->updateApiIssueISSUE('', $senderId);

                $response = $wBot->payInput($senderId, $userQuery, $text, $recharge_button);

            }
        }
    }

} else {

    if (!is_null($payload) && !empty($payload)) {
        $responseData = json_decode($wBot->nlpEngine($payload), true);
    } else {
        $responseData = json_decode($wBot->nlpEngine($userQuery), true);
    }

    $queryLanguage = $responseData['lang'];

    if ($queryLanguage == "bn") {

        $greet_bn = $responseData['response']['greet_bn'];

        if (!empty($greet_en)) {
            $greet = $greet_en." ".$users->getUserName($senderId).", ";
        } else {
            $greet = '';
        }


        $reply_text = $greet.$responseData['response']['reply_text_bn'];
        $buttons_ = explode('&', $responseData['response']['buttons'])[1];
        $quick_replies = explode('&', $responseData['response']['quick_replies'])[1];
        $carousels = explode('&', $responseData['response']['carousels'])[1];

    } else {

        $greet_en = $responseData['response']['greet_en'];

        if (!empty($greet_bn)) {
            $greet = $greet_bn." ".$users->getUserName($senderId).", ";
        } else {
            $greet = '';
        }

        $reply_text = $greet.$responseData['response']['reply_text_en'];
        $buttons_ = explode('&', $responseData['response']['buttons'])[0];
        $quick_replies = explode('&', $responseData['response']['quick_replies'])[0];
        $carousels = explode('&', $responseData['response']['carousels'])[0];

    }

    $wBot->updateApiIssueLang($queryLanguage, $senderId);
    $intent = $responseData['response']['intent'];
    $data = $wBot->getApiIssue($senderId);
    $userLang = $data['lang'];


    if ($intent == 'check_my_balance') {

        $values = array(
            'user_id' => $senderId,
            'issue' => 'check_my_balance',
            'date_time' => date('Y-m-d H:i:s')
        );

        $wBot->recordApiIssue($values);

        if ($userLang == 'bn') {
            $text = "অনুগ্রহ করে আপনার এয়ারটেল নম্বর লিখুন";
        } else {

            $text = 'Please enter your Airtel number';
        }

        $response = $wBot->message($senderId, $userQuery, $text);

    } elseif ($intent == 'add_fnf') {

        $values = array(
            'user_id' => $senderId,
            'issue' => 'add_fnf_number',
            'date_time' => date('Y-m-d H:i:s')
        );

        $wBot->recordApiIssue($values);
        $text = 'Please enter your own Airtel number first';
        $response = $wBot->message($senderId, $userQuery, $text);

    } elseif ($intent == 'get_fnf') {

        $values = array(
            'user_id' => $senderId,
            'issue' => 'get_fnf',
            'date_time' => date('Y-m-d H:i:s')
        );

        $wBot->recordApiIssue($values);

        if ($userLang == 'bn') {
            $text = "অনুগ্রহ করে আপনার এয়ারটেল নম্বর লিখুন";
        } else {

            $text = 'Please enter your Airtel number';
        }

        $response = $wBot->message($senderId, $userQuery, $text);

    } elseif ($intent == 'tariff_plan') {

        $values = array(
            'user_id' => $senderId,
            'issue' => 'tariff_plan',
            'date_time' => date('Y-m-d H:i:s')
        );

        $wBot->recordApiIssue($values);

        if ($userLang == 'bn') {
            $text = "অনুগ্রহ করে আপনার এয়ারটেল নম্বর লিখুন";
        } else {

            $text = 'Please enter your Airtel number';
        }

        $response = $wBot->message($senderId, $userQuery, $text);

    } elseif ($intent == 'recharge_now') {

        $values = array(
            'user_id' => $senderId,
            'issue' => 'recharge_now',
            'date_time' => date('Y-m-d H:i:s')
        );

        $analytics = array(
            'recharge'
        );

        $wBot->recordApiIssue($values);
        $wBot->updateDailyAnalytics('recharge');

        if ($userLang == 'bn') {
            $text = "অনুগ্রহ করে আপনার এয়ারটেল নম্বর লিখুন";
        } else {

            $text = 'Please enter your Airtel number';
        }

        $response = $wBot->message($senderId, $userQuery, $text);


    } else {

        // Only Message Text
        if (empty($buttons_) && empty($carousels) && empty($quick_replies) && !empty($reply_text)) {

            $response = $wBot->message($senderId, $userQuery, $reply_text);
        } // Carousels and/or Buttons

        elseif (!empty($carousels)) {

            $slidersData = $wBot->getCarousel($carousels);
            $sliders = array();

            foreach ($slidersData as $row) {

                $buttonsData = $wBot->getButtons($row['buttons']);
                $buttons = array();

                foreach ($buttonsData as $data) {

                    if ($data['btn_type'] == 'web_url') {

                        $tmpButtons = array(

                            "type" => "web_url",
                            "url" => $data['url_or_title'],
                            "title" => $data['title_or_payload'],
                        );
                    } elseif ($data['btn_type'] == 'postback') {

                        $tmpButtons = array(
                            "type" => "postback",
                            "title" => $data['url_or_title'],
                            "payload" => $data['title_or_payload'],
                        );
                    }

                    array_push($buttons, $tmpButtons);
                }

                $tmpSliders = array(
                    "active" => $row['active'],
                    "title" => $row['title'],
                    "subtitle" => $row['subtitle'],
                    "image_url" => $row['image_url'],
                    "buttons" => $buttons
                );

                array_push($sliders, $tmpSliders);
            }

            if (!empty($quick_replies)) {

                $buttonsData = $wBot->getQuickReplies($quick_replies);
                $quickReplyButtons = array();

                foreach ($buttonsData as $data) {

                    if ($data['btn_type'] == 'web_url') {

                        $tmpButtons = array(

                            "type" => "web_url",
                            "url" => $data['url_or_title'],
                            "title" => $data['title_or_payload'],
                        );
                    } elseif ($data['btn_type'] == 'postback') {

                        $tmpButtons = array(
                            "type" => "postback",
                            "title" => $data['url_or_title'],
                            "payload" => $data['title_or_payload'],
                        );
                    }

                    array_push($quickReplyButtons, $tmpButtons);
                }
            }

            $response = $wBot->carousel($senderId, $userQuery, $reply_text, $sliders, $quickReplyButtons);
        } // Only Buttons
        elseif (empty($carousels) && !empty($buttons_)) {

            $buttonsData = $wBot->getButtons($buttons_);
            $buttons = array();

            foreach ($buttonsData as $data) {

                if ($data['btn_type'] == 'web_url') {

                    $tmpButtons = array(

                        "type" => "web_url",
                        "url" => $data['url_or_title'],
                        "title" => $data['title_or_payload'],
                    );
                } elseif ($data['btn_type'] == 'postback') {

                    $tmpButtons = array(
                        "type" => "postback",
                        "title" => $data['url_or_title'],
                        "payload" => $data['title_or_payload'],
                    );
                }

                array_push($buttons, $tmpButtons);
            }

            $response = $wBot->payInput($senderId, $userQuery, $reply_text, $buttons);
        } // Only Quick Replies
        elseif (!empty($quick_replies)) {

            $buttonsData = $wBot->getQuickReplies($quick_replies);
            $buttons = array();

            foreach ($buttonsData as $data) {

                if ($data['btn_type'] == 'web_url') {

                    $tmpButtons = array(

                        "type" => "web_url",
                        "url" => $data['url_or_title'],
                        "title" => $data['title_or_payload'],
                    );
                } elseif ($data['btn_type'] == 'postback') {

                    $tmpButtons = array(
                        "type" => "postback",
                        "title" => $data['url_or_title'],
                        "payload" => $data['title_or_payload'],
                    );
                }

                array_push($buttons, $tmpButtons);
            }

            $response = $wBot->quickReplies($senderId, $userQuery, $reply_text, $buttons);
        }
    }
}

###########   SEND USER INPUT BOX3 ###############

if (!empty($response)) {

    $ch = curl_init($webBotUrl);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $response);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type=> application/json',
            'Content-Length=> ' . strlen($response))
    );
    curl_exec($ch);
    curl_close($ch);
}

