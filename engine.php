<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 2/9/19
 * Time: 6:17 PM
 */

require_once 'app/core/WebBot.php';

date_default_timezone_set("Asia/Dhaka");

$wBot = new WebBot();

//user, query, response, dateTime

$input = json_decode(file_get_contents('php://input'), true);
$recipientId = $input['data']['recipient']['id'];
$query = $input['data']['recipient']['query'];
$dateTime = date('Y-m-d H:i:s');

$userChat = array(
    $recipientId,
    $query,
    json_encode($input['data']),
    $dateTime
);

$message = array(
    $recipientId,
    $input['chat']['query'],
    $input['chat']['response'],
    $dateTime
);

$wBot->recordUserMessage($message);

if ($wBot->userChatHistoryRaw($userChat)) {
    echo json_encode(array('status' => 'success'));
} else {
    echo json_encode(array('status' => 'failed'));
}