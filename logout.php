<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 2/24/19
 * Time: 10:58 AM
 */

require_once 'app/inc/common.php';

session();

session_unset();
session_destroy();

if (!isset($_SESSION['user'])) {
    header('Location: user.php');
}